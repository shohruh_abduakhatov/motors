package com.example.nasimxon.components.utils;

import android.widget.EditText;

import com.example.nasimxon.components.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nasimxon on 10/3/2017.
 */

public class ValidatorUtils {
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String FULL_NAME_PATTERN = "[A-Za-z]+ [A-Za-z]+";
    private static final String POSTCODE_PATTERN = "^(([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9][0-9]?)|(([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))))( ?)([0-9][ABD-HJLNP-UW-Z]{2})?$";

    public static final Pattern POSTCODE_REGEX = Pattern.compile(POSTCODE_PATTERN);
    public static final Pattern PASSWORD_REGEX = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z@#$%]).{8,12})");
    public static final Pattern SYMBOLS_REGEX = Pattern.compile("^[A-Za-z0-9`\"@$£*()_{};\\':,.!?\\s-]*$");
    public static final Pattern BANNED_PHONE_NUMBER = Pattern.compile("^(\\+44\\s?|0|0044)([7][0]|[9][0]|[9][1]|[9][8])\\d{1,15}$");
    public static final Pattern LAND_LINE_NUMBER_REGEX = Pattern.compile("^\\(?(?:(?:0(?:0|11)\\)?[\\s-]?\\(?|\\+)44\\)?[\\s-]?\\(?(?:0\\)?[\\s-]?\\(?)?|0)(?:\\d{2}\\)?[\\s-]?\\d{4}[\\s-]?\\d{4}|\\d{3}\\)?[\\s-]?\\d{3}[\\s-]?\\d{3,4}|\\d{4}\\)?[\\s-]?(?:\\d{5}|\\d{3}[\\s-]?\\d{3})|\\d{5}\\)?[\\s-]?\\d{4,5}|8(?:00[\\s-]?11[\\s-]?11|45[\\s-]?46[\\s-]?4\\d))(?:(?:[\\s-]?(?:x|ext\\.?\\s?|\\#)\\d+)?)$");
    public static final Pattern UK_PHONE_NUMBER = Pattern.compile("^((\\+)?44\\s?|0)7([45789]\\d{2}|624)\\s?\\d{3}\\s?\\d{3}$");

    public static boolean isValidPostcode(String val) {
        return POSTCODE_REGEX.matcher(val).matches();
    }

    public static boolean isValidPassword(String val) {
        //check if mobile number matches set criteria
        //074, 075, 076, 077, 078, 079, +44
        //07711 111 111, 077111111111
        return PASSWORD_REGEX.matcher(val).matches();
    }


    public static boolean isValidMobile(String val) {
        //check if mobile number matches set criteria
        //074, 075, 076, 077, 078, 079, +44
        //07711 111 111, 077111111111
        return UK_PHONE_NUMBER.matcher(val).matches();
    }


    public static boolean isValidLandline(String val) {
        return LAND_LINE_NUMBER_REGEX.matcher(val).matches();
    }

    public static boolean isBannedNumber(String val) {
        return BANNED_PHONE_NUMBER.matcher(val).matches();
    }

    public static Integer errorMsgMobileNumber(String phoneNumber) {
        //if left empty then dont validate, if it is required then a required error will show instead
        Integer message = null;
        if (!StringUtils.isEmpty(phoneNumber)) {
            if (isBannedNumber(phoneNumber)) {
                message = R.string.error_banned_phone_number;
            } else if (!isValidMobile(phoneNumber)) {
                message = R.string.error_not_valid_phone_number;
            }
        }
        return message;
    }

    public static Integer errorMsgLandlineNumber(String number) {
        //if left empty then dont validate, if it is required then a required error will show instead
        Integer message = null;
        if (!StringUtils.isEmpty(number)) {
            if (isBannedNumber(number)) {
                message = R.string.error_banned_phone_number;
            } else if (!isValidLandline(number)) {
                message = R.string.error_not_valid_land_line_phone_number;
            }
        }
        return message;
    }

    public static Integer alertMsgUKNumber(String number) {
        Integer message = null;
        if (!StringUtils.isEmpty(number)) {
            number = number.replaceAll("\\s", "");
            if (isBannedNumber(number)) {
                message = R.string.error_banned_phone_number;

            } else if (errorMsgLandlineNumber(number) != null && errorMsgMobileNumber(number) != null) {
                message = R.string.error_not_valid_phone_number;
            }
        }
        return message;
    }

    public static boolean validateVehicleOutput(String text) {
        return !StringUtils.isEmpty(text) && !Constants.NOT_SUPPLIED.equals(text);
    }

    public static boolean validatePassword(String password) {
        return PASSWORD_REGEX.matcher(password).matches();
    }

    public static boolean isValidSymbols(String str) {
        return SYMBOLS_REGEX.matcher(str).matches();
    }


    /**
     * Method checks if given String object match FULL_NAME_PATTERN pattern.
     *
     * @param fullName String to be verified.
     * @return True if given String match FULL_NAME_PATTERN pattern, false otherwise.
     */
    public static boolean isFullNameValid(String fullName) {
        return isTextValid(fullName, FULL_NAME_PATTERN);
    }

    /**
     * Method checks if given String object match EMAIL_PATTERN pattern.
     *
     * @param email String to be verified.
     * @return True if given String match EMAIL_PATTERN pattern, false otherwise.
     */
    public static boolean isEmailValid(String email) {
        return isTextValid(email, EMAIL_PATTERN);
    }

    /**
     * Method checks if given text match provided pattern.
     *
     * @param text    String to be verified.
     * @param pattern Pattern to be used.
     * @return True if given text match provided pattern, false otherwise.
     */
    public static boolean isTextValid(String text, String pattern) {
        if (StringUtils.isEmpty(pattern)) {
            return false;
        }
        if (text == null) {
            return false;
        }
        Pattern patt = Pattern.compile(pattern);
        Matcher matcher = patt.matcher(text);
        return matcher.matches();
    }

    public static void showError(final EditText editText, CharSequence message) {
        editText.requestFocus();
        editText.setError(message);
    }

    public static boolean validateEmail(String email) {
        String matrix = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        try {
            Pattern pattern = Pattern.compile(matrix);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
