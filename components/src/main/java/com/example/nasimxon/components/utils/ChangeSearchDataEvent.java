package com.example.nasimxon.components.utils;

/**
 * Created by Nasimxon on 9/29/2017.
 */

public class ChangeSearchDataEvent {
    private int sectionType;

    public ChangeSearchDataEvent(int sectionType) {
        this.sectionType = sectionType;
    }

    public int getSectionType() {
        return sectionType;
    }

    public void setSectionType(int sectionType) {
        this.sectionType = sectionType;
    }
}
