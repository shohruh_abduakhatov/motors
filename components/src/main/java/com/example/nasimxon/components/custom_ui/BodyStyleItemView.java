package com.example.nasimxon.components.custom_ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

/**
 * Created by Nasimxon on 10/24/2017.
 */

public class BodyStyleItemView extends LinearLayout {
    public static final String SALOON = "saloon";
    public static final String HATCHBACK = "hatchback";
    public static final String CONVERTIBLE = "convertible";
    public static final String PEOPLE_CARRIER = "people carrier";
    public static final String COUPE = "coupe";
    public static final String ESTATE = "estate";
    public static final String PICK_UP = "pick-up";
    public static final String FOUR_X_FOUR = "4 x 4";

    private AppCompatImageView imageView;
    private TextView tvName, tvCount;
    private String name;
    private int count;

    public BodyStyleItemView(Context context) {
        super(context);
        initUI();
    }

    public BodyStyleItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public BodyStyleItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BodyStyleItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI();
    }

    private void initUI() {
        inflate(getContext(), R.layout.body_style_item_view, this);
        imageView = findViewById(R.id.ivBodyStyleItem);
        tvName = findViewById(R.id.tvBodyStyleItemName);
        tvCount = findViewById(R.id.tvBodyStyleItemCount);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
            tvName.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
            tvCount.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        } else {
            if (count == 0) {
                imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.silver));
                tvName.setTextColor(ContextCompat.getColor(getContext(), R.color.silver));
                tvCount.setTextColor(ContextCompat.getColor(getContext(), R.color.silver));
            } else {
                imageView.setColorFilter(null);
                tvName.setTextColor(Color.BLACK);
                tvCount.setTextColor(Color.BLACK);
            }
        }
        startAnimation(null);
    }

    @Override
    public void startAnimation(Animation animation) {
        ValueAnimator animator = ValueAnimator.ofFloat(1f, 0.8f);
        animator.addUpdateListener(valueAnimator -> {
            float value = (float) valueAnimator.getAnimatedValue();
            setScaleX(value > 0.9f ? value : (1.8f - value));
            setScaleY(value > 0.9f ? value : (1.8f - value));
        });
        animator.setDuration(200);
        animator.start();
    }

    public boolean setBodyStyleItem(String name, int count) {
        if (name == null) throw new NullPointerException();
        this.name = name;
        this.count = count;
        tvName.setText(name);
        tvCount.setText("(" + count + ")");
        return setImage(name);
    }

    private boolean setImage(String name) {
        int resId = 0;
        switch (name.toLowerCase()) {
            case SALOON:
                resId = R.drawable.saloon;
                break;
            case HATCHBACK:
                resId = R.drawable.hatchback;
                break;
            case PEOPLE_CARRIER:
                resId = R.drawable.people_carrier;
                break;
            case CONVERTIBLE:
                resId = R.drawable.convertible;
                break;
            case COUPE:
                resId = R.drawable.coupe;
                break;
            case ESTATE:
                resId = R.drawable.estate;
                break;
            case PICK_UP:
                resId = R.drawable.pick_up;
                break;
            case FOUR_X_FOUR:
                resId = R.drawable._4_x_4;
                break;
        }
        imageView.setImageResource(resId);
        return resId != 0;
    }
}
