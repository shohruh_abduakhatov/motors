package com.example.nasimxon.components.custom_ui;

/**
 * Created by Developer on 7/1/17.
 */

public interface SearchViewListener {
    void onTextChanged(String text);

    void onCleared();
}
