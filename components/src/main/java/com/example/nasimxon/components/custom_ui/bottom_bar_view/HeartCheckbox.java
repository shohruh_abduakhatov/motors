package com.example.nasimxon.components.custom_ui.bottom_bar_view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import com.example.nasimxon.components.R;


/**
 * Created by Achilov Bakhrom on 9/6/17.
 */

public class HeartCheckbox extends View {

    private boolean checked = false;
    private HeartCheckboxStateChangedListener listener;

    public HeartCheckbox(Context context) {
        super(context);
        init();
    }

    public HeartCheckbox(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeartCheckbox(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HeartCheckbox(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setMinimumWidth((int) getResources().getDimension(R.dimen.heart_checker_min_size));
        setMinimumHeight((int) getResources().getDimension(R.dimen.heart_checker_min_size));
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap bitmap = null;
        if (checked) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_filled_heart);
        } else {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_empty_heart);
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, getWidth(), getHeight(), false);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawBitmap(bitmap, 0, 0, paint);
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        if (listener != null)
            listener.checked(checked);
        invalidate();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof BottomBarView.SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        HeartCheckbox.SavedState ss = (HeartCheckbox.SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setChecked(ss.checked);
        invalidate();
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        HeartCheckbox.SavedState ss = new HeartCheckbox.SavedState(superState);
        ss.checked = checked;
        return ss;
    }

    static class SavedState extends BaseSavedState {
        boolean checked;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.checked = in.readInt() != 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.checked ? 1 : 0);
        }

        //required field that makes Parcelables from a Parcel
        public static final Parcelable.Creator<HeartCheckbox.SavedState> CREATOR =
                new Parcelable.Creator<HeartCheckbox.SavedState>() {
                    public HeartCheckbox.SavedState createFromParcel(Parcel in) {
                        return new HeartCheckbox.SavedState(in);
                    }

                    public HeartCheckbox.SavedState[] newArray(int size) {
                        return new HeartCheckbox.SavedState[size];
                    }
                };
    }

    public interface HeartCheckboxStateChangedListener {
        public void checked(boolean checked);
    }
}
