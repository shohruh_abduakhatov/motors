package com.example.nasimxon.components.custom_ui.bottom_bar_view;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;
import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.RxBus;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Achilov Bakhrom on 9/4/17.
 */

public class BottomBarView extends LinearLayout {
    private int selection = -1;

    private BottomBarClickListener listener;

    public BottomBarView(Context context) {
        super(context);
        init(context);
    }

    public BottomBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BottomBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BottomBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(lp);
        setOrientation(HORIZONTAL);
        setBackgroundColor(Color.WHITE);
        RxBus.instanceOf().getObservable().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o instanceof ChangeSearchDataEvent) {
                            Log.d("sss", "sdasd");
                            if (selection != -1)
                                paintSelection(selection, R.color.colorAccent);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void setListener(BottomBarClickListener listener) {
        this.listener = listener;
    }

    public void select(int selection) {
        this.selection = selection;
        if (listener != null)
            listener.selection(this.selection);
    }

    public void setAdapter(final BottomBarBaseAdapter adapter) {
        removeAllViews();
        int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int pos = i;
            LinearLayout ll = new LinearLayout(getContext());
            LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.weight = 1.0f;
            ll.setLayoutParams(lp);
            adapter.onBind(ll, i);
            addView(ll);
            ll.setClickable(true);
            ll.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (adapter.getCount() == 5) {
                        View child = null;
                        if (selection != -1)
                            paintSelection(selection, R.color.colorAccent);
                        select(pos);
                        child = ((LinearLayout) view).getChildAt(0);
                        if (child != null) {
                            if (child instanceof ViewGroup) {
                                for (int j = 0; j < ((ViewGroup) child).getChildCount(); j++) {
                                    if (((ViewGroup) child).getChildAt(j) instanceof AppCompatImageView) {
                                        ((AppCompatImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                    } else if (((ViewGroup) child).getChildAt(j) instanceof ImageView) {
                                        ((ImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                    } else if (((ViewGroup) child).getChildAt(j) instanceof AppCompatTextView) {
                                        ((AppCompatTextView) ((ViewGroup) child).getChildAt(j)).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                    } else if (((ViewGroup) child).getChildAt(j) instanceof TextView) {
                                        ((TextView) ((ViewGroup) child).getChildAt(j)).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                    }
                                }
                            } else {
                                if (child instanceof AppCompatImageView) {
                                    ((AppCompatImageView) child).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                } else if (child instanceof ImageView) {
                                    ((ImageView) child).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                } else if (child instanceof AppCompatTextView) {
                                    ((AppCompatTextView) child).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                } else if (child instanceof TextView) {
                                    ((TextView) child).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                }
                            }
                        }
                    } else {
                        select(pos);
                    }
                }
            });
        }
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        select(ss.selectionState);
        if (ss.selectionState != -1) {
            View child = ((LinearLayout) getChildAt(ss.selectionState)).getChildAt(0);
            if (child != null) {
                if (child instanceof ViewGroup) {
                    for (int j = 0; j < ((ViewGroup) child).getChildCount(); j++) {
                        if (((ViewGroup) child).getChildAt(j) instanceof AppCompatImageView) {
                            ((AppCompatImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        } else if (((ViewGroup) child).getChildAt(j) instanceof ImageView) {
                            ((ImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        } else if (((ViewGroup) child).getChildAt(j) instanceof AppCompatTextView) {
                            ((AppCompatTextView) ((ViewGroup) child).getChildAt(j)).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        } else if (((ViewGroup) child).getChildAt(j) instanceof TextView) {
                            ((TextView) ((ViewGroup) child).getChildAt(j)).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        }
                    }
                } else {
                    if (child instanceof AppCompatImageView) {
                        ((AppCompatImageView) child).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    } else if (child instanceof ImageView) {
                        ((ImageView) child).setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    } else if (child instanceof AppCompatTextView) {
                        ((AppCompatTextView) child).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    } else if (child instanceof TextView) {
                        ((TextView) child).setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    }
                }
            }
        }
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.selectionState = selection;
        return ss;
    }

    public void paintSelection(int pos, int color) {
        View child = ((LinearLayout) getChildAt(pos)).getChildAt(0);
        if (pos != -1) {
            if (child != null) {
                if (child instanceof ViewGroup) {
                    for (int j = 0; j < ((ViewGroup) child).getChildCount(); j++) {
                        if (((ViewGroup) child).getChildAt(j) instanceof AppCompatImageView) {
                            ((AppCompatImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(color);
                        } else if (((ViewGroup) child).getChildAt(j) instanceof ImageView) {
                            ((ImageView) ((ViewGroup) child).getChildAt(j)).setColorFilter(color);
                        } else if (((ViewGroup) child).getChildAt(j) instanceof AppCompatTextView) {
                            ((AppCompatTextView) ((ViewGroup) child).getChildAt(j)).setTextColor(color);
                        } else if (((ViewGroup) child).getChildAt(j) instanceof TextView) {
                            ((TextView) ((ViewGroup) child).getChildAt(j)).setTextColor(color);
                        }
                    }
                } else {
                    if (child instanceof AppCompatImageView) {
                        ((AppCompatImageView) child).setColorFilter(color);
                    } else if (child instanceof ImageView) {
                        ((ImageView) child).setColorFilter(color);
                    } else if (child instanceof AppCompatTextView) {
                        ((AppCompatTextView) child).setTextColor(color);
                    } else if (child instanceof TextView) {
                        ((TextView) child).setTextColor(color);
                    }
                }
            }
        }
    }

    public void clearSelection() {
        if (selection != -1)
            paintSelection(selection, R.color.black);
        selection = -1;
    }

    public interface BottomBarClickListener {
        void selection(int pos);
    }

    static class SavedState extends BaseSavedState {
        //required field that makes Parcelables from a Parcel
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
        int selectionState;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.selectionState = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.selectionState);
        }
    }
}
