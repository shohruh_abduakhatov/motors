package com.example.nasimxon.components.custom_ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.nasimxon.components.R;
import com.example.nasimxon.components.utils.ValidatorUtils;

/**
 * Created by Nasimxon on 10/26/2017.
 */

public class PostCodeDialog extends Dialog {
    private EditText editText;
    private Button cancel, ok;
    private PostCodeListener onClickOk;

    public PostCodeDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public PostCodeDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected PostCodeDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    private void init() {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.post_code_dialog);
//        int width = getContext().getResources().getDisplayMetrics().widthPixels;
//        getWindow().setLayout((int) (width * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        editText = findViewById(R.id.etPostCode);
        cancel = findViewById(R.id.btnCancel);
        ok = findViewById(R.id.btnOk);
        editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        if (onClickOk != null) ok.setOnClickListener(view -> {
            if (editText.getText().toString().trim().isEmpty()) return;
            if (ValidatorUtils.isValidPostcode(editText.getText().toString())) {
                onClickOk.clickOk(editText.getText().toString());
                dismiss();
            }
        });
        cancel.setOnClickListener(view -> {
            dismiss();
        });
        setOnDismissListener(dialogInterface -> editText.setText(""));
    }

    public void setOkListener(PostCodeListener okListener) {
        onClickOk = okListener;
        ok.setOnClickListener(view -> {
            if (editText.getText().toString().trim().isEmpty()) {
                editText.setError(getContext().getString(R.string.error_enter_postcode));
                editText.requestFocus();
                return;
            }
            if (ValidatorUtils.isValidPostcode(editText.getText().toString())) {
                onClickOk.clickOk(editText.getText().toString());
                dismiss();
            } else {
                editText.setError(getContext().getString(R.string.error_enter_valid_postcode));
                editText.requestFocus();
            }
        });
    }

    public interface PostCodeListener {
        void clickOk(String postCode);
    }
}