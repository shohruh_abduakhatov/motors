package com.example.nasimxon.components.custom_ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.example.nasimxon.components.R;


/**
 * Created by Nasimxon on 9/5/2017.
 */

public class CircleIndicator extends View implements ViewPager.OnPageChangeListener {
    private int count = 5, selectedPos;
    private float radius;
    private float betweenPadding;
    private int selectColor, defaultColor;
    private Paint paint;

    public CircleIndicator(Context context) {
        super(context);
    }

    public CircleIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircleIndicator, 0, 0);
        try {
            radius = a.getDimensionPixelOffset(R.styleable.CircleIndicator_radius, 15);
            betweenPadding = a.getDimensionPixelOffset(R.styleable.CircleIndicator_betweenPadding, 4);
            selectColor = a.getColor(R.styleable.CircleIndicator_selectColor, Color.parseColor("#4DB881"));
            defaultColor = a.getColor(R.styleable.CircleIndicator_defaultColor, Color.parseColor("#c4c4c4"));
        } finally {
            a.recycle();
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Log.d("sss", "" + betweenPadding + " " + dpToPx(10));
    }

    public CircleIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CircleIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(selectColor);
        if (count != 0) {
            for (int i = 0; i < count; i++) {
                if (i != selectedPos) {
                    paint.setColor(defaultColor);
                } else paint.setColor(selectColor);
                canvas.drawCircle(i * radius * 2 + radius + i * betweenPadding, radius, radius, paint);
            }
        }
    }

    public void setWithViewPager(ViewPager viewPager) {
        this.count = viewPager.getAdapter().getCount();
        viewPager.addOnPageChangeListener(this);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        selectedPos = position;
        postInvalidate();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension((int) (radius * 2 * count + (count - 1) * betweenPadding), (int) radius * 2);
    }
}
