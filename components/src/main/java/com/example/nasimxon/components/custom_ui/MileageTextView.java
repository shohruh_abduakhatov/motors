package com.example.nasimxon.components.custom_ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nasimxon on 9/29/2017.
 */

public class MileageTextView extends LinearLayout {
    private List<TextView> tvNumbers;

    public MileageTextView(Context context) {
        super(context);
        initUI();
    }

    public MileageTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public MileageTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MileageTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI();
    }

    private void initUI() {
        tvNumbers = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            TextView tv = new TextView(getContext());
            if (i > 3) {
                tv.setTextColor(Color.WHITE);
                tv.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_mileage_shape));
            } else
                tv.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_mileage_shape_white));
            tv.setText("0");
            tv.setPadding(4, 2, 4, 2);
            LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 3, 0);
            tv.setLayoutParams(layoutParams);
            addView(tv);
            tvNumbers.add(tv);
        }
    }

    public void setText(int number) {
        for (int i = 0; i < 8; i++) {
            tvNumbers.get(7 - i).setText("" + (number % 10));
            number /= 10;
        }
        if (tvNumbers.get(0).getText().toString().equalsIgnoreCase("0"))
            getChildAt(0).setVisibility(GONE);
    }
}
