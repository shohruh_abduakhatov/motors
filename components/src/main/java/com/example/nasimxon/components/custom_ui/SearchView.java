package com.example.nasimxon.components.custom_ui;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

/**
 * Created by Nasimxon on 7/1/17.
 */

public class SearchView extends RelativeLayout {
    private EditText etSearchInput;
    private TextView tvRequestsCount;
    private SearchViewListener listener;
    private ImageView ivSearch;

    public SearchView(Context context) {
        super(context);
        init(context);
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.search_view, this, true);
        ivSearch = findViewById(R.id.ivSearch);
        etSearchInput = findViewById(R.id.etSearchInput);
        tvRequestsCount = findViewById(R.id.tvRequestsCount);
        etSearchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_NULL) {
                    InputMethodManager imm = (InputMethodManager)
                            getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if (listener != null) {
                        if (etSearchInput.getText().toString().isEmpty()) listener.onCleared();
                        else
                            listener.onTextChanged(etSearchInput.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });
        etSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (listener != null)
                    listener.onTextChanged(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    if (listener != null)
                        listener.onCleared();
                }
            }
        });
        ivSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener == null) return;
                if (etSearchInput.getText().toString().isEmpty()) listener.onCleared();
                else
                    listener.onTextChanged(etSearchInput.getText().toString());
            }
        });
    }

    public void setListener(SearchViewListener listener) {
        this.listener = listener;
    }

    public void setTextSearch(String text) {
        etSearchInput.setText(text);
    }

    public String getSearchText() {
        if (etSearchInput != null)
            return etSearchInput.getText().toString();
        else
            return null;
    }

    public void setLeaveRequestsCount(int count) {
//        tvRequestsCount.setText(getContext().getString(R.string.leave_requests_count_format, count));
    }
}
