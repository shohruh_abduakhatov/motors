package com.example.nasimxon.components.custom_ui.bottom_bar_view;

import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Achilov Bakhrom on 9/5/17.
 */

public abstract class BottomBarBaseAdapter<T> {
    protected List<T> items;

    public BottomBarBaseAdapter(List<T> items) {
        this.items = items;
    }

    public abstract int getCount();

    protected abstract void onBind(ViewGroup child, int position);

    protected abstract T getItem(int position);
}
