package com.example.nasimxon.components.utils;

/**
 * Created by bakhrom on 9/4/17.
 */

public interface Constants {
    String BASE_URL = "http://qa.webservices.motors.co.uk/v3/";

    String APP_TYPE = "Motors";
    String APP_VERSION = "3.7";
    String DEVICE_NAME = "Android";

    String IS_INTRO = "IS_INTRO";
    String UID_KEY = "UID_KEY";
    String UserData = "UserData";
    String DefaultPostCode = "OX14ER";
    String IS_VALID = "IsValid";

    String INFO_INTERNET_CONNECTION = "Please check your connection and try again";

    String PARAM_USERNAME = "username";
    String PARAM_AUTHTOKEN_TYPE = "authtokenType";
    String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
    String SESSION_TIMEOUT = "sessionTimeout";

    int CONNECT_TIMEOUT = 30;
    int READ_TIMEOUT = 15;
    int WRITE_TIMEOUT = 15;

    // Search Sections
    int MAKE_MODEL = 0;
    int BUDGET = 1;
    int AGE_MILEAGE = 2;
    int TRANSMISSION = 3;
    int FUEL_TYPE = 4;
    int BODY_STYLE = 5;
    int COLOURS = 6;
    int VEHICLE_SPEC = 7;
    int PERFORMANCE = 8;
    int RUNNING_COST = 9;
    int KEYWORDS = 10;
    int MORE_OPTIONS = 11;

    //region <Search Data Values>
    String MULTIPLE = "Multiple";
    String ANY = "Any";
    String FILTERS_SELECTED = "Filters Selected";
    String NATIONAL = "National";
    String POUND = "?";
    String ZERO_PRICE = "?0";
    String ONE_PRICE = "?1";
    String MIN = "Min";
    String MAX = "Max";
    String UNLISTED = "Unlisted";
    String NOT_SUPPLIED = "Not Supplied";
    String UP_TO = "up to";
    String Transmissions = "Transmissions";
    String Colours = "Colours";
    String BodyTypes = "BodyTypes";
    String FuelType = "FuelType";
    String Manufacturers = "Manufacturers";
    String Models = "Models";
    String Doors = "Doors";
    String SafetyRatings = "SafetyRatings";
    String Seats = "Seats";
    String OtherOptions = "OtherOptions";
    int ASSOCIATED_VEHICLE_COUNT = 3;

    double DEFAULT_LATITUDE = 52.9226019899796;
    double DEFAULT_LONGITUDE = -1.46744881348129;
    String DEFAULT_POSTCODE = "ox144er";
    String DEFAULT_SOLD_STATUS = "both";
    String NATIONAL_DISTANCE = "1000";
    String NEW_SOCIAL_USER = "NEW_SOCIAL_USER";
    String ZERO_DISTANCE = "Within 1 mile";
    String CALL_FOR_PRICE = "Call for price";
    String PLEASE_SELECT = "Please Select";
    int REQUEST_TIMEOUT = 40000;
    //endregion

    int PER_PAGE = 60;
    int DEFAULT_INTERVAL = 55;
    int SHORTLIST_LIMIT = 25;

    //API ALL RES
    String P_MOTLOIN = "motloin";
    String P_MOTMYGR = "motmygr";
    String P_MOTGEGADET = "motgegadet";
    String P_MOTGENOT = "motgenot";
    String P_MOTSASE = "motsase";
    String P_MOTGESASE = "motgesase";
    String P_MOTGEEM = "motgeem";
    String P_MOTCRAC = "motcrac";
    String P_MOTUPAC = "motupac";
    String P_MOTGESOLOLIUR = "motgesololiur";
    String P_MOTCHPWD = "motchpwd";
    String P_MOTSASHLI = "motsashli";
    String P_MOTGESHLI = "motgeshli";
    String P_MOTDESHLI = "motdeshli";
    String P_MOTRESASE = "motresase";
    String P_MOTDESE = "motdese";
    String P_MOTDEEM = "motdeem";
    String P_MOTSEEM = "motseem";
    String P_MOTREPA = "motrepa";

    //search vehicle Resources
    String P_MOTSECH = "motsech";
    String P_MOTTOCO = "mottoco";
    String P_MOTMAKES = "motmakes";
    String P_MOTMODELS = "motmodels";
    String P_MOTBODY = "motbody";
    String P_MOTFUEL = "motfuel";
    String P_MOTFACET = "motfacet";

    String P_MOTLOG = "motlog";
    String P_MOTVEDET = "motvedet";
    String P_MOTDEASTOCK = "motdeastock";
    String P_MOTSHLISY = "motshlisy";

    String ProviderName = "ProviderName";
    String Location = "Location";
    String WEB_URL = "web_url";
    String Ticket = "Ticket";

    String Name = "Name";
    String IsParent = "IsParent";
    String ParentName = "ParentName";
    String IsSelected = "IsSelected";
    String Value = "Value";
    String Type = "Type";
    String MakeModels = "MakeModels";
    String Data = "Data";

    interface PREFENCE_UTILS {
        String USER_DATA = "user_data";
        String EMAIL_DATA = "email_data";
        String SEARCH_TEMP_DATA = "search_temp_data";
        String SMART_SEARCH_DATA = "smart_search";
        String SEARCH_TOTAL_COUNT = "search_total_count";
    }

    /*Extras*/
    interface REQUESTS {
        int LOGIN_ACTIVITY_REQUEST = 1;
        int REGISTER_ACTIVITY_REQUEST = 2;
        int REFINE_SEARCH_REQUEST = 3;
        int WEB_VIEW_REQUEST = 4;
        int SHORTLIST_ITEM_REQUEST = 4;
    }
}