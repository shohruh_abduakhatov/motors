package com.example.nasimxon.components.custom_ui.bottom_bar_view.bottom_bar_item_view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

/**
 * Created by bakhrom on 9/5/17.
 */

public class MessageCountView extends LinearLayout {

    private int messageCount = 0;

    public MessageCountView(Context context) {
        super(context);
        init(context);
    }

    public MessageCountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MessageCountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MessageCountView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.message_count_view, this, true);
    }

    public void setMessageCount(int count) {
        messageCount = count;
        String messageText = count > 99 ? "+99" : String.valueOf(count);
        ((TextView) findViewById(R.id.tvMessageCount)).setText(messageText);
    }
}
