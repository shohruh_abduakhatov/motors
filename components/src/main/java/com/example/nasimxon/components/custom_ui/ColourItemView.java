package com.example.nasimxon.components.custom_ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

/**
 * Created by LavenderMoon on 10/24/17.
 */

public class ColourItemView extends LinearLayout {
    public static final String BLACK = "black";
    public static final String BLUE = "blue";
    public static final String PURPLE = "purple";
    public static final String SILVER = "silver";
    public static final String GOLD = "gold";
    public static final String GREEN = "green";
    public static final String ORANGE = "orange";
    public static final String GREY = "grey";
    public static final String YELLOW = "yellow";
    public static final String PINK = "pink";
    public static final String BROWN = "brown";
    public static final String WHITE = "white";
    public static final String RED = "red";

    private CheckBox imageView;
    private TextView tvName, tvCount;
    private String name;
    private int count;

    public ColourItemView(Context context) {
        super(context);
        initUI();
    }

    public ColourItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public ColourItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ColourItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI();
    }

    private void initUI() {
        inflate(getContext(), R.layout.colour_item_view, this);
        imageView = findViewById(R.id.colour);
        tvName = findViewById(R.id.colour_title);
        tvCount = findViewById(R.id.count);

    }

    public boolean setColourItem(String name, int count) {
        if (name == null) throw new NullPointerException();
        StringBuilder stringBuilder = new StringBuilder(name);
        stringBuilder.replace(0, 1, String.valueOf(Character.toUpperCase(name.charAt(0))));
        this.name = stringBuilder.toString();
        this.count = count;
        tvName.setText(this.name);
        tvCount.setText("" + count);
        return setImage(this.name);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        imageView.setChecked(selected);
        startAnimation(null);
    }

    @Override
    public void startAnimation(Animation animation) {
        ValueAnimator animator = ValueAnimator.ofFloat(1f, 0.8f);
        animator.addUpdateListener(valueAnimator -> {
            float value = (float) valueAnimator.getAnimatedValue();
            setScaleX(value > 0.9f ? value : (1.8f - value));
            setScaleY(value > 0.9f ? value : (1.8f - value));
        });
        animator.setDuration(200);
        animator.start();
    }

    private boolean setImage(String name) {
        int resId = 0;
        switch (name.toLowerCase()) {
            case BLACK:
                resId = R.drawable.dark_black_selector;
                break;
            case BLUE:
                resId = R.drawable.cyan_selector;
                break;
            case ORANGE:
                resId = R.drawable.light_orange_selector;
                break;
            case PURPLE:
                resId = R.drawable.purple_selector;
                break;
            case SILVER:
                resId = R.drawable.silver_selector;
                break;
            case GOLD:
                resId = R.drawable.gold_selector;
                break;
            case GREEN:
                resId = R.drawable.dark_green_selector;
                break;
            case GREY:
                resId = R.drawable.grey_selector;
                break;
            case WHITE:
                resId = R.drawable.shape_white_selector;
                break;
            case PINK:
                resId = R.drawable.light_pink_selector;
                break;
            case RED:
                resId = R.drawable.dark_red_selector;
                break;
        }
        imageView.setButtonDrawable(getContext().getResources().getDrawable(resId));
        return resId != 0;
    }
}