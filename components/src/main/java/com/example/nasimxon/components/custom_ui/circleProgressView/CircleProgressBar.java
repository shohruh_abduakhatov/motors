package com.example.nasimxon.components.custom_ui.circleProgressView;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.nasimxon.components.R;


/**
 * Created by Nasimxon on 9/27/2017.
 */

public class CircleProgressBar extends View {
    private float radius, lineWidth;
    private float value, maxValue;

    public CircleProgressBar(Context context) {
        super(context);
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CycleProgressView);
        try {
            radius = a.getDimensionPixelOffset(R.styleable.CycleProgressView_radiusProgress, 50);
            lineWidth = a.getDimensionPixelOffset(R.styleable.CycleProgressView_lineWidth, 10);
        } finally {
            a.recycle();
        }
        setMinimumHeight(dpToPx((int) (radius * 2)));
        setMinimumWidth(dpToPx((int) (radius * 2)));
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CircleProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#d7d7d7"));
        canvas.drawCircle(radius, radius, radius, paint);
        RectF rectf = new RectF(0, 0, radius * 2, radius * 2);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        canvas.drawArc(rectf, -90, (float) (value / maxValue * 360f), true, paint);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(radius, radius, radius - lineWidth, paint);
    }

    public void setProgress(int value, int maxValue) {
        this.maxValue = maxValue;
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                CircleProgressBar.this.value = (int) valueAnimator.getAnimatedValue();
                postInvalidate();
            }
        });
        animator.start();
    }

    public void start() {
        int val = (int) this.value;
        ValueAnimator animator = ValueAnimator.ofInt(0, val);
        animator.setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                CircleProgressBar.this.value = (int) valueAnimator.getAnimatedValue();
                postInvalidate();
            }
        });
        animator.start();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension((int) radius * 2, (int) radius * 2);
    }
}