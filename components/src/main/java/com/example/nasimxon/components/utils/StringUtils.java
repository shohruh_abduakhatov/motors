package com.example.nasimxon.components.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nasimxon on 10/3/2017.
 */

public class StringUtils {

    private static final String TAG = StringUtils.class.getSimpleName();
    static DecimalFormat numberFormat = new DecimalFormat("###,###,###,###,###,###,##0");
    static DecimalFormat floatFormat = new DecimalFormat("###,###,###,###,###,###,##0.0");
    public static final String to_date_format = "MMM-dd-yyyy";

    public static final String EMPTY = "";

    public static boolean equals(String str1, String str2) {
        return str1 == null && str2 == null || str1.equals(str2) || (isEmpty(str1) && isEmpty(str2));
    }

    /**
     * Checks if given string is null or empty.
     *
     * @param string String to be verified.
     * @return True if given string is null or empty.
     */
    public static boolean isEmpty(String string) {
        return (string == null) || (string.length() == 0) || string.equals("*") || string.equals("null");
    }

    /**
     * Checks if given string is null. If yes returns empty string.
     *
     * @param text String to be verified.
     * @return Provided string or empty string if the provided one is null.
     */
    public static String getNotNull(String text) {
        return text != null ? text : EMPTY;
    }

    /**
     * Creates concatenation of all strings provided, additionally adding
     * given separator between each concatenation.
     *
     * @param strings   List of string to merge.
     * @param separator Separator to use.
     * @return The concatenated string.
     */
    public static String getConcatenatedString(List<String> strings, String separator) {
        String sep = getNotNull(separator);
        StringBuilder stringBuilder = new StringBuilder();
        if (strings != null) {
            int size = strings.size();
            for (int i = 0; i < size; ++i) {
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(sep);
                }
                stringBuilder.append(strings.get(i));
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Remove all html elements in string
     *
     * @param string Text than contains html elements.
     * @return The concatenated string.
     */
    public static String removeHtml(String string) {
        return null;
    }

    /**
     * Tries to read data under provided input stream as a String object using given encoding.
     *
     * @param inputStream InputStream from which data has to be read.
     * @param encoding    Encoding to be used.
     * @return String object from the stream or null in case of error.
     */
    public static String streamToString(InputStream inputStream, String encoding) {
        String result = null;

        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, encoding));
            String line;
            String newLine = System.getProperty("line.separator");
            if (newLine == null) {
                newLine = "\n";
            }
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(newLine);
            }
            result = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    Log.w(TAG, "StringUtils.streamToString IOException", e);
                }
            }
        }
        return result;
    }

    public static String replace(String str, String find, String replace) {
        if (StringUtils.isEmpty(str)) return "";
        str = str.replaceAll(find, replace);
        return str;
    }

    public static String replaceSpecialCharacters(String str) {
        if (!StringUtils.isEmpty(str)) {
            str = StringUtils.replace(str, "\\+", "[plus]");
            str = StringUtils.replace(str, "\\=", "[equals]");
            str = StringUtils.replace(str, "\\%", "[percent]");
            str = StringUtils.replace(str, "\\&", "[and]");
        }
        return str;
    }

    /**
     * Checks if given string is null or empty.
     *
     * @param string String to be verified.
     * @return True if given string is null or empty.
     */
    public static boolean isNullOrEmpty(String string) {
        return (string == null) || (string.length() == 0) || string.equals("null");
    }

    public static boolean isNotNormal(String str) {
        return isNullOrEmpty(str) || str.equals("*") || str.equals("0") || str.equals("-1") || str.equals("-2") ||
                str.equals(Constants.NOT_SUPPLIED) || str.equals(Constants.ANY) ||
                str.contains(Constants.ANY) || str.equals(Constants.UNLISTED);
    }

    public static String getString(String val) {
        if (val == null) {
            return "";
        }
        return val;
    }

    public static String getString(String value, String defValue) {
        if (value == null || value.isEmpty() || value.equals("*") || value.equals("-1")) {
            return defValue;
        }
        return value;
    }


    public static String formatPrice(String price) {
        String out = "";
        if (!isEmpty(price)) {
            out = "£" + numberCurrency(price);
        }
        return "" + out;
    }

    public static String numberCurrency(String number) {
        return numberFormat.format(Double.parseDouble(number));
    }

    public static String numberCurrency(Integer number) {
        return numberFormat.format(Double.parseDouble(String.valueOf(number)));
    }

    public static String floatFormat(String number) {
        return floatFormat.format(Double.parseDouble(number));
    }

    public static String formatNumber(Integer price) {
        return (price.toString().length() > 3) ? numberFormat.format(price) : price.toString();
    }

    public static String stringToDate(Date date) {
        if (date == null) return "";

        SimpleDateFormat format = new SimpleDateFormat(to_date_format);

        return format.format(date);
    }

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("\\?");
        if (params.length > 1) {
            query = params[1];
        }
        params = query.split("\\&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }

    public static String toShortMonth(String month) {
        String result = "Jan";

        if (month.toLowerCase().contains("feb")) {
            result = "Feb";
        } else if (month.toLowerCase().contains("mar")) {
            result = "Mar";
        } else if (month.toLowerCase().contains("apr")) {
            result = "Apr";
        } else if (month.toLowerCase().contains("may")) {
            result = "May";
        } else if (month.toLowerCase().contains("jun")) {
            result = "Jun";
        } else if (month.toLowerCase().contains("jul")) {
            result = "Jul";
        } else if (month.toLowerCase().contains("aug")) {
            result = "Aug";
        } else if (month.toLowerCase().contains("sep")) {
            result = "Sep";
        } else if (month.toLowerCase().contains("oct")) {
            result = "Oct";
        } else if (month.toLowerCase().contains("nov")) {
            result = "Nov";
        } else if (month.toLowerCase().contains("dec")) {
            result = "Dec";
        }
        return result;
    }

    public static String convertToIntegerString(String str) {
        if (str.contains(".00")) {
            return str.substring(0, str.indexOf(".00"));
        } else if (str.contains(".")) {
            String value1 = str.substring(0, str.indexOf("."));
            String value2 = str.substring(str.indexOf("."), 3);
            return value1 + value2;
        }
        return str;
    }

    public static String toVehicleIcon(String value) {
        value = StringUtils.isEmpty(value) ? value : value.trim();
        if (value.toLowerCase().equals("manual")) {
            return "\uE61B";
        }
        if (value.toLowerCase().equals("automatic")) {
            return "\uE61A";
        }

        if (value.toLowerCase().equals("Semi Automatic")) {
            return "\uE61B";
        }

        if (value.toLowerCase().equals("hatchback")) {
            return "\uE611";
        }

        if (value.toLowerCase().equals("estate")) {
            return "\uE60F";
        }

        if (value.toLowerCase().equals("coupe")) {
            return "\uE60D";
        }

        if (value.toLowerCase().equals("convertible")) {
            return "\uE60E";
        }

        if (value.toLowerCase().equals("saloon")) {
            return "\uE610";
        }

        if (value.toLowerCase().equals("4 x 4")) {
            return "\uE60C";
        }

        if (value.toLowerCase().equals("people carrier")) {
            return "\uE600";
        }

        if (value.toLowerCase().equals("van")) {
            return "\uE604";
        }

        if (value.toLowerCase().equals("pick-up")) {
            return "\uE602";
        }

        if (value.toLowerCase().equals("minibus")) {
            return "\uE61B";
        }

        if (value.toLowerCase().equals("not supplied")) {//todo return van
            return "\uE603";
        }

        if (value.toLowerCase().equals("combi van")) {
            return "\uE605";
        }

        if (value.toLowerCase().equals("chassis cab")) {
            return "\uE608";
        }

        if (value.toLowerCase().equals("dropside")) {
            return "\uE607";
        }

        if (value.toLowerCase().equals("tipper")) {
            return "\uE61B";
        }

//        if (PreferenceUtils.getSearchPanelType().equals(SearchPanelType.UsedCars)) {
//            return "\uE610";
//        } else {
            return "\uE604";
//        }
    }
}
