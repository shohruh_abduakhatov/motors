package com.example.nasimxon.components.custom_ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nasimxon.components.R;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class ProgressLoadingDialog extends Dialog {
    public ProgressLoadingDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.loading_view);
        getWindow().setBackgroundDrawable
                (new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCanceledOnTouchOutside(false);
    }
}