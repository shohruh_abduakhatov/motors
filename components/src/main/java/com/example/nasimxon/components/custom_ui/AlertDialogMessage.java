package com.example.nasimxon.components.custom_ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.nasimxon.components.R;


/**
 * Created by Nasimxon on 7/20/17.
 */

public class AlertDialogMessage extends Dialog {
    private String message, messageSecondary;
    private Button left, right;
    private TextView tvMessage, tvMessageSecondary;
    private View.OnClickListener leftListener, rightListener;

    public AlertDialogMessage(@NonNull Context context, String message) {
        super(context);
        this.message = message;
    }

    public AlertDialogMessage(@NonNull Context context, int themeResId, String message) {
        super(context, themeResId);
        this.message = message;
    }

    protected AlertDialogMessage(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener, String message) {
        super(context, cancelable, cancelListener);
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable
                (new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.alert_dialog_view);
        init();
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();

        int width = displayMetrics.widthPixels;
        getWindow().setLayout((int) (width * 0.8), WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void setOnShowListener(@Nullable OnShowListener listener) {
        super.setOnShowListener(listener);
    }

    private void init() {
        left = findViewById(R.id.btnLeft);
        right = findViewById(R.id.btnRight);
        tvMessage = findViewById(R.id.tvMessage);
        tvMessageSecondary = findViewById(R.id.tvMessageSecondary);
        setMessage(message);
        if (leftListener != null && rightListener != null) {
            left.setOnClickListener(leftListener);
            right.setOnClickListener(rightListener);
        }
        if (messageSecondary != null) tvMessageSecondary.setText(messageSecondary);
    }

    public void setMessage(String message) {
        tvMessage.setText(message);
    }

    public void setMessageSecondary(String message) {
        messageSecondary = message;
        if (tvMessageSecondary != null)
            tvMessageSecondary.setText(message);
    }

    public void setLeftText(String text) {
        left.setText(text);
    }

    public void setRightText(String text) {
        right.setText(text);
    }

    public void setClickLeft(View.OnClickListener listener) {
        leftListener = listener;
    }

    public void setClickRight(View.OnClickListener listener) {
        rightListener = listener;
    }
}
