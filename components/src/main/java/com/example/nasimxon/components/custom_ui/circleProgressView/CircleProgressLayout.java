package com.example.nasimxon.components.custom_ui.circleProgressView;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.nasimxon.components.R;

/**
 * Created by Nasimxon on 9/27/2017.
 */

public class CircleProgressLayout extends FrameLayout {
    private CircleProgressBar progressBar;
    private TextView tvDescription;

    public CircleProgressLayout(@NonNull Context context) {
        super(context);
        initUI();
    }

    public CircleProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public CircleProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CircleProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI();
    }

    private void initUI() {
        LayoutInflater.from(getContext()).inflate(R.layout.progress_view, this, true);
        progressBar = (CircleProgressBar) findViewById(R.id.cpb);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
    }

    public void setProgress(int value, int maxValue) {
        progressBar.setProgress(value, maxValue);
    }

    public void start() {
        progressBar.start();
    }

    public void setDescription(String description) {
        tvDescription.setText(description);
    }
}
