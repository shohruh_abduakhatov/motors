package com.motors.phone.model;

import lombok.Data;

/**
 * Created by Achilov Bakhrom on 9/5/17.
 */

@Data
public class BottomBarItem {
    private int iconId;
    private String text;
    private boolean isMessageSupport = false;

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isMessageSupport() {
        return isMessageSupport;
    }

    public void setMessageSupport(boolean messageSupport) {
        isMessageSupport = messageSupport;
    }
}
