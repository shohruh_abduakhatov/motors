package com.motors.phone.app.search.presenter.adapters;

import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nasimxon.components.custom_ui.ColourItemView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;
import com.motors.phone.common.net_objects.ChoiceItem;

/**
 * Created by LavenderMoon on 10/24/17.
 */

public class ColourListHolder extends BaseViewHolder<ChoiceItem> {
    private ColourItemView colourItemView;
    private TextView textView;

    public ColourListHolder(ViewGroup parent, int res) {
        super(parent, res);
        colourItemView = $(R.id.item_colour);
        textView = $(R.id.colour_title);
    }

    @Override
    public void setData(ChoiceItem data) {
        super.setData(data);
        colourItemView.setColourItem(data.getName(), data.getValue());
        colourItemView.setSelected(data.isSelected());
        textView.setTextColor((data.isSelected()) ? getContext().getResources().getColor(R.color.colorAccent) : getContext().getResources().getColor(R.color.grey));
    }
}