package com.motors.phone.app.main.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.main.model.RecentSearchModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class RecentSearchAdapter extends BaseViewHolder<RecentSearchModel> {

    public RecentSearchAdapter(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(RecentSearchModel data) {
        super.setData(data);

    }
}
