package com.motors.phone.app.main.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.main.model.SinceAwayModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class SinceYouWereAwayHolder extends BaseViewHolder<SinceAwayModel> {

    public SinceYouWereAwayHolder(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(SinceAwayModel data) {
        super.setData(data);

    }
}
