package com.motors.phone.app.login.presenter;

import android.accounts.AccountManager;
import android.content.Intent;
import android.util.Log;

import com.motors.phone.app.login.LoginFragment;
import com.motors.phone.app.login.model.LoginModel;
import com.motors.phone.app.login.oauth.AccountUtils;
import com.motors.phone.core.BasePresenterImp;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class LoginPresenterImpl extends BasePresenterImp<LoginFragment> implements LoginPresenter {
    @Inject
    public LoginPresenterImpl(LoginFragment loginFragment) {
        super(loginFragment);
    }

    @Override
    public void fillLogin(LoginModel user) {
        Observable.create((ObservableOnSubscribe<Intent>) e -> {
            String authToken = AccountUtils.mServerAuthenticator.signIn(user.getEmail(), user.getPassword());
            final Intent res = new Intent();
            res.putExtra(AccountManager.KEY_ACCOUNT_NAME, user.getEmail());
            res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AccountUtils.ACCOUNT_TYPE);
            res.putExtra(AccountManager.KEY_AUTHTOKEN, authToken);
            res.putExtra("password", user.getPassword());
            e.onNext(res);
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Intent>() {
                    @Override
                    public void onSubscribe(Disposable d) {}
                    @Override
                    public void onNext(Intent intent) {
                        if (null == intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)) {
                            Log.d("sss", "hato");
                        } else
                            Log.d("sss", "ok");
                    }
                    @Override
                    public void onError(Throwable e) {}
                    @Override
                    public void onComplete() {}
                });
//        if (apiManager.hasConnection()) {
//            fragment.setRequestSend(true);
//            loadingDialog.show();
//            apiManager.login(user)
//                    .subscribe(new MotorFilterSubscribe<MotorsResponse<LoginResponse>>() {
//                        @Override
//                        protected void onError(Throwable t, String message) {
//                            loadingDialog.hide();
//                            fragment.setRequestSend(false);
//                            if (message != null)
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                        }
//                        @Override
//                        protected void onResult(MotorsResponse<LoginResponse> result) {
//                            loadingDialog.hide();
//                            if (fragment.isRequestSend()) {
//                                fragment.setRequestSend(false);
//
//                                managerUtil.saveUID(result.getData().getUid());
//                                if (managerUtil.getUID().equalsIgnoreCase(Constants.NEW_SOCIAL_USER)) {
//                                    fragment.openRegister();
//                            } else
//                                    fragment.startMain();
//                            }
//                        }
//                    });
//        } else {
//            fragment.setRequestSend(false);
//            Toast.makeText(fragment.getContext(), "Internet connection failed", Toast.LENGTH_SHORT).show();
//        }
    }
}
