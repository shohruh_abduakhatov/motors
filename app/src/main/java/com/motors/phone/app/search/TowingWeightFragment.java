package com.motors.phone.app.search;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.motors.phone.R;
import com.motors.phone.common.MaxTowingBrakedType;
import com.motors.phone.common.MaxTowingUnbrakedType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class TowingWeightFragment extends BaseSearchFragment implements AdapterView.OnItemSelectedListener {
    @Inject
    SearchData searchData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_towing_weight, container, false);
        AppCompatSpinner spinner = view.findViewById(R.id.spMaxBraked);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getData(MaxTowingBrakedType.values())));
        spinner.setOnItemSelectedListener(this);
        spinner = view.findViewById(R.id.spMaxUnbraked);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getData(MaxTowingUnbrakedType.values())));
        spinner.setOnItemSelectedListener(this);
        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spMaxBraked:

                break;
            case R.id.spMaxUnbraked:

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    private List<String> getData(iSearchType[] list) {
        List<String> temp = new ArrayList<>();
        for (iSearchType a : list) {
            temp.add(a.getStrKey());
        }
        return temp;
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveSearches() {

    }
}
