package com.motors.phone.app.main.presenter;

import com.motors.phone.app.main.view.UsedCarsDetailFragment;
import com.motors.phone.util.ApiManager;

import javax.inject.Inject;

/**
 * Created by Achilov Bakhrom on 9/13/17.
 */

public class UsedCarsDetailPresenterImpl implements UsedCarsDetailPresenter {

    private final UsedCarsDetailFragment fragment;


    private final ApiManager apiManager;

    @Inject
    public UsedCarsDetailPresenterImpl(UsedCarsDetailFragment fragment, ApiManager apiManager) {
        this.fragment = fragment;
        this.apiManager = apiManager;
    }

    @Override
    public void fillDatas() {

    }
}

