package com.motors.phone.app.main.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.main.model.WishListModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class WishListHolder extends BaseViewHolder<WishListModel> {

    public WishListHolder(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(WishListModel data) {
        super.setData(data);

    }
}
