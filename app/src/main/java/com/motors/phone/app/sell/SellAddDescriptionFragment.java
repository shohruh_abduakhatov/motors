package com.motors.phone.app.sell;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motors.phone.R;
import com.motors.phone.core.BaseFragment;

import butterknife.ButterKnife;

/**
 * Created by bakhrom on 9/12/17.
 */

public class SellAddDescriptionFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selling_description, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
