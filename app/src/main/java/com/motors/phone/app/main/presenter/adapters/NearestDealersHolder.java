package com.motors.phone.app.main.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.main.model.NearestDealersModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class NearestDealersHolder extends BaseViewHolder<NearestDealersModel> {

    public NearestDealersHolder(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(NearestDealersModel data) {
        super.setData(data);
    }

}