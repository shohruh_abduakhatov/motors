package com.motors.phone.app.search.presenter;

/**
 * Created by Nasimxon on 10/25/2017.
 */

public interface JustListPresenter {
    void fillLoadList(String type);
}