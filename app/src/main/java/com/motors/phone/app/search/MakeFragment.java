package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.ChoiceHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.ChoicesComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public class MakeFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    SearchData searchData;
    @Inject
    JustListPresenterImp<MakeFragment> presenter;
    @BindView(R.id.ervMakeModel)
    EasyRecyclerView ervMakeModel;
    @Getter
    RecyclerArrayAdapter adapter;
    @BindView(R.id.swMakeFragment)
    SwitchCompat swMakeFragment;
    private List<ChoiceItem> makes;
    private List<ChoiceItem> selectMakes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.make_model_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        selectMakes = new ArrayList<>();
        makes = new ArrayList<>();
        ervMakeModel.setLayoutManager(new LinearLayoutManager(getContext()));
        ervMakeModel.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ChoiceHolder(parent, R.layout.item_makes, true);
            }
        });

        adapter.setOnItemClickListener(position -> {
            if (makes != null) {
                if (swMakeFragment.isChecked()) {
                    if (selectMakes.get(position).getValue() == 0) return;
                    selectMakes.get(position).setWithAnim(true);
                    selectMakes.get(position).setSelected(!selectMakes.get(position).isSelected());
                } else {
                    if (makes.get(position).getValue() == 0) return;
                    makes.get(position).setWithAnim(true);
                    makes.get(position).setSelected(!makes.get(position).isSelected());
                    if (makes.get(position).isSelected())
                        selectMakes.add(makes.get(position));
                    else {
                        searchData.removeModelsByParent(makes.get(position));
                        selectMakes.remove(makes.get(position));
                    }
                }
                adapter.notifyItemChanged(position);
                saveSearches();
            }
        });
        fillLoadList(Constants.Manufacturers);
        swMakeFragment.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (searchData.getMakes().isEmpty()) {
                    swMakeFragment.setChecked(false);
                    Toast.makeText(getContext(), "No select makes", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int i = 0; i < selectMakes.size(); i++) {
                    if (!selectMakes.get(i).isSelected()) {
                        selectMakes.remove(i);
                        i--;
                    }
                }
                adapter.clear();
                Collections.sort(selectMakes, new ChoicesComparator(selectMakes.size()));
                adapter.addAll(selectMakes);
            } else {
                adapter.clear();
                saveSearches();
                adapter.addAll(makes);
            }
        });
    }

    @Override
    public void setData(List<ChoiceItem> makes) {
        Collections.sort(makes, new ChoicesComparator(makes.size()));
        this.makes = makes;
        for (ChoiceItem make : makes) {
            if (searchData.getMakes() != null)
                for (int i = 0; i < searchData.getMakes().size(); i++) {
                    if (make.getName().equalsIgnoreCase(searchData.getMakes().get(i).getName())) {
                        if (make.getValue() == 0) {
                            searchData.getMakes().remove(i);
                            i--;
                        } else {
                            selectMakes.add(make);
                            make.setSelected(true);
                        }
                    }
                }
        }
        adapter.addAll(makes);
    }

    @Override
    public void fillLoadList(String type) {
        presenter.fillLoadList(type);
    }

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.MAKE_MODEL));
    }

    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectMakes.size(); i++) {
            if (selectMakes.get(i).isSelected()) choiceItems.add(selectMakes.get(i));
        }
        searchData.setMakes(choiceItems);
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        searchData.clearMakeModels();
        selectMakes.clear();
        for (ChoiceItem make : makes) {
            make.setSelected(false);
        }
        swMakeFragment.setChecked(false);
        adapter.notifyDataSetChanged();
    }
}