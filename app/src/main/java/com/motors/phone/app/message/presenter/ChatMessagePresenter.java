package com.motors.phone.app.message.presenter;

/**
 * Created by Nasimxon on 9/6/17.
 */

public interface ChatMessagePresenter {
    void fillMessages();
}