package com.motors.phone.app.value;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motors.phone.R;
import com.motors.phone.core.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bakhrom on 9/5/17.
 */

public class ValueFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.value_screen, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnGo)
    public void go() {
        ValueDetailActivity.start(getActivity());
    }
}
