package com.motors.phone.app.value;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motors.phone.R;
import com.motors.phone.util.FragmentManagerUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ValueDetailsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.value_detail_screen, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnGetInstant)
    public void go() {
        FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new ValueInstantCashFragment());
    }

    @OnClick(R.id.tvBack)
    public void back() {
        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1)
            getActivity().finish();
        FragmentManagerUtils.popBackstack((AppCompatActivity) getActivity());
    }


}
