package com.motors.phone.app.main.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.main.model.UsedCars;

/**
 * Created by bakhrom on 9/13/17.
 */


public class UsedCarsHolder extends BaseViewHolder<UsedCars> {

    public UsedCarsHolder(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(UsedCars data) {
        super.setData(data);

    }
}
