package com.motors.phone.app.buy;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

import com.motors.phone.R;
import com.motors.phone.core.BaseActivity;
import com.motors.phone.util.FragmentManagerUtils;

/**
 * Created by Nasimxon on 9/25/2017.
 */

public class BuyDetailActivity extends BaseActivity {

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BuyDetailActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    protected void openSubFragment() {
        FragmentManagerUtils.addFragment(this, R.id.flMain, new BuyDetailFragment());
    }

    @Override
    protected void setTitle() {
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.buy_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
