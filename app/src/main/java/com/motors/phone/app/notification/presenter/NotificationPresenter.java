package com.motors.phone.app.notification.presenter;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public interface NotificationPresenter {
    void fillNotifications();
}
