package com.motors.phone.app.buy.presenter;

import com.motors.phone.app.buy.BuyDetailFragment;
import com.motors.phone.app.buy.model.BuyCarsModel;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Nasimxon on 9/6/17.
 */


public class BuyDetailPresenterImpl implements BuyPresenter {

    private BuyDetailFragment buyFragment;
    private ApiManager apiManager;

    @Inject
    public BuyDetailPresenterImpl(BuyDetailFragment buyFragment, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.buyFragment = buyFragment;
    }

    @Override
    public void fillBuyCars() {
        apiManager.getBuyCars()
                .subscribe(new Observer<List<BuyCarsModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<BuyCarsModel> recentSearchModels) {
                        buyFragment.getAdapterCars().addAll(recentSearchModels);
                    }
                });
    }

}
