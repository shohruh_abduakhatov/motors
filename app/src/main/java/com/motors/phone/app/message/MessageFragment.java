package com.motors.phone.app.message;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.message.presenter.MessagePresenter;
import com.motors.phone.app.message.presenter.MessagePresenterImpl;
import com.motors.phone.app.message.presenter.adapters.MessagesHolder;
import com.motors.phone.core.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by bakhrom on 9/5/17.
 */

public class MessageFragment extends BaseFragment implements MessagePresenter {
    @BindView(R.id.ervMessages)
    EasyRecyclerView ervMessages;
    @Getter
    RecyclerArrayAdapter adapterMessages;

    @Inject
    MessagePresenterImpl messagePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.messages_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        ervMessages.setLayoutManager(new LinearLayoutManager(getContext()));
        ervMessages.setAdapter(adapterMessages = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new MessagesHolder(parent, R.layout.item_message);
            }
        });
        adapterMessages.setOnItemClickListener(position -> {
//            FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new ChatMessagesFragment());
            Intent intent = new Intent(getContext(), ChatActivity.class);
            startActivity(intent);
        });
        fillMessages();
    }

    @Override
    public void fillMessages() {
        messagePresenter.fillMessages();
    }
}