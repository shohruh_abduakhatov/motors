package com.motors.phone.app.search;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.motors.phone.R;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModelTrimPagerFragment extends BaseSearchFragment {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    private AdapterModelTrim adapterModelTrim;
    private String selectPosName;
    private List<ModelFragment> modelFragments;

    public static ModelTrimPagerFragment getInstance(String selectPosName) {
        ModelTrimPagerFragment modelTrimPagerFragment = new ModelTrimPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("selectPosName", selectPosName);
        modelTrimPagerFragment.setArguments(bundle);
        return modelTrimPagerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectPosName = getArguments().getString("selectPosName");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_model_trim_pager, container, false);
        ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    private void initUi() {
        modelFragments = new ArrayList<>();
        modelFragments.add(ModelFragment.getInstance(selectPosName));
        modelFragments.add(ModelFragment.getInstance(selectPosName));
        adapterModelTrim = new AdapterModelTrim(getChildFragmentManager(), modelFragments);
        viewPager.setAdapter(adapterModelTrim);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void clickReset() {
        ((BaseSearchFragment)adapterModelTrim.getItem(viewPager.getCurrentItem())).clickReset();
    }

    @Override
    public void saveSearches() {

    }

    private class AdapterModelTrim extends FragmentStatePagerAdapter {
        private List<ModelFragment> fragments;

        public AdapterModelTrim(FragmentManager fm, List<ModelFragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }
        @Override
        public int getCount() {
            return fragments.size();
        }
        @Override
        public Fragment getItem(int position) {
            return modelFragments.get(position);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return position == 0 ? "Models" : "Trims";
        }
    }
}