package com.motors.phone.app.login;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nasimxon.components.utils.StringUtils;
import com.example.nasimxon.components.utils.ValidatorUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.motors.phone.R;
import com.motors.phone.app.login.model.LoginModel;
import com.motors.phone.app.login.presenter.LoginPresenter;
import com.motors.phone.app.login.presenter.LoginPresenterImpl;
import com.motors.phone.app.main.MainActivity;
import com.motors.phone.common.SocialProviderType;
import com.motors.phone.common.net_objects.User;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Nasimxon on 9/4/2017.
 */

public class LoginFragment extends BaseFragment implements LoginPresenter, GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 21;
    private static final String TAG = "Motors Login";
    public boolean hasError;
    @Inject
    LoginPresenterImpl loginPresenter;
    @BindView(R.id.btnLoginSection)
    View btnLoginSection;
    @BindView(R.id.llRegister)
    View llRegister;
    @BindView(R.id.tvSkip)
    View tvSkip;
    @BindView(R.id.btnRegisterSection)
    Button btnRegister;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    private boolean isLogin = true;
    private boolean isSocialUser;
    private LoginButton fbLoginButton;
    private CallbackManager fbCallbackManager;
    private TwitterLoginButton twitterLoginButton;
    private GoogleApiClient mGoogleApiClient;
    private AccountManager accountManager;
    private LoginActivity activity;
    private User user = new User();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        activity = (LoginActivity) getActivity();
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.log_in_screen, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    private void init() {
        btnLogin.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(btnLogin.getWindowToken(), 0);
        if (!btnRegister.isSelected() && !btnLoginSection.isSelected()) {
            btnLoginSection.setSelected(true);
        }
        accountManager = AccountManager.get(getActivity());
        etPassword.setOnEditorActionListener(this::onEditorListener);
    }

    private boolean onEditorListener(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            hideKeyboard(textView);
            clickLoginRegister();
            return true;
        }
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        // facebook sign in init
        fbLoginButton = new LoginButton(getContext());
        fbLoginButton.setReadPermissions("email", "public_profile");
        fbCallbackManager = CallbackManager.Factory.create();
        fbLoginButton.registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("sss", "facebook:onSuccess:" + loginResult.getAccessToken().getToken());
                user.setTicket(loginResult.getAccessToken().getToken());
                user.setSocialProviderType(SocialProviderType.Facebook);
                clickLoginRegister();
            }

            @Override
            public void onCancel() {
                Log.d("sss", "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("sss", "facebook:onError", error);
            }
        });
        // Twitter sign in init
        twitterLoginButton = new TwitterLoginButton(getContext());
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d("sss", "twitterLogin:success" + result.data.getAuthToken());
            }

            @Override
            public void failure(TwitterException exception) {
                Log.w("sss", "twitterLogin:failure", exception);
            }
        });
        // Google sign in init
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            if (mGoogleApiClient != null) mGoogleApiClient.connect();
            else
                try {
                    mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                            .enableAutoManage(getActivity(), this)
                            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                            .build();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        } catch (Exception e) {
        }
    }

    @OnClick(value = {R.id.btnLoginSection, R.id.btnRegisterSection})
    public void clickRegisterButtons(View v) {
        if (v.getId() == R.id.btnLoginSection) {
            isLogin = true;
            v.setSelected(true);
            btnRegister.setSelected(false);
            btnLogin.setText("LOG IN");
            tvSkip.setVisibility(View.VISIBLE);
        } else {
            isLogin = false;
            v.setSelected(true);
            btnLoginSection.setSelected(false);
            btnLogin.setText("Register");
            tvSkip.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnLogin)
    public void clickLoginRegister() {
//        if (!hasErrorsAfterValidation()) {
        if (isLogin) {
            LoginModel loginModel = new LoginModel();
            if (user.getTicket() != null) {
                loginModel.setEmail(etEmail.getText().toString());
                loginModel.setPassword(etPassword.getText().toString());
                loginModel.setSocialProviderType(SocialProviderType.Default.getName());
            } else {
                loginModel.setTicket(user.getTicket());
                loginModel.setSocialProviderType(user.getSocialProviderType().getName());
            }
            fillLogin(loginModel);
//            } else {
//                if (user.getTicket() == null) {
//                    user.setEmail(etEmail.getText().toString());
//                    user.setPassword(etPassword.getText().toString());
//                    user.setSocialProviderType(SocialProviderType.Default);
//                    openRegister();
//                }
//                hideKeyboard(etPassword);
//            }
        }
    }

    public void openRegister() {
        FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flContainer, RegisterFragment.getInstance(user));
    }

    private boolean hasErrorsAfterValidation() {
        if (!StringUtils.isEmpty(user.getTicket())) return false;
        hasError = false;
        EditText tempView = etEmail;
        if (TextUtils.isEmpty(tempView.getText().toString())) {
            makeAlertText(tempView, R.string.error_enter_email);
            hasError = true;
        } else if (!ValidatorUtils.validateEmail(tempView.getText().toString())) {
            makeAlertText(tempView, R.string.error_enter_valid_email);
            hasError = true;
        }
        if (!isSocialUser) {
            tempView = etPassword;
            if (TextUtils.isEmpty(tempView.getText().toString())) {
                makeAlertText(tempView, R.string.error_enter_password);
                hasError = true;
            } else if (!ValidatorUtils.isValidPassword(tempView.getText().toString())) {
                makeAlertText(tempView, R.string.error_enter_valid_password);
                hasError = true;
            }
        }
        return hasError;
    }

    @OnClick(R.id.tvSkip)
    public void clickSkip() {
        MainActivity.start(getContext());
        getActivity().finish();
    }

    @Override
    public void fillLogin(LoginModel user) {
        loginPresenter.fillLogin(user);
    }


    @OnClick(value = {R.id.llTwitter, R.id.llFacebook, R.id.llGoogle})
    public void clickSocial(View v) {
        if (v.getId() == R.id.llTwitter) {
            twitterLoginButton.performClick();
        } else if (v.getId() == R.id.llFacebook) {
            fbLoginButton.performClick();
        } else {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            fbCallbackManager.onActivityResult(requestCode, resultCode, data);
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            Toast.makeText(getContext(), "" + result.getSignInAccount().getDisplayName(), Toast.LENGTH_SHORT).show();
            user.setTicket(result.getSignInAccount().getIdToken());
            user.setEmail(result.getSignInAccount().getEmail());
            user.setUserName(result.getSignInAccount().getDisplayName());
            user.setMembershipUserKey(result.getSignInAccount().getId());
            clickLoginRegister();
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(status -> {
            });
        } else {
        }
    }

    public void startMain() {
        MainActivity.start(getContext());
        getActivity().finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}