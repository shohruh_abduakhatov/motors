package com.motors.phone.app.main.presenter;

import com.motors.phone.app.main.model.WishListModel;
import com.motors.phone.app.main.view.WishListDetailFragment;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Achilov Bakhrom on 9/13/17.
 */

public class WishDetailListPresenterImpl implements WishDetailListPresenter {
    private ApiManager apiManager;
    private WishListDetailFragment fragment;

    @Inject
    public WishDetailListPresenterImpl(WishListDetailFragment fragment, ApiManager apiManager) {
        this.fragment = fragment;
        this.apiManager = apiManager;
    }

    @Override
    public void fillWishList() {
        apiManager
                .getWishCars()
                .subscribe(new Observer<List<WishListModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<WishListModel> wishListModels) {
                        fragment.getAdapter().addAll(wishListModels);
                    }
                });
    }
}