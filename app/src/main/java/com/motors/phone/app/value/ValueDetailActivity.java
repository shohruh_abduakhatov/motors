package com.motors.phone.app.value;

import android.app.Activity;
import android.content.Intent;

import com.motors.phone.R;
import com.motors.phone.core.BaseActivity;
import com.motors.phone.util.FragmentManagerUtils;

/**
 * Created by Nasimxon on 9/24/2017.
 */

public class ValueDetailActivity extends BaseActivity {

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ValueDetailActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    protected void openSubFragment() {
        FragmentManagerUtils.addFragment(this, R.id.flMain, new ValueDetailsFragment());
    }

    @Override
    protected void setTitle() {
        getSupportActionBar().setTitle("Value Detail");
    }
}