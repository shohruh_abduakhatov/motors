package com.motors.phone.app.login.model;

import com.google.gson.annotations.SerializedName;
import com.example.nasimxon.components.utils.Constants;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class LoginModel implements Serializable {
    @SerializedName("Password")
    private String password;
    @SerializedName("Username")
    private String email;
    @SerializedName(Constants.Ticket)
    private String ticket;
    @SerializedName("ProviderName")
    private String socialProviderType;
}
