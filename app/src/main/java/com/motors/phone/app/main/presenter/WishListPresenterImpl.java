package com.motors.phone.app.main.presenter;

import com.motors.phone.app.main.model.WishListModel;
import com.motors.phone.app.main.view.WishListFragment;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Achilov Bakhrom on 9/13/17.
 */

public class WishListPresenterImpl implements WishListPresenter {
    private ApiManager apiManager;
    private WishListFragment fragment;

    @Inject
    public WishListPresenterImpl(WishListFragment fragment, ApiManager apiManager) {
        this.fragment = fragment;
        this.apiManager = apiManager;
    }

    @Override
    public void fillWishList() {
        apiManager
                .getWishCars()
                .subscribe(new Observer<List<WishListModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<WishListModel> wishListModels) {
                        fragment.getAdapter().addAll(wishListModels);
                    }
                });
    }
}