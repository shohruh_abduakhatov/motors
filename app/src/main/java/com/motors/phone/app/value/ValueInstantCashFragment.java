package com.motors.phone.app.value;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motors.phone.R;
import com.motors.phone.util.FragmentManagerUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ValueInstantCashFragment extends Fragment {
    public ValueInstantCashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.value_screen_instant_cash, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnNext)
    public void next() {
        FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new ValuePreviewFragment());
    }

    @OnClick(R.id.tvBack2)
    public void back() {
        FragmentManagerUtils.popBackstack((AppCompatActivity) getActivity());
    }
}
