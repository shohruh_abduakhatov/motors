package com.motors.phone.app.notification.presenter;

import com.motors.phone.app.main.model.NotificationModel;
import com.motors.phone.app.notification.NotificationsFragment;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Nasimxon on 9/6/17.
 */


public class NotificationPresenterImpl implements NotificationPresenter {

    private NotificationsFragment notificationsFragment;
    private ApiManager apiManager;

    @Inject
    public NotificationPresenterImpl(NotificationsFragment notificationsFragment, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.notificationsFragment = notificationsFragment;
    }

    @Override
    public void fillNotifications() {
        apiManager.getNotification()
                .subscribe(new Observer<List<NotificationModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<NotificationModel> recentSearchModels) {
                        notificationsFragment.getAdapterNotification().addAll(recentSearchModels);
                        notificationsFragment.getAdapterNotification().addAll(recentSearchModels);
                        notificationsFragment.getAdapterNotification().addAll(recentSearchModels);
                        notificationsFragment.getAdapterNotification().addAll(recentSearchModels);
                        notificationsFragment.getAdapterNotification().addAll(recentSearchModels);
                    }
                });
    }
}