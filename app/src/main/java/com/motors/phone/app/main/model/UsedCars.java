package com.motors.phone.app.main.model;

import lombok.Data;

/**
 * Created by bakhrom on 9/13/17.
 */

@Data
public class UsedCars {

    private String carName;
    private String carDescription;
    private String carImageURL;
    private String carPrice;
    private String mileage;
    private String fuelType;
    private String date;

}
