package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Dimension;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.common.EngineSizeType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by Nasimxon on 10/25/2017.
 */

public class EngineFragment extends BaseSearchFragment {
    @Inject
    SearchData searchData;

    @BindView(R.id.rgEngineSize)
    RadioGroup rgEngineSize;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.engine_fragment, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    private void init() {
        EngineSizeType [] sizes = EngineSizeType.values();
        for (EngineSizeType size : sizes) {
            RadioButton radioButton = new RadioButton(getContext());
            radioButton.setText(size.toString());
            radioButton.setTextSize(Dimension.SP, 18);
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 15, 0, 0);
            radioButton.setLayoutParams(layoutParams);
            radioButton.setId(size.getId());
            rgEngineSize.addView(radioButton);
        }
        rgEngineSize.check(EngineSizeType.getEngineByKey(searchData.getSelectedEngineSize()).getId());
        rgEngineSize.setOnCheckedChangeListener((radioGroup, i) -> {
            searchData.setSelectedEngineSize(EngineSizeType.getEngineById(i).getKey());
        });
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        rgEngineSize.check(EngineSizeType.Any.getId());
        searchData.setSelectedEngineSize(EngineSizeType.Any.getKey());
    }

    @Override
    public void saveSearches() {}
}