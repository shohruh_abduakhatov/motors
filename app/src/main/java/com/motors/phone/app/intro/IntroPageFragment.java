package com.motors.phone.app.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nasimxon.components.custom_ui.CircleIndicator;
import com.motors.phone.R;
import com.motors.phone.app.login.LoginActivity;
import com.motors.phone.core.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nasimxon on 10/8/2017.
 */

public class IntroPageFragment extends BaseFragment {
    @BindView(R.id.vpIntroPage)
    ViewPager vpIntroPage;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_intro_page, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        vpIntroPage.setPageTransformer(true, new ZoomOutTranformer());
        vpIntroPage.setAdapter(new WizardPagerAdapter());
        indicator.setWithViewPager(vpIntroPage);
        vpIntroPage.setOffscreenPageLimit(5);
    }

    @OnClick(R.id.tvSkip)
    public void clickSkip() {
        ((LoginActivity) getActivity()).finishIntro();
    }

    @OnClick(R.id.btnNext)
    public void clickNext() {
        if (vpIntroPage.getCurrentItem() == vpIntroPage.getAdapter().getCount() - 1) {
            ((LoginActivity) getActivity()).finishIntro();
        } else
            vpIntroPage.setCurrentItem(vpIntroPage.getCurrentItem() + 1, true);
    }

    public class WizardPagerAdapter extends PagerAdapter {
        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            ViewGroup layout = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.intro_pager_layout, collection, false);
            CircleImageView imageView = layout.findViewById(R.id.civIntro);
            TextView tvIntroBold = layout.findViewById(R.id.tvIntroBold);
            TextView tvIntroDescription = layout.findViewById(R.id.tvIntroDescription);
            switch (position) {
                case 0:
                    imageView.setImageResource(R.drawable.onboarding1);
                    tvIntroBold.setText("Search thousands of used cars");
                    tvIntroDescription.setText("Find the right car for you");
                    break;
                case 1:
                    imageView.setImageResource(R.drawable.onboarding2);
                    tvIntroBold.setText("Advertise your car for free");
                    tvIntroDescription.setText("Get your car seen by thousands of online visitors");
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.onboarding3);
                    tvIntroBold.setText("Instant Cash Offer");
                    tvIntroDescription.setText("Get a guaranteed valuation for your car");
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.onboarding4);
                    tvIntroBold.setText("Car ownership made easy");
                    tvIntroDescription.setText("Get reminders for important dates such as MOTs, Tax, Insurance and Servicing");
                    break;
                default:
                    imageView.setImageResource(R.drawable.onboarding5);
                    tvIntroBold.setText("Connect with friends");
                    tvIntroDescription.setText("Share your online \"canvas\" and shortlisted vehicles with friends");
            }
            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
