package com.motors.phone.app.main.presenter;

import com.google.gson.Gson;
import com.motors.phone.app.main.model.NearestDealersModel;
import com.motors.phone.app.main.model.NotificationModel;
import com.motors.phone.app.main.model.RecentSearchModel;
import com.motors.phone.app.main.model.SinceAwayModel;
import com.motors.phone.app.main.model.WishListModel;
import com.motors.phone.app.main.view.MainFragment;
import com.motors.phone.common.net_objects.FullDetail;
import com.motors.phone.common.net_objects.MotorsResponse;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by bakhrom on 9/6/17.
 */


public class MainPresenterImpl implements MainPresenter {

    private MainFragment mainFragment;
    private ApiManager apiManager;

    @Inject
    public MainPresenterImpl(MainFragment mainFragment, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.mainFragment = mainFragment;
    }

    @Override
    public void fillNotifications() {
        apiManager.getNotification()
                .subscribe(new Observer<List<NotificationModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<NotificationModel> recentSearchModels) {
                        mainFragment.getAdapterNotifications().clear();
                        mainFragment.getAdapterNotifications().addAll(recentSearchModels);
                    }
                });
    }

    @Override
    public void fillSinceYouWereAway() {
        apiManager.getSinceAway()
                .subscribe(new Observer<List<SinceAwayModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<SinceAwayModel> recentSearchModels) {
                        mainFragment.getAdapterSinceAway().clear();
                        mainFragment.getAdapterSinceAway().addAll(recentSearchModels);
                    }
                });
    }

    @Override
    public void fillRecentSearches() {
        apiManager.getRecentSearch()
                .subscribe(new Observer<List<RecentSearchModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<RecentSearchModel> recentSearchModels) {
                        mainFragment.getAdapterRecent().clear();
                        mainFragment.getAdapterRecent().addAll(recentSearchModels);
                    }
                });
    }

    @Override
    public void fillSavedSearches() {
        apiManager.getSavedSearch()
                .subscribe(new Observer<List<WishListModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<WishListModel> recentSearchModels) {
                        mainFragment.getAdapterSavedReserch().clear();
                        mainFragment.getAdapterSavedReserch().addAll(recentSearchModels);
                    }
                });
    }

    @Override
    public void fillNearestDealers() {
        apiManager.getNearestDealer()
                .subscribe(new Observer<List<NearestDealersModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<NearestDealersModel> recentSearchModels) {
                        mainFragment.getAdapterNearest().clear();
                        mainFragment.getAdapterNearest().addAll(recentSearchModels);
                    }
                });
    }

}
