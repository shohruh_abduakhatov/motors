package com.motors.phone.app.buy.presenter;

/**
 * Created by Nasimxon on 9/6/17.
 */

public interface BuyPresenter {
    void fillBuyCars();
}