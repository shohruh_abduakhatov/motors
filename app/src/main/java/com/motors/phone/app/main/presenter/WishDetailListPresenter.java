package com.motors.phone.app.main.presenter;

/**
 * Created by Achilov Bakhrom on 9/13/17.
 */

public interface WishDetailListPresenter {
    void fillWishList();
}

