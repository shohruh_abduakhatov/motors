package com.motors.phone.app.login.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class LoginResponse implements Serializable {
    @SerializedName("UID")
    private String uid;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
}
