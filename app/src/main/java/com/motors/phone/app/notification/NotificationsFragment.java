package com.motors.phone.app.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.main.presenter.adapters.NotificationsHolder;
import com.motors.phone.app.notification.presenter.NotificationPresenter;
import com.motors.phone.app.notification.presenter.NotificationPresenterImpl;
import com.motors.phone.core.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/18/17.
 */

public class NotificationsFragment extends BaseFragment implements NotificationPresenter {
    @BindView(R.id.ervNotifications)
    EasyRecyclerView ervNotifications;
    @Getter
    RecyclerArrayAdapter adapterNotification;
    @Inject
    NotificationPresenterImpl notificationPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notifications_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        ervNotifications.getRecyclerView().setNestedScrollingEnabled(false);
        ervNotifications.setLayoutManager(new LinearLayoutManager(getContext()));
        ervNotifications.setAdapter(adapterNotification = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new NotificationsHolder(parent, R.layout.item_notifications);
            }
        });
        fillNotifications();
    }

    @Override
    public void fillNotifications() {
        notificationPresenter.fillNotifications();
    }
}