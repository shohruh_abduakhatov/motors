package com.motors.phone.app.message.presenter;

import com.motors.phone.app.message.MessageFragment;
import com.motors.phone.app.message.model.MessagesModel;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Nasimxon on 9/6/17.
 */


public class MessagePresenterImpl implements MessagePresenter {

    private MessageFragment messageFragment;
    private ApiManager apiManager;

    @Inject
    public MessagePresenterImpl(MessageFragment messageFragment, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.messageFragment = messageFragment;
    }

    @Override
    public void fillMessages() {
        apiManager.getMessages()
                .subscribe(new Observer<List<MessagesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<MessagesModel> recentSearchModels) {
                        messageFragment.getAdapterMessages().addAll(recentSearchModels);
                    }
                });
    }
}
