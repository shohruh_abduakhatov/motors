package com.motors.phone.app.buy;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.buy.presenter.BuyPresenter;
import com.motors.phone.app.buy.presenter.BuyPresenterImpl;
import com.motors.phone.app.buy.presenter.adapters.BuyCarsHolder;
import com.motors.phone.app.search.SearchActivity;
import com.motors.phone.app.search.SearchFragment;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/5/17.
 */

public class BuyFragment extends BaseFragment implements BuyPresenter, ScrollView.OnTouchListener {
    @Inject
    BuyPresenterImpl buyPresenter;
    @Getter
    @BindView(R.id.ervBuyCars)
    EasyRecyclerView ervBuyCars;
    @Getter
    RecyclerArrayAdapter adapterCars;
    @BindView(R.id.ivChangeMode)
    ImageView ivChangeMode;

    @BindView(R.id.llFilter)
    LinearLayout btnFilter;

    int clickCounter = 0;

    Dialog dialog = null;


    float touch_up = 0;
    float pos = 0.0f;


    private boolean mode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buy_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wishlist_dialog);
        dialog.findViewById(R.id.btnExit).setOnClickListener((view) -> dialog.dismiss());
        ervBuyCars.setOnTouchListener(this);
        pos = btnFilter.getTranslationY();
        ervBuyCars.setLayoutManager(new LinearLayoutManager(getContext()));
        ervBuyCars.setAdapter(adapterCars = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new BuyCarsHolder(parent, R.layout.item_used_cars);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

                final ImageView heart = holder.itemView.findViewById(R.id.heart);
                heart.setOnClickListener((view -> {

                    clickCounter++;
                    if (clickCounter % 2 != 0) {
                        heart.setImageResource(R.drawable.ic_filled_heart);
                        dialog.show();
                    } else {
                        heart.setImageResource(R.drawable.ic_empty_heart);
                        dialog.show();
                        clickCounter = 0;
                    }
                }));
            }
        });
        fillBuyCars();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE || motionEvent.getAction() == MotionEvent.ACTION_SCROLL) {
            if (touch_up > motionEvent.getY()) {
                ObjectAnimator move = ObjectAnimator.ofFloat(btnFilter, View.TRANSLATION_Y, size.y);
                move.setDuration(480);
                move.start();
            } else {
                ObjectAnimator move = ObjectAnimator.ofFloat(btnFilter, View.TRANSLATION_Y, pos * -1);
                move.setDuration(480);
                move.start();
            }

            touch_up = motionEvent.getY();
        }
        return false;
    }

    @Override
    public void fillBuyCars() {
        buyPresenter.fillBuyCars();
        clickItemBuyCar();
    }

    @OnClick(R.id.ivChangeMode)
    public void clickIvChangeMode() {
        if (mode) {
            ivChangeMode.setImageResource(R.drawable.vector_layout_grid);
            ervBuyCars.setAdapter(adapterCars = new RecyclerArrayAdapter(getContext()) {
                @Override
                public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                    return new BuyCarsHolder(parent, R.layout.item_used_cars);
                }
            });
        } else {
            ivChangeMode.setImageResource(R.drawable.vector_layout_list);
            ervBuyCars.setAdapter(adapterCars = new RecyclerArrayAdapter(getContext()) {
                @Override
                public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                    return new BuyCarsHolder(parent, R.layout.item_used_cars2);
                }
            });
        }
        mode = !mode;
        fillBuyCars();
        clickItemBuyCar();
    }

    @OnClick(R.id.llFilter)
    public void clickFilter() {
        SearchActivity.start(getActivity());
    }

    private void clickItemBuyCar() {
        adapterCars.setOnItemClickListener(position -> {
            BuyDetailActivity.start(getActivity());
        });
    }
}