package com.motors.phone.app.ownership.presenter;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public interface OwnershipPresenter {
    void fillLocalGarages();
}
