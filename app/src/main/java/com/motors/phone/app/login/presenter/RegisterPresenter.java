package com.motors.phone.app.login.presenter;

import com.motors.phone.app.login.model.RegisterModel;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public interface RegisterPresenter {
    void fillRegister(RegisterModel user);
    void fillAddressPostCode(String postCode);
}
