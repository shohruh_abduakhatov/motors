package com.motors.phone.app.ownership.presenter;

import com.motors.phone.app.main.model.NearestDealersModel;
import com.motors.phone.app.ownership.OwnershipFragment;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Nasimxon on 9/6/17.
 */


public class OwnershipPresenterImpl implements OwnershipPresenter {

    private OwnershipFragment ownershipFragment;
    private ApiManager apiManager;

    @Inject
    public OwnershipPresenterImpl(OwnershipFragment ownershipFragment, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.ownershipFragment = ownershipFragment;
    }

    @Override
    public void fillLocalGarages() {
        apiManager.getNearestDealer()
                .subscribe(new Observer<List<NearestDealersModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<NearestDealersModel> recentSearchModels) {
                        ownershipFragment.getAdapterGarages().clear();
                        ownershipFragment.getAdapterGarages().addAll(recentSearchModels);
                    }
                });
    }
}