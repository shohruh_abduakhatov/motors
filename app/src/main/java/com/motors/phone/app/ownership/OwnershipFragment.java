package com.motors.phone.app.ownership;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.intro.BaseTransformer;
import com.motors.phone.app.main.presenter.adapters.NearestDealersHolder;
import com.motors.phone.app.ownership.presenter.OwnershipPresenter;
import com.motors.phone.app.ownership.presenter.OwnershipPresenterImpl;
import com.motors.phone.app.ownership.presenter.adapters.SlidingImageAdapter;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by bakhrom on 9/5/17.
 */

public class OwnershipFragment extends BaseFragment implements OwnershipPresenter {
    @BindView(R.id.ervLocalGarages)
    EasyRecyclerView ervLocalGarages;
    @Getter
    RecyclerArrayAdapter adapterGarages;
    @Inject
    OwnershipPresenterImpl ownershipPresenter;
    int[] layouts = {R.layout.item_pager, R.layout.item_pager2, R.layout.item_pager3};
    @BindView(R.id.pager)
    ViewPager pager;

    SlidingImageAdapter adapter = null;

    @BindView(R.id.back_arrow)
    ImageView backArrow;

    @BindView(R.id.forward_arrow)
    ImageView forwardArrow;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ownership_fragment, container, false);
        ButterKnife.bind(this, view);
        adapter = new SlidingImageAdapter(layouts, getContext());
        pager.setAdapter(adapter);
        pager.setPageTransformer(true, new ZoomOutSlideTransformer());

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (pager.getCurrentItem() == 0) {
                    backArrow.setVisibility(View.INVISIBLE);
                } else if (pager.getCurrentItem() == adapter.getCount() - 1) {
                    forwardArrow.setVisibility(View.INVISIBLE);
                } else {
                    forwardArrow.setVisibility(View.VISIBLE);
                    backArrow.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initUI();
        return view;
    }

    private void initUI() {
        ervLocalGarages.setLayoutManager(new LinearLayoutManager(getContext()));
        ervLocalGarages.setAdapter(adapterGarages = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new NearestDealersHolder(parent, R.layout.item_nearest_dealer);
            }
        });
        fillLocalGarages();
    }

    @OnClick(R.id.back_arrow)
    public void back() {

        if (pager.getCurrentItem() > 0) {
            forwardArrow.setVisibility(View.VISIBLE);
            pager.setCurrentItem(pager.getCurrentItem() - 1, true);
        } else {
            backArrow.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.forward_arrow)
    public void forward() {

        if (pager.getCurrentItem() < adapter.getCount() - 1) {
            backArrow.setVisibility(View.VISIBLE);
            pager.setCurrentItem(pager.getCurrentItem() + 1, true);
        } else {
            forwardArrow.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.pager)
    public void go() {
        FragmentManagerUtils.replaceFragment((AppCompatActivity) getActivity(), R.id.flMain, new OwnershipDetailFragment());
    }

    @Override
    public void fillLocalGarages() {
        ownershipPresenter.fillLocalGarages();
    }

    public class ZoomOutSlideTransformer extends BaseTransformer {

        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        @Override
        protected void onTransform(View view, float position) {
            if (position >= -1 || position <= 1) {
                // Modify the default slide transition to shrink the page as well
                final float height = view.getHeight();
                final float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                final float vertMargin = height * (1 - scaleFactor) / 2;
                final float horzMargin = view.getWidth() * (1 - scaleFactor) / 2;

                // Center vertically
                view.setPivotY(0.5f * height);

                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA));
            }
        }

    }
}