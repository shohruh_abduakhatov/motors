package com.motors.phone.app.message.presenter.adapters;

import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.message.model.MessagesModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class MessagesHolder extends BaseViewHolder<MessagesModel> {

    public MessagesHolder(ViewGroup parent, int res) {
        super(parent, res);
    }

    @Override
    public void setData(MessagesModel data) {
        super.setData(data);
    }

}