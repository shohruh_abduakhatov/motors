package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.FragmentManagerUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public class VehicleSpecFragment extends BaseSearchFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vehicle_spec_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {}

    @OnClick(value = {R.id.llEngineSize, R.id.llNumberOfDoors, R.id.llNumberOfSeats, R.id.llSafetyRatings, R.id.llTowingWeight})
    public void clickSection(View v) {
        switch (v.getId()) {
            case R.id.llEngineSize: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new EngineFragment());
                break;
            }
            case R.id.llNumberOfDoors: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, JustListFragment.getInstance(Constants.Doors));
                break;
            }
            case R.id.llNumberOfSeats: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, JustListFragment.getInstance(Constants.Seats));
                break;
            }
            case R.id.llSafetyRatings: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, JustListFragment.getInstance(Constants.SafetyRatings));
                break;
            }
            case R.id.llTowingWeight: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new TowingWeightFragment());
                break;
            }
        }
    }
    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void saveSearches() {}
    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.VEHICLE_SPEC));
    }
}