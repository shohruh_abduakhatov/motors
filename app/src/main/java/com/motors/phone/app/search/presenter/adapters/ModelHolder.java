package com.motors.phone.app.search.presenter.adapters;

import android.support.v7.widget.AppCompatCheckBox;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;
import com.motors.phone.common.net_objects.ChoiceItem;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class ModelHolder extends BaseViewHolder<ChoiceItem> {
    private TextView tvName;
    private AppCompatCheckBox chbOptions;
    private int color;

    public ModelHolder(ViewGroup parent, int res) {
        super(parent, res);
        chbOptions = $(R.id.chbOption2);
        tvName = $(R.id.tvName2);
        color = tvName.getTextColors().getDefaultColor();
    }

    @Override
    public void setData(ChoiceItem data) {
        super.setData(data);
        tvName.setText(String.format("%s (%d)", data.getName(), data.getValue()));
        chbOptions.setChecked(data.isSelected());
        tvName.setTextColor(color);
        chbOptions.setEnabled(true);
        if (data.getValue() <= 0) {
            tvName.setTextColor(getContext().getResources().getColor(R.color.grey));
            chbOptions.setEnabled(false);
        }

    }
}