package com.motors.phone.app.main.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.main.presenter.WishListPresenter;
import com.motors.phone.app.main.presenter.WishListPresenterImpl;
import com.motors.phone.app.main.presenter.adapters.WishListHolder;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Achilov Bakhrom on 9/16/17.
 */

public class WishListFragment extends BaseFragment implements WishListPresenter {
    @Getter
    RecyclerArrayAdapter adapter;

    @Inject
    WishListPresenterImpl wishListPresenter;
    @Getter
    @BindView(R.id.ervWishlist)
    EasyRecyclerView easyRecyclerView;
    Dialog dialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wish_list_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        easyRecyclerView.getRecyclerView().setNestedScrollingEnabled(false);
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.create_wishlist_dialog);
        dialog.findViewById(R.id.btnExit).setOnClickListener((view) -> dialog.dismiss());
        easyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        easyRecyclerView.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new WishListHolder(parent, R.layout.item_wishlist);
            }
        });
        adapter.addHeader(new HeaderItem());
        adapter.setOnItemClickListener(position -> FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new WishListDetailFragment()));
        fillWishList();
    }

    @Override
    public void fillWishList() {
        wishListPresenter.fillWishList();
    }

    class HeaderItem implements RecyclerArrayAdapter.ItemView {
        @Override
        public View onCreateView(ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.item_wish_header, parent, false);
            view.findViewById(R.id.flButton).setOnClickListener(v -> {
                dialog.show();
            });

            return view;
        }

        @Override
        public void onBindView(View headerView) {
        }
    }
}