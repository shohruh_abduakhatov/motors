package com.motors.phone.app.message;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.message.model.ChatMessagesModel;
import com.motors.phone.app.message.presenter.ChatMessagePresenter;
import com.motors.phone.app.message.presenter.ChatMessagePresenterImpl;
import com.motors.phone.app.message.presenter.adapters.ChatMessagesHolder;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/23/2017.
 */

public class ChatActivity extends AppCompatActivity implements ChatMessagePresenter {
    @BindView(R.id.ervChatMessage)
    EasyRecyclerView ervChatMessage;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etChatMessage)
    EditText etChatMessage;
    @Getter
    RecyclerArrayAdapter adapterMessages;

    @Inject
    ChatMessagePresenterImpl chatMessagePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_message_activity);
        ButterKnife.bind(this);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

//            TODO Implement the layout according to the orientation
        }
        initUI();
    }

    private void initUI() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ervChatMessage.setLayoutManager(new LinearLayoutManager(this));
        ervChatMessage.setAdapter(adapterMessages = new RecyclerArrayAdapter(this) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ChatMessagesHolder(parent, viewType == 1 ?
                        R.layout.item_chat_message_2 : R.layout.item_chat_message);
            }

            @Override
            public int getViewType(int position) {
                return position % 3 == 1 ? 1 : 0;
            }
        });
        fillMessages();
        etChatMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ervChatMessage.scrollToPosition(adapterMessages.getCount() - 1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @OnClick(R.id.tvSend)
    public void clickSend() {
        if (!etChatMessage.getText().toString().trim().isEmpty()) {
            etChatMessage.postDelayed(() -> {
                ChatMessagesModel model = new ChatMessagesModel();
                model.setTxt1("" + etChatMessage.getText().toString());
                adapterMessages.add(model);
                ervChatMessage.scrollToPosition(adapterMessages.getCount() - 1);
                etChatMessage.setText("");
            }, 500);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void fillMessages() {
        chatMessagePresenter.fillMessages();
    }
}