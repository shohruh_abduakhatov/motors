package com.motors.phone.app.search.presenter.adapters;

import android.animation.ValueAnimator;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;
import com.motors.phone.app.search.ModelTrimPagerFragment;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.util.FragmentManagerUtils;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class ChoiceHolder extends BaseViewHolder<ChoiceItem> {
    private AppCompatCheckBox chbOptions;
    private TextView tvName;
    private ImageView ivFilter;
    private boolean isMakeFragment;

    public ChoiceHolder(ViewGroup parent, int res, boolean isMakeFragment) {
        super(parent, res);
        chbOptions = $(R.id.chbOptions);
        tvName = $(R.id.tvName);
        ivFilter = $(R.id.ivFilter);
        this.isMakeFragment = isMakeFragment;
    }

    @Override
    public void setData(ChoiceItem data) {
        tvName.setText(stringWithSpaces(data.getName()) + " (" + data.getValue() + ")");
        chbOptions.setSelected(data.isSelected());
        if (isMakeFragment) {
            ivFilter.setVisibility(data.isSelected() ? View.VISIBLE : View.INVISIBLE);
            ivFilter.setOnClickListener(view ->
                    FragmentManagerUtils.addFragment((AppCompatActivity) getContext(), R.id.flMain,
                            ModelTrimPagerFragment.getInstance(data.getName())));
        }
        if (data.getValue() == 0)
            tvName.setTextColor(ContextCompat.getColor(getContext(), R.color.silver));
        else tvName.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        if (data.isWithAnim()) startAnimation();
        data.setWithAnim(false);
    }

    private String stringWithSpaces(final String text) {
        StringBuilder stringBuilder = new StringBuilder(text);
        int size = stringBuilder.length();
        if (stringBuilder.toString().equalsIgnoreCase("RecentlyAddedVehicle") ||
                stringBuilder.toString().equalsIgnoreCase("ReducedVehicle")) {
            for (int i = 0; i < size; i++) {
                if (i >= 1 && Character.isUpperCase(stringBuilder.charAt(i))) {
                    stringBuilder.insert(i++, " ");
                    size++;
                }
            }
        }
        return stringBuilder.toString();
    }

    private void startAnimation() {
        ValueAnimator animator = ValueAnimator.ofFloat(1f, 0.8f);
        animator.addUpdateListener(valueAnimator -> {
            float value = (float) valueAnimator.getAnimatedValue();
            itemView.setScaleX(value > 0.9f ? value : (1.8f - value));
            itemView.setScaleY(value > 0.9f ? value : (1.8f - value));
            itemView.setAlpha(value > 0.9f ? value : (1.8f - value));
        });
        animator.setDuration(200);
        animator.start();
    }
}