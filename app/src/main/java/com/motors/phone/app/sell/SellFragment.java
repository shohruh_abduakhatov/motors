package com.motors.phone.app.sell;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motors.phone.R;
import com.motors.phone.core.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nasimxon on 9/5/17.
 */

public class SellFragment extends BaseFragment {
    private static final int CAMERA_REQUEST = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selling_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnGo)
    public void go() {
        SellDetailActivity.start(getActivity());
    }

    @OnClick(R.id.tvUseNumberPlate)
    public void clickUseNumber() {
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
}