package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.github.aakira.expandablelayout.ExpandableLayoutListener;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.ExpandableWeightLayout;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.ChoiceHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.ChoicesComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public class MoreOptionsFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    JustListPresenterImp<MoreOptionsFragment> presenterImp;
    @Inject
    SearchData searchData;
    @BindView(R.id.btnSellers)
    AppCompatButton btnSellers;
    @BindView(R.id.llAllDealerRecommended)
    ExpandableLinearLayout llAllDealerRecommended;
    @BindView(R.id.btnDealer)
    AppCompatButton btnDealer;
    @BindView(R.id.btnPrivate)
    AppCompatButton btnPrivate;
    @BindView(R.id.swExclude)
    SwitchCompat swExclude;
    @BindView(R.id.ervOtherOptions)
    EasyRecyclerView ervOtherOptions;
    RecyclerArrayAdapter<ChoiceItem> adapter;

    private List<ChoiceItem> options;
    private List<ChoiceItem> selectOptions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_options_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        options = new ArrayList<>();
        selectOptions = new ArrayList<>();
        btnSellers.setSelected(true);
        ervOtherOptions.setLayoutManager(new LinearLayoutManager(getContext()));
        ervOtherOptions.setAdapter(adapter = new RecyclerArrayAdapter<ChoiceItem>(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ChoiceHolder(parent, R.layout.item_makes, false);
            }
        });
        adapter.setOnItemClickListener(position -> {
            if (options.get(position).getValue() == 0) return;
            options.get(position).setWithAnim(true);
            options.get(position).setSelected(!options.get(position).isSelected());
            if (options.get(position).isSelected())
                selectOptions.add(options.get(position));
            else selectOptions.remove(options.get(position));
            adapter.notifyItemChanged(position);
            saveSearches();
        });
        fillLoadList(Constants.OtherOptions);
        swExclude.setOnCheckedChangeListener((compoundButton, b) -> {
            searchData.setExcluded(b);
        });

    }

    @OnClick(value = {R.id.btnSellers, R.id.btnDealer, R.id.btnPrivate})
    public void onClicked(View view) {
        view.setSelected(true);
        switch (view.getId()) {
            case R.id.btnSellers:
                if (!llAllDealerRecommended.isExpanded()) llAllDealerRecommended.expand();
                btnDealer.setSelected(false);
                btnPrivate.setSelected(false);
                break;
            case R.id.btnDealer:
                if (!llAllDealerRecommended.isExpanded()) llAllDealerRecommended.expand();
                btnSellers.setSelected(false);
                btnPrivate.setSelected(false);
                break;
            case R.id.btnPrivate:
                if (llAllDealerRecommended.isExpanded()) llAllDealerRecommended.collapse();
                btnDealer.setSelected(false);
                btnSellers.setSelected(false);
                break;
        }
        llAllDealerRecommended.setListener(new ExpandableLayoutListener() {
            @Override
            public void onAnimationStart() {}
            @Override
            public void onAnimationEnd() {
                if (view.getId() == R.id.btnPrivate)
                    llAllDealerRecommended.collapse();
                else llAllDealerRecommended.expand();
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onPreClose() {
            }

            @Override
            public void onOpened() {
            }

            @Override
            public void onClosed() {
            }
        });
    }

    @Override
    public void setData(List<ChoiceItem> list) {
        this.options = list;
        Collections.sort(list, new ChoicesComparator(list.size()));
        if (searchData.getOtherOptions() != null && !searchData.getOtherOptions().isEmpty())
            for (ChoiceItem otherItem : options) {
                for (ChoiceItem choiceItem : searchData.getOtherOptions()) {
                    if (otherItem.getName().equalsIgnoreCase(choiceItem.getName())) {
                        selectOptions.add(otherItem);
                        otherItem.setSelected(true);
                    }
                }
            }
        adapter.addAll(list);
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        swExclude.setChecked(false);
        searchData.setOtherOptions(new ArrayList<>());
        selectOptions.clear();
        for (ChoiceItem option : options) {
            option.setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectOptions.size(); i++) {
            if (selectOptions.get(i).isSelected()) choiceItems.add(selectOptions.get(i));
        }
        searchData.setOtherOptions(choiceItems);
    }

    @Override
    public void fillLoadList(String type) {
        presenterImp.fillLoadList(type);
    }

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.MORE_OPTIONS));
    }
}