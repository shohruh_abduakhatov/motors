package com.motors.phone.app.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.ValidatorUtils;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.motors.phone.R;
import com.motors.phone.app.login.model.RegisterModel;
import com.motors.phone.app.login.presenter.RegisterPresenter;
import com.motors.phone.app.login.presenter.RegisterPresenterImpl;
import com.motors.phone.common.SalutationType;
import com.motors.phone.common.net_objects.User;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class RegisterFragment extends BaseFragment implements RegisterPresenter, EditText.OnEditorActionListener {
    @Inject
    RegisterPresenterImpl registerPresenter;
    @BindView(R.id.tvBack)
    View tvBack;
    @BindView(R.id.spSalutationType)
    AppCompatSpinner spSalutationType;
    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etHouseNumber)
    EditText etHouseNumber;
    @BindView(R.id.etMobileNumber)
    EditText etNumber;
    @BindView(R.id.etPostCode)
    EditText etPostCode;
    @BindView(R.id.chbAcceptTerms)
    CheckBox chbAcceptTerms;
    @BindView(R.id.chbCarefullySelect)
    CheckBox chbCarefullySelect;
    @BindView(R.id.chbUpdates)
    CheckBox chbUpdates;
    @BindView(R.id.chbLatestNews)
    CheckBox chbLatestNews;
    @BindView(R.id.chbCompetitions)
    CheckBox chbCompetitions;
    @BindView(R.id.expAddress)
    ExpandableLinearLayout expAddress;
    @Getter
    @BindView(R.id.etStreetName)
    EditText etStreetName;
    @Getter
    @BindView(R.id.etCity)
    EditText etCity;
    @Getter
    @BindView(R.id.etCountry)
    EditText etCountry;
    @Getter
    @BindView(R.id.etEmail)
    EditText etEmail;

    private User user;

    public static RegisterFragment getInstance(User user) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.UserData, user);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        user = (User) getArguments().getSerializable(Constants.UserData);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_register, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        etEmail.setOnEditorActionListener(this);
        etFirstName.setOnEditorActionListener(this);
        etLastName.setOnEditorActionListener(this);
        etNumber.setOnEditorActionListener(this);
        etHouseNumber.setOnEditorActionListener(this);
        etPostCode.setOnEditorActionListener(this);
        ArrayAdapter<String> saluttationAdapter = new ArrayAdapter<>(getContext(), R.layout.item_spinner);
        saluttationAdapter.add("Select");
        for (SalutationType item : SalutationType.values()) {
            saluttationAdapter.add(item.getName());
        }
        spSalutationType.setAdapter(saluttationAdapter);
        if (user.getTicket() != null) {
            etEmail.setVisibility(View.VISIBLE);
            etEmail.setText("" + user.getEmail() != null ? user.getEmail() : "");
            etLastName.setText(user.getUserName());
            etFirstName.setText(user.getUserName());
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getImeActionId()) {
            case 1:
                etLastName.requestFocus();
                return true;
            case 2:
                etNumber.requestFocus();
                return true;
            case 3:
                etHouseNumber.requestFocus();
                return true;
            case 4:
                etPostCode.requestFocus();
                return true;
            case 5:
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(etPostCode.getWindowToken(), 0);
                return true;
        }
        return false;
    }

    @OnClick(value = {R.id.btnLookUp, R.id.btnManualEntry})
    public void lookUpOnClick(View v) {
        if (v.getId() == R.id.btnManualEntry) {
            expAddress.expand();
        } else {
            if (!ValidatorUtils.isValidPostcode(etPostCode.getText().toString().trim())) {
                makeAlertText(etPostCode, "Postcode is required for address!");
            } else {
                fillAddressPostCode(etPostCode.getText().toString());
                expAddress.expand();
            }
        }
    }

    @OnClick(R.id.tvBack)
    public void clickBack() {
        FragmentManagerUtils.popBackstack((AppCompatActivity) getActivity());
    }

    @OnClick(R.id.btnRegister)
    public void clickRegister() {
        if (isValidate()) {
            RegisterModel regmodel = new RegisterModel();
            regmodel.setFirstName(etFirstName.getText().toString());
            regmodel.setLastName(etLastName.getText().toString());
            regmodel.setPostcode(etPostCode.getText().toString().trim().isEmpty()
                    ? Constants.DEFAULT_POSTCODE : etPostCode.getText().toString());
            regmodel.setEmail(user.getEmail());
            regmodel.setPassword(user.getPassword());
            regmodel.setProvider(user.getSocialProviderType().getName());
            regmodel.setPhoneNumber(etNumber.getText().toString());
            regmodel.setCompetitions(chbCompetitions.isChecked());
            regmodel.setTermsAccepted(true);
            regmodel.setMotorsUpdates(chbUpdates.isChecked());
            regmodel.setThirdParties(chbCarefullySelect.isChecked());
            regmodel.setLatestNews(chbLatestNews.isChecked());
            fillRegister(regmodel);
        }
    }

    @Override
    public void fillRegister(RegisterModel user) {
        registerPresenter.fillRegister(user);
    }

    @Override
    public void fillAddressPostCode(String postCode) {
        registerPresenter.fillAddressPostCode(postCode);
    }

    public boolean isValidate() {
        if (etFirstName.getText().toString().trim().isEmpty()) {
            makeAlertText(etFirstName, "First name is required!");
            return false;
        } else if (etLastName.getText().toString().trim().isEmpty()) {
            makeAlertText(etLastName, "Last name is required!");
            return false;
        } else if (!TextUtils.isEmpty(etNumber.getText().toString())) {
            Integer error = ValidatorUtils.alertMsgUKNumber(etNumber.getText().toString());
            if (error != null) {
                makeAlertText(etNumber, error);
                return false;
            }
        }
        return true;
    }
}