package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.nasimxon.components.custom_ui.PostCodeDialog;
import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.example.nasimxon.components.utils.ValidatorUtils;
import com.github.aakira.expandablelayout.ExpandableWeightLayout;
import com.motors.phone.R;
import com.motors.phone.common.MilesType;
import com.motors.phone.common.SearchPanelType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.FragmentManagerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Nasimxon on 9/18/17.
 */

public class SearchFragment extends BaseSearchFragment implements TextWatcher, AdapterView.OnItemSelectedListener {
    @BindView(R.id.rootLinear)
    ExpandableWeightLayout rootLinear;
    @BindView(R.id.spDistance)
    Spinner spDistance;
    @BindView(R.id.etPostCode)
    EditText etPostCode;
    @BindView(R.id.btnCars)
    AppCompatButton btnCars;
    @BindView(R.id.btnVans)
    AppCompatButton btnVans;
    @BindView(R.id.llVehicleSpec)
    LinearLayout vehicle;
    @BindView(R.id.llPerformance)
    LinearLayout performance;
    @BindView(R.id.first)
    LinearLayout first_layout;
    @BindView(R.id.second)
    LinearLayout second_layout;
    @BindView(R.id.llKeywords)
    ViewGroup keyword;
    @BindView(R.id.llMoreOptions)
    ViewGroup moreOptions;
    @BindView(R.id.llRunningCost)
    ViewGroup running;
    @Inject
    SearchData searchData;

    private Timer timer;
    private int length;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_layout, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        btnCars.setOnClickListener(this::onClick);
        btnVans.setOnClickListener(this::onClick);
        btnVans.setTextColor(getResources().getColor(R.color.dark_grey));
        btnCars.setSelected(true);
        btnCars.setTextColor(getResources().getColor(R.color.white));
        etPostCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        etPostCode.addTextChangedListener(this);

        spDistance.setAdapter(new ArrayAdapter<>(getContext(), R.layout.spinner_text, getData(MilesType.values())));
        spDistance.setOnItemSelectedListener(this);
        // if distance not null set spinner select another position
        if (searchData.getDistance() != null) {
            spDistance.setSelection(MilesType.getPosByvalue(searchData.getDistance()));
        }
        // if postcode not null set edittext set postcode else show dialog for enter postcode
        if (searchData.getPostCode() != null && !searchData.getPostCode().trim().isEmpty()) {
            etPostCode.setText(searchData.getPostCode());
        } else {
            PostCodeDialog postCodeDialog = new PostCodeDialog(getContext());
            postCodeDialog.setOkListener(postCode -> {
                etPostCode.setText(postCode);
                searchData.setPostCode(postCode);
            });
            postCodeDialog.show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchData.setDistance(MilesType.getValueByKey(spDistance.getSelectedItem().toString()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        this.length = charSequence.length();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (this.timer != null) {
            this.timer.cancel();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() >= length) {
            this.timer = new Timer();
            this.timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!ValidatorUtils.isValidPostcode(editable.toString())) {
                        getActivity().runOnUiThread(() -> makeAlertText(etPostCode, R.string.error_enter_valid_postcode));
                        return;
                    }
                    getActivity().runOnUiThread(() -> makeAlertText(etPostCode, null));
                    searchData.setPostCode(etPostCode.getText().toString());
                }
            }, 500);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCars:
                view.setSelected(true);
                btnCars.setTextColor(getResources().getColor(R.color.white));
                btnVans.setTextColor(getResources().getColor(R.color.dark_grey));
                btnVans.setSelected(false);
                searchData.setType(SearchPanelType.UsedCars.getValue());
                if (keyword.getParent() != null && moreOptions.getParent() != null) {
                    removeView(keyword, first_layout);
                    removeView(moreOptions, first_layout);
                }
                if (vehicle.getParent() == null && performance.getParent() == null) {
                    addView(vehicle, first_layout);
                    addView(performance, first_layout);
                }
                if (running.getParent() == null && keyword.getParent() == null && moreOptions.getParent() == null) {
                    addView(running, second_layout);
                    addView(keyword, second_layout);
                    addView(moreOptions, second_layout);
                }
                break;
            case R.id.btnVans:
                view.setSelected(true);
                btnVans.setTextColor(getResources().getColor(R.color.white));
                btnCars.setTextColor(getResources().getColor(R.color.dark_grey));
                btnCars.setSelected(false);
                searchData.setType(SearchPanelType.UsedVans.getValue());
                if (vehicle.getParent() != null && performance.getParent() != null) {
                    removeView(vehicle, first_layout);
                    removeView(performance, first_layout);
                }
                if (running.getParent() != null && keyword.getParent() != null && moreOptions.getParent() != null) {
                    removeView(running, second_layout);
                    removeView(keyword, second_layout);
                    removeView(moreOptions, second_layout);
                }
                if (keyword.getParent() == null && moreOptions.getParent() == null) {
                    addView(keyword, first_layout);
                    addView(moreOptions, first_layout);
                }
                break;
        }
//        rootLinear.collapse();
//        rootLinear.setListener(new ExpandableLayoutListener() {
//            @Override
//            public void onAnimationStart() {}
//            @Override
//            public void onAnimationEnd() {
//                if (!rootLinear.isExpanded())
//                rootLinear.expand();
//            }
//
//            @Override
//            public void onPreOpen() {
//
//            }
//
//            @Override
//            public void onPreClose() {
//
//            }
//
//            @Override
//            public void onOpened() {
//
//            }
//
//            @Override
//            public void onClosed() {
//
//            }
//        });
    }

    private void removeView(ViewGroup view, ViewGroup layout) {
        layout.removeView(view);
    }

    private void addView(ViewGroup view, ViewGroup layout) {
        layout.addView(view);
    }

    private List<String> getData(iSearchType[] list) {
        List<String> ray = new ArrayList<>();
        for (iSearchType a : list) {
            ray.add(a.getStrKey());
        }
        return ray;
    }

    @OnClick(value = {R.id.llMakeModel, R.id.llBudget, R.id.llAgeMileage, R.id.llTransmission,
            R.id.llFuelType, R.id.llBodySytle, R.id.llColours, R.id.llVehicleSpec,
            R.id.llKeywords, R.id.llMoreOptions, R.id.llRunningCost, R.id.llPerformance})
    public void clickItem(View v) {
        hideKeyboard(v);
        switch (v.getId()) {
            case R.id.llRunningCost:
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new RunningCostsFragment());
                break;

            case R.id.llPerformance:
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new PerformanceFragment());
                break;

            case R.id.llMakeModel: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new MakeFragment());
                break;
            }
            case R.id.llBudget: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new BudgetFragment());
                break;
            }
            case R.id.llAgeMileage: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new AgeMileageFragment());
                break;
            }
            case R.id.llTransmission: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, JustListFragment.getInstance(Constants.Transmissions));
                break;
            }
            case R.id.llFuelType: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, JustListFragment.getInstance(Constants.FuelType));
                break;
            }
            case R.id.llBodySytle: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new BodyStyleFragment());
                break;
            }
            case R.id.llColours: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new ColoursFragment());
                break;
            }
            case R.id.llVehicleSpec: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new VehicleSpecFragment());
                break;
            }
            case R.id.llKeywords: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new KeywordFragment());
                break;
            }
            case R.id.llMoreOptions: {
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new MoreOptionsFragment());
                break;
            }
        }
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        searchData.reset();
        spDistance.setSelection(0);
    }

    @Override
    public void saveSearches() {
    }

    @Override
    public void onStart() {
        super.onStart();
        RxBus.instanceOf().getObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o instanceof ChangeSearchDataEvent) {
                            ChangeSearchDataEvent event = (ChangeSearchDataEvent) o;
                            switch (event.getSectionType()) {
                                case Constants.MAKE_MODEL: {
                                    if (searchData.isChangeMake()) {

                                    }
                                    Toast.makeText(getContext(), "" + searchData.isChangeMake(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.BUDGET: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeBudget(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.AGE_MILEAGE: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeAgeMileage(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.TRANSMISSION: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeTransmissions(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.FUEL_TYPE: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeFuelType(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.BODY_STYLE: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeBodyStyle(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.COLOURS: {
                                    Toast.makeText(getContext(), "" + searchData.isChageColour(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.VEHICLE_SPEC: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeVehicle(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.PERFORMANCE: {
                                    Toast.makeText(getContext(), "" + searchData.isChangePerformance(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.RUNNING_COST: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeRunningCost(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.KEYWORDS: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeKeyword(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                case Constants.MORE_OPTIONS: {
                                    Toast.makeText(getContext(), "" + searchData.isChangeMoreOptions(), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}