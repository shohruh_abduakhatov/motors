package com.motors.phone.app.main.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.main.presenter.UsedCarsPresenter;
import com.motors.phone.app.main.presenter.UsedCarsPresenterImpl;
import com.motors.phone.app.main.presenter.adapters.UsedCarsHolder;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Bakhrom Achilov on 9/13/17.
 */

public class UsedCarsFragment extends BaseFragment implements UsedCarsPresenter, RecyclerArrayAdapter.OnItemClickListener {

    @BindView(R.id.ervUserCars)
    EasyRecyclerView ervUserCars;

    @Getter
    RecyclerArrayAdapter adapter;

    @Inject
    UsedCarsPresenterImpl usedCarsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.used_cars_fragment, null, false);
        ButterKnife.bind(this, view);
        initERV();
        fillUsedCarsList();
        return view;
    }

    private void initERV() {
        ervUserCars.setLayoutManager(new LinearLayoutManager(getContext()));
        ervUserCars.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new UsedCarsHolder(parent, R.layout.item_used_cars);
            }
        });
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void fillUsedCarsList() {
        usedCarsPresenter.fillUsedCarsList();
    }

    @Override
    public void onItemClick(int position) {
        FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new UsedCarsDetailFragment());
    }
}
