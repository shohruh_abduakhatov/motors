package com.motors.phone.app.message.presenter.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;
import com.motors.phone.app.message.model.ChatMessagesModel;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class ChatMessagesHolder extends BaseViewHolder<ChatMessagesModel> {
    private CircleImageView imageView;
    private TextView tvSms, time;
    private SimpleDateFormat simpleDateFormat;

    public ChatMessagesHolder(ViewGroup parent, int res) {
        super(parent, res);
        imageView = $(R.id.civUser);
        tvSms = $(R.id.tvSms);
        time = $(R.id.tvTime);
    }

    public ChatMessagesHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setData(ChatMessagesModel data) {
        super.setData(data);
        simpleDateFormat = new SimpleDateFormat("HH:mm");
        tvSms.setText("" + data.getTxt1());
        time.setText(simpleDateFormat.format(new Date(System.currentTimeMillis())));
    }

}