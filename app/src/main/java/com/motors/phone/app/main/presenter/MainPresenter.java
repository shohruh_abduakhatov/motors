package com.motors.phone.app.main.presenter;

/**
 * Created by bakhrom on 9/6/17.
 */

public interface MainPresenter {
    void fillNotifications();

    void fillSinceYouWereAway();

    void fillRecentSearches();

    void fillSavedSearches();

    void fillNearestDealers();
}
