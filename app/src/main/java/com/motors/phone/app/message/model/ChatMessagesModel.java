package com.motors.phone.app.message.model;

import android.support.annotation.IdRes;

import lombok.Data;

/**
 * Created by Nasimxon on 9/6/2017.
 */
@Data
public class ChatMessagesModel {
    private @IdRes
    int photo;
    private String txt1;
    private String txt2;
}