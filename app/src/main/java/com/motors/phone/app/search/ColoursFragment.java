package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.Constants;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.ColourListHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

import static com.example.nasimxon.components.custom_ui.ColourItemView.BLACK;
import static com.example.nasimxon.components.custom_ui.ColourItemView.BLUE;
import static com.example.nasimxon.components.custom_ui.ColourItemView.GOLD;
import static com.example.nasimxon.components.custom_ui.ColourItemView.GREEN;
import static com.example.nasimxon.components.custom_ui.ColourItemView.GREY;
import static com.example.nasimxon.components.custom_ui.ColourItemView.ORANGE;
import static com.example.nasimxon.components.custom_ui.ColourItemView.PINK;
import static com.example.nasimxon.components.custom_ui.ColourItemView.PURPLE;
import static com.example.nasimxon.components.custom_ui.ColourItemView.RED;
import static com.example.nasimxon.components.custom_ui.ColourItemView.SILVER;
import static com.example.nasimxon.components.custom_ui.ColourItemView.WHITE;

/**
 * Created by Nasimxon on 9/21/2017.
 */

public class ColoursFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    JustListPresenterImp<ColoursFragment> colourPresenter;
    @Inject
    SearchData searchData;
    @BindView(R.id.evColour)
    EasyRecyclerView easyRecyclerView;
    RecyclerArrayAdapter adapter;
    private List<ChoiceItem> selectedColour;
    private List<ChoiceItem> colourItems;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.colours_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        colourItems = new ArrayList<>();
        selectedColour = new ArrayList<>();
        easyRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        easyRecyclerView.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ColourListHolder(parent, R.layout.item_colour);
            }
        });
        adapter.setOnItemClickListener(position -> {
            if (colourItems.get(position).getValue() == 0) return;
            colourItems.get(position).setSelected(!colourItems.get(position).isSelected());
            if (colourItems.get(position).isSelected())
                selectedColour.add(colourItems.get(position));
            else selectedColour.remove(colourItems.get(position));
            adapter.notifyItemChanged(position);
            saveSearches();
        });
        fillLoadList(Constants.Colours);
    }

    @Override
    public void setData(List<ChoiceItem> item) {
        this.colourItems = item;
        for (int i = 0; i < colourItems.size(); i++) {
            if (!checkColour(colourItems.get(i))) {
                colourItems.remove(i);
                i--;
            } else {
                if (searchData.getColours() != null && !searchData.getColours().isEmpty())
                for (ChoiceItem choiceItem : searchData.getColours()) {
                    if (choiceItem.getName().equalsIgnoreCase(colourItems.get(i).getName())) {
                        colourItems.get(i).setSelected(true);
                        selectedColour.add(choiceItem);
                    }
                }
            }
        }
        adapter.addAll(colourItems);
    }

    private boolean checkColour(ChoiceItem item) {
        switch (item.getName().toLowerCase()) {
            case BLACK:
                return true;
            case BLUE:
                return true;
            case PURPLE:
                return true;
            case SILVER:
                return true;
            case GOLD:
                return true;
            case ORANGE:
                return true;
            case GREEN:
                return true;
            case GREY:
                return true;
            case PINK:
                return true;
            case RED:
                return true;
            case WHITE:
                return true;
        }
        return false;
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        selectedColour.clear();
        for (ChoiceItem number : colourItems) {
            number.setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectedColour.size(); i++)
            if (selectedColour.get(i).isSelected()) choiceItems.add(selectedColour.get(i));
        searchData.setColours(choiceItems);
    }

    @Override
    public void fillLoadList(String type) {
        colourPresenter.fillLoadList(type);
    }
}