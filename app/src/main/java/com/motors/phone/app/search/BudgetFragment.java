package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.common.CostType;
import com.motors.phone.common.DepositType;
import com.motors.phone.common.MileageType;
import com.motors.phone.common.PriceType;
import com.motors.phone.common.TermsType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by LOC on 10/30/2017.
 */

public class BudgetFragment extends BaseSearchFragment implements AdapterView.OnItemSelectedListener {
    @Inject
    SearchData searchData;
    @BindView(R.id.spMinPrice)
    AppCompatSpinner spMinPrice;
    @BindView(R.id.spMaxPrice)
    AppCompatSpinner spMaxPrice;
    @BindView(R.id.spMinCost)
    AppCompatSpinner spMinCost;
    @BindView(R.id.spMaxCost)
    AppCompatSpinner spMaxCost;
    @BindView(R.id.spDeposit)
    AppCompatSpinner spDeposit;
    @BindView(R.id.spTerm)
    AppCompatSpinner spTerm;
    @BindView(R.id.spMileage)
    AppCompatSpinner spMileage;
    @BindView(R.id.swtich)
    SwitchCompat swtich;
    @BindView(R.id.priceContainer)
    LinearLayout priceContainer;
    @BindView(R.id.llCostContainer)
    LinearLayout llCostContainer;

    private int minVal;
    private int maxVal;
    private int maxPos;
    private int minPos;
    private ArrayAdapter<String> minPriceAdapter, maxPriceAdapter;
    private ArrayAdapter<String> minCostAdapter, maxCostAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.budget_fragment, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    private void init() {
        minVal = PriceType.Any.getIntValue();
        maxVal = PriceType.Any.getIntValue();

        // restore data
        swtich.setChecked(searchData.isMonthly());
        if (searchData.isMonthly()) {
            if (searchData.getMinCost() != null) minVal = searchData.getMinCost();
            if (searchData.getMaxCost() != null) maxVal = searchData.getMaxCost();
            llCostContainer.setVisibility(View.VISIBLE);
            priceContainer.setVisibility(View.GONE);
        } else {
            if (searchData.getMaxPrice() != null) maxVal = searchData.getMaxPrice();
            if (searchData.getMinPrice() != null) minVal = searchData.getMinPrice();
        }

        spMinPrice.setAdapter(minPriceAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getMinMaxList(true, PriceType.values())));
        if (!swtich.isChecked()) spMinPrice.setSelection(minPos, true);
        spMaxPrice.setAdapter(maxPriceAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getMinMaxList(false, PriceType.values())));
        if (!swtich.isChecked()) spMaxPrice.setSelection(maxPos, true);

        spMinCost.setAdapter(minCostAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getMinMaxList(true, CostType.values())));
        if (swtich.isChecked()) spMinCost.setSelection(minPos, true);
        spMaxCost.setAdapter(maxCostAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getMinMaxList(false, CostType.values())));
        if (swtich.isChecked()) spMaxCost.setSelection(maxPos, true);

        spDeposit.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getMonthlyTypeList(DepositType.values())));
        spDeposit.setOnItemSelectedListener(this);
        if (swtich.isChecked() && searchData.getDeposit() != null)
            spDeposit.setSelection(DepositType.getPosByvalue(searchData.getDeposit()));
        spTerm.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getMonthlyTypeList(TermsType.values())));
        spTerm.setOnItemSelectedListener(this);
        if (swtich.isChecked() && searchData.getTerm() != null)
            spTerm.setSelection(TermsType.getPosByvalue(searchData.getTerm()));
        spMileage.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getMonthlyTypeList(MileageType.values())));
        spMileage.setOnItemSelectedListener(this);
        if (swtich.isChecked() && searchData.getMileage() != null)
            spMileage.setSelection(MileageType.getPosByvalue(searchData.getMileage()));

        spMinPrice.setOnItemSelectedListener(this);
        spMaxPrice.setOnItemSelectedListener(this);

        spMinCost.setOnItemSelectedListener(this);
        spMaxCost.setOnItemSelectedListener(this);

        swtich.setOnCheckedChangeListener((compoundButton, b) -> {
            maxPos = 0;
            minPos = 0;
            minVal = PriceType.Any.getIntValue();
            maxVal = PriceType.Any.getIntValue();
            minPriceAdapter.clear();
            maxPriceAdapter.clear();

            minCostAdapter.clear();
            maxCostAdapter.clear();

            spDeposit.setSelection(spDeposit.getCount() - 1);
            spMileage.setSelection(spMileage.getCount() - 1);
            spTerm.setSelection(spTerm.getCount() - 1);

            maxPriceAdapter.addAll(getMinMaxList(false, PriceType.values()));
            minPriceAdapter.addAll(getMinMaxList(true, PriceType.values()));

            spMinPrice.setSelection(0);
            spMaxPrice.setSelection(0);

            maxCostAdapter.addAll(getMinMaxList(false, CostType.values()));
            minCostAdapter.addAll(getMinMaxList(true, CostType.values()));
            spMinCost.setSelection(0);
            spMaxCost.setSelection(0);
            if (b) {
                llCostContainer.setVisibility(View.VISIBLE);
                priceContainer.setVisibility(View.GONE);
            } else {
                llCostContainer.setVisibility(View.GONE);
                priceContainer.setVisibility(View.VISIBLE);
            }
            clearData();
            searchData.setMonthly(b);
        });
    }

    public List<String> getMonthlyTypeList(iSearchType[] st) {
        List<String> res = new ArrayList<>();
        for (iSearchType i : st)
            res.add(i.getStrKey());
        return res;
    }

    private List<String> getMinMaxList(boolean isMin, iSearchType[] types) {
        int pos = 0;
        List<String> list = new ArrayList<>();
        for (iSearchType item : types) {
            if (item.getIntValue() == PriceType.Any.getIntValue()) {
                if (!isMin && maxVal == item.getIntValue()) maxPos = pos;
                else if (minVal == item.getIntValue()) minPos = pos;
                list.add((isMin ? Constants.MIN : Constants.MAX) + " " + item.getStrKey());
                pos++;
            } else {
                if (isMin && maxVal == PriceType.Any.getIntValue()) {
                    list.add(item.getStrKey());
                    pos++;
                } else if (isMin ? (item.getIntValue() <= maxVal) : (item.getIntValue() >= minVal)) {
                    list.add(item.getStrKey());
                    if (!isMin && maxVal == item.getIntValue()) maxPos = pos;
                    else if (isMin && minVal == item.getIntValue()) minPos = pos;
                    pos++;
                }
            }
        }
        return list;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spMinPrice: {
                minVal = PriceType.getValueByKey(spMinPrice.getSelectedItem().toString());
                maxPriceAdapter.clear();
                maxPriceAdapter.addAll(getMinMaxList(false, PriceType.values()));
                spMaxPrice.setSelection(maxPos);
                searchData.setMinPrice(minVal);
                break;
            }
            case R.id.spMaxPrice: {
                maxVal = PriceType.getValueByKey(spMaxPrice.getSelectedItem().toString());
                minPriceAdapter.clear();
                minPriceAdapter.addAll(getMinMaxList(true, PriceType.values()));
                spMinPrice.setSelection(minPos);
                searchData.setMaxPrice(maxVal);
                break;
            }
            case R.id.spMinCost: {
                minVal = CostType.getValueByKey(spMinCost.getSelectedItem().toString());
                maxCostAdapter.clear();
                maxCostAdapter.addAll(getMinMaxList(false, CostType.values()));
                spMaxCost.setSelection(maxPos);
                searchData.setMinCost(minVal);
                break;
            }
            case R.id.spMaxCost: {
                maxVal = CostType.getValueByKey(spMaxCost.getSelectedItem().toString());
                minCostAdapter.clear();
                minCostAdapter.addAll(getMinMaxList(true, CostType.values()));
                spMinCost.setSelection(minPos);
                searchData.setMaxCost(maxVal);
                break;
            }
            case R.id.spDeposit: {
                if (swtich.isChecked()) searchData.setDeposit(DepositType.getValueByKey(spDeposit.getSelectedItem().toString()));
                break;
            }
            case R.id.spTerm: {
                if (swtich.isChecked()) searchData.setTerm(TermsType.getValueByKey(spTerm.getSelectedItem().toString()));
                break;
            }
            case R.id.spMileage: {
                if (swtich.isChecked()) searchData.setMileage(MileageType.getValueByKey(spMileage.getSelectedItem().toString()));
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        clearData();
        init();
    }

    @Override
    public void saveSearches() {}

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.BUDGET));
    }

    private void clearData() {
        searchData.setMaxPrice(null);
        searchData.setMinPrice(null);
        searchData.setMaxCost(null);
        searchData.setMinCost(null);
        searchData.setDeposit(null);
        searchData.setTerm(null);
        searchData.setMileage(null);
    }
}