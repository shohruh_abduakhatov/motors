package com.motors.phone.app.login.presenter;

import com.motors.phone.app.login.model.LoginModel;
import com.motors.phone.app.login.model.RegisterModel;
import com.motors.phone.common.net_objects.User;

import org.json.JSONException;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public interface LoginPresenter {
    void fillLogin(LoginModel user);
}
