package com.motors.phone.app.ownership.presenter.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by KarimovSardor on 26/09/2017.
 */

public class SlidingImageAdapter extends PagerAdapter {

    private int[] layouts;
    private Context context = null;

    public SlidingImageAdapter(int[] layout, Context context) {

        this.layouts = layout;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = LayoutInflater.from(context).inflate(layouts[position], container, false);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;

    }
}
