package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.common.AccelerationType;
import com.motors.phone.common.PowerType;
import com.motors.phone.common.TankType;
import com.motors.phone.common.TopSpeedType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */

public class PerformanceFragment extends BaseSearchFragment implements AdapterView.OnItemSelectedListener {
    @Inject
    SearchData searchData;

    List<Spinner> spinnerList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        spinnerList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_performance, container, false);
        AppCompatSpinner spinner = view.findViewById(R.id.spSpeed);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                getData(TopSpeedType.values())));
        if (searchData.getSelectedTopSpeed() != null) spinner.setSelection(TopSpeedType.getPosByvalue(searchData.getSelectedTopSpeed()));
        spinner.setOnItemSelectedListener(this);
        spinnerList.add(spinner);
        spinner = view.findViewById(R.id.spPower);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                getData(PowerType.values())));
        if (searchData.getSelectedPower() != null) spinner.setSelection(PowerType.getPosByvalue(searchData.getSelectedPower()));
        spinner.setOnItemSelectedListener(this);
        spinnerList.add(spinner);
        spinner = view.findViewById(R.id.spTank);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                getData(TankType.values())));
        if (searchData.getSelectedTankRange() != null) spinner.setSelection(TankType.getPosByvalue(searchData.getSelectedTankRange()));
        spinner.setOnItemSelectedListener(this);
        spinnerList.add(spinner);
        spinner = view.findViewById(R.id.spAcceleration);
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                getData(AccelerationType.values())));
        if (searchData.getSelectedAcceleration() != null) spinner.setSelection(AccelerationType.getPosByvalue(searchData.getSelectedAcceleration()));
        spinner.setOnItemSelectedListener(this);
        spinnerList.add(spinner);
        return view;
    }

    private List<String> getData(iSearchType[] ray) {
        List<String> list = new ArrayList<>();
        for (iSearchType a : ray) {
            list.add(a.getStrKey());
        }
        return list;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spSpeed:
                searchData.setSelectedTopSpeed(TopSpeedType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            case R.id.spPower:
                searchData.setSelectedPower(PowerType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            case R.id.spTank:
                searchData.setSelectedTankRange(TankType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            case R.id.spAcceleration:
                searchData.setSelectedAcceleration(AccelerationType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView){}

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        searchData.setSelectedAcceleration(null);
        searchData.setSelectedTankRange(null);
        searchData.setSelectedPower(null);
        searchData.setSelectedTopSpeed(null);
        for (Spinner spinner : spinnerList) spinner.setSelection(0);
    }

    @Override
    public void saveSearches() {}

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.PERFORMANCE));
    }
}