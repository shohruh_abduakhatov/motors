package com.motors.phone.app.main.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.buy.BuyDetailFragment;
import com.motors.phone.app.main.presenter.MainPresenter;
import com.motors.phone.app.main.presenter.MainPresenterImpl;
import com.motors.phone.app.main.presenter.adapters.NearestDealersHolder;
import com.motors.phone.app.main.presenter.adapters.NotificationsHolder;
import com.motors.phone.app.main.presenter.adapters.RecentSearchAdapter;
import com.motors.phone.app.main.presenter.adapters.SinceYouWereAwayHolder;
import com.motors.phone.app.main.presenter.adapters.WishListHolder;
import com.motors.phone.app.ownership.OwnershipFragment;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Achilov Bakhrom on 9/5/17.
 */

public class MainFragment extends BaseFragment implements MainPresenter {
    @Inject
    MainPresenterImpl mainPresenter;
    @Getter
    @BindView(R.id.ervRecentSearches)
    EasyRecyclerView ervRecentSearches;
    @Getter
    @BindView(R.id.ervSavedSearches)
    EasyRecyclerView ervSavedSearches;
    @Getter
    @BindView(R.id.ervNearest)
    EasyRecyclerView ervNearest;
    @Getter
    @BindView(R.id.ervSinceYouAway)
    EasyRecyclerView ervSinceYouAway;
    @Getter
    @BindView(R.id.ervNotifications)
    EasyRecyclerView ervNotifications;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    @Getter
    RecyclerArrayAdapter
            adapterRecent, adapterSavedReserch,
            adapterNearest, adapterSinceAway, adapterNotifications;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        setRetainInstance(true);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    @OnClick({R.id.tvUsedCars, R.id.tvSavedCars, R.id.tvOwnership})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvUsedCars:
//                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new BuyDetailFragment());
                break;
            case R.id.tvSavedCars:
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new WishListFragment());
                break;
            case R.id.tvOwnership:
                FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new OwnershipFragment());
                break;
        }
    }

    private void initUI() {
        ervNearest.getRecyclerView().setNestedScrollingEnabled(false);
        ervNearest.setLayoutManager(new LinearLayoutManager(getContext()));
        ervNotifications.getRecyclerView().setNestedScrollingEnabled(false);
        ervNotifications.setLayoutManager(new LinearLayoutManager(getContext()));
        ervSinceYouAway.getRecyclerView().setNestedScrollingEnabled(false);
        ervSinceYouAway.setLayoutManager(new LinearLayoutManager(getContext()));
        ervRecentSearches.getRecyclerView().setNestedScrollingEnabled(false);
        ervRecentSearches.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ervSavedSearches.getRecyclerView().setNestedScrollingEnabled(false);
        ervSavedSearches.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ervNotifications.setAdapter(adapterNotifications = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new NotificationsHolder(parent, R.layout.item_notifications);
            }
        });
        ervSinceYouAway.setAdapter(adapterSinceAway = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new SinceYouWereAwayHolder(parent, R.layout.item_since_your_away);
            }
        });
        ervRecentSearches.setAdapter(adapterRecent = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new RecentSearchAdapter(parent, R.layout.item_recent_searches);
            }
        });
        ervSavedSearches.setAdapter(adapterSavedReserch = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new WishListHolder(parent, R.layout.item_recent_searches);
            }
        });
        ervNearest.setAdapter(adapterNearest = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new NearestDealersHolder(parent, R.layout.item_nearest_dealer);
            }
        });
        fillNotifications();
        fillSinceYouWereAway();
        fillRecentSearches();
        fillSavedSearches();
        fillNearestDealers();
        ervNearest.post(() -> {
            scrollView.setScrollY(0);
        });
    }

    @Override
    public void fillNotifications() {
        mainPresenter.fillNotifications();
    }

    @Override
    public void fillSinceYouWereAway() {
        mainPresenter.fillSinceYouWereAway();
    }

    @Override
    public void fillRecentSearches() {
        mainPresenter.fillRecentSearches();
    }

    @Override
    public void fillSavedSearches() {
        mainPresenter.fillSavedSearches();
    }

    @Override
    public void fillNearestDealers() {
        mainPresenter.fillNearestDealers();
    }
}