package com.motors.phone.app.login.presenter;

import android.util.Log;
import android.widget.Toast;

import com.motors.phone.app.login.RegisterFragment;
import com.motors.phone.app.login.model.RegisterModel;
import com.motors.phone.app.main.MainActivity;
import com.motors.phone.common.net_objects.MotorsAddress;
import com.motors.phone.common.net_objects.MotorsResponse;
import com.motors.phone.core.BasePresenterImp;
import com.motors.phone.util.JsonParseUtil;
import com.motors.phone.util.MotorFilterSubscribe;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class RegisterPresenterImpl extends BasePresenterImp<RegisterFragment> implements RegisterPresenter {
    @Inject
    public RegisterPresenterImpl(RegisterFragment registerFragment) {
        super(registerFragment);
    }

    @Override
    public void fillRegister(RegisterModel user) {
        if (apiManager.hasConnection()) {
            loadingDialog.show();
            apiManager.register(user)
                    .subscribe(new MotorFilterSubscribe<MotorsResponse<Object>>() {
                        @Override
                        protected void onError(Throwable t, String message) {
                            loadingDialog.hide();
                            fragment.setRequestSend(false);
                            if (message != null)
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        protected void onResult(MotorsResponse<Object> result) {
                            loadingDialog.hide();
                            if (fragment.isRequestSend()) {
                                Toast.makeText(context, "Registration completed", Toast.LENGTH_SHORT).show();
                                MainActivity.start(context);
                            }
                            fragment.setRequestSend(false);
                        }
                    });
        } else {
            Toast.makeText(context, "Internet connection failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void fillAddressPostCode(String postCode) {
        if (apiManager.hasConnection()) {
            loadingDialog.show();
            apiManager.getAddressByPostcode(postCode)
                    .subscribe(new Observer<ResponseBody>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(ResponseBody result) {
                            loadingDialog.hide();
                            try {
                                MotorsAddress address = JsonParseUtil.parseGoogleLocation(new JSONObject(result.string()));
                                if (address.getLine1() != null)
                                    fragment.getEtStreetName().setText(address.getLine1());
                                if (address.getLine2() != null)
                                    fragment.getEtCity().setText(address.getLine2());
                                if (address.getLine3() != null)
                                    fragment.getEtCountry().setText(address.getLine3());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("sss", "error " + e.toString());
                            loadingDialog.hide();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            Toast.makeText(context, "Internet connection failed", Toast.LENGTH_SHORT).show();
        }
    }
}