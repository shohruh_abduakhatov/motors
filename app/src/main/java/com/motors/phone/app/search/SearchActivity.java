package com.motors.phone.app.search;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.nasimxon.components.custom_ui.PostCodeDialog;
import com.motors.phone.R;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

/**
 * Created by Nasimxon on 10/13/2017.
 */

public class SearchActivity extends AppCompatActivity {
    @Inject
    SearchData searchData;
    private PostCodeDialog postCodeDialog;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SearchActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            TODO Implement the layout according to the orientation
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search");
        openSubFragment();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        postCodeDialog = new PostCodeDialog(this);
        postCodeDialog.setOkListener(postCode -> {
            searchData.setPostCode(postCode);
        });
    }

    protected void openSubFragment() {
        FragmentManagerUtils.addFragment(this, R.id.flMain, new SearchFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 1)
                finish();
            FragmentManagerUtils.popBackstack(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tvReset)
    public void clickReset() {
        BaseSearchFragment fragment = (BaseSearchFragment) getSupportFragmentManager().findFragmentById(R.id.flMain);
        if (fragment != null) fragment.clickReset();
    }

    @OnClick(R.id.tvUpdateResult)
    public void clickUpdateResult() {
        if (searchData.getPostCode() != null && !searchData.getPostCode().isEmpty())
            finish();
        else postCodeDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1)
            finish();
        FragmentManagerUtils.popBackstack(this);
    }

}