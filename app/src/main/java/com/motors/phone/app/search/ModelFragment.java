package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.Constants;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.ModelHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.ChoicesComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by Nasimxon on 10/18/2017.
 */

public class ModelFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    JustListPresenterImp<ModelFragment> presenter;
    @Inject
    SearchData searchData;
    @BindView(R.id.ervModels)
    EasyRecyclerView ervModels;
    RecyclerArrayAdapter adapter;

    private String selectMake;
    private List<ChoiceItem> models;
    private List<ChoiceItem> selectModels;

    public static ModelFragment getInstance(String selectPosName) {
        ModelFragment modelFragment = new ModelFragment();
        Bundle bundle = new Bundle();
        bundle.putString("selectPosName", selectPosName);
        modelFragment.setArguments(bundle);
        return modelFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        selectMake = getArguments().getString("selectPosName");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_models, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        selectModels = new ArrayList<>();
        ervModels.setLayoutManager(new LinearLayoutManager(getContext()));
        ervModels.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ModelHolder(parent, R.layout.item_makes_v2);
            }
        });
        adapter.setOnItemClickListener(position -> {
            if (models.get(position).getValue() == 0) return;
            models.get(position).setSelected(!models.get(position).isSelected());
            if (models.get(position).isSelected())
                selectModels.add(models.get(position));
            else {
                selectModels.remove(models.get(position));
            }
            adapter.notifyItemChanged(position);
            saveSearches();
        });
        fillLoadList(Constants.Models);
    }

    @Override
    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectModels.size(); i++) {
            if (selectModels.get(i).isSelected()) choiceItems.add(selectModels.get(i));
        }
        searchData.setModels(choiceItems);
    }

    @Override
    public void setData(List<ChoiceItem> models) {
        this.models = models;
        for (int i = 0; i < models.size(); i++) {
            String make = models.get(i).getName().split("#")[0];
            String model = models.get(i).getName().split("#")[1];
            if (!make.equalsIgnoreCase(selectMake)) {
                models.remove(i);
                i--;
            } else {
                models.get(i).setParentName(make);
                models.get(i).setName(model);
            }
        }
        Collections.sort(models, new ChoicesComparator(models.size()));
        if (searchData.getModels() != null && !searchData.getModels().isEmpty())
        for (ChoiceItem item : this.models) {
            for (ChoiceItem choiceItem : searchData.getModels()) {
                if (item.getName().equalsIgnoreCase(choiceItem.getName())) {
                    selectModels.add(item);
                    item.setSelected(true);
                }
            }
        }
        adapter.addAll(models);
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset models", Toast.LENGTH_SHORT).show();
        searchData.clearMakeModels();
        selectModels.clear();
        for (ChoiceItem make : models) {
            make.setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void fillLoadList(String type) {
        presenter.fillLoadList(type);
    }
}