package com.motors.phone.app.login.model;

import com.google.gson.annotations.SerializedName;
import com.example.nasimxon.components.utils.Constants;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class RegisterModel implements Serializable {
    @SerializedName(Constants.Ticket)
    private String ticket;
    @SerializedName("Password")
    private String password;
    @SerializedName("PostCode")
    private String postcode;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("EmailAddress")
    private String email;
    @SerializedName("ProviderName")
    private String provider;
    @SerializedName("MobileNumber")
    private String phoneNumber;
    @SerializedName("Salutation")
    private String salutation;
    @SerializedName("Line1")
    private String line1;

    @SerializedName("Competitions")
    private boolean competitions;
    @SerializedName("MotorsUpdates")
    private boolean motorsUpdates;
    @SerializedName("LatestNews")
    private boolean latestNews;
    @SerializedName("ThirdParties")
    private boolean thirdParties;
    @SerializedName("TermsAccepted")
    private boolean termsAccepted;


}
