package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nasimxon.components.utils.Constants;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.BodyStyleHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.CONVERTIBLE;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.COUPE;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.ESTATE;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.FOUR_X_FOUR;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.HATCHBACK;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.PEOPLE_CARRIER;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.PICK_UP;
import static com.example.nasimxon.components.custom_ui.BodyStyleItemView.SALOON;

/**
 * Created by Nasimxon on 9/21/2017
 */

public class BodyStyleFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    SearchData searchData;
    @Inject
    JustListPresenterImp<BodyStyleFragment> presenterImp;
    @BindView(R.id.ervBodyStyle)
    EasyRecyclerView ervBodyStyle;
    @Getter
    RecyclerArrayAdapter adapter;
    private List<ChoiceItem> bodyItems;
    private List<ChoiceItem> selectBodyItems;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.body_style_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        bodyItems = new ArrayList<>();
        selectBodyItems = new ArrayList<>();
        ervBodyStyle.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        ervBodyStyle.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new BodyStyleHolder(parent, R.layout.item_body_style);
            }
        });
        adapter.setOnItemClickListener(position -> {
            if (bodyItems.get(position).getValue() == 0) return;
            bodyItems.get(position).setSelected(!bodyItems.get(position).isSelected());
            if (bodyItems.get(position).isSelected())
                selectBodyItems.add(bodyItems.get(position));
            else selectBodyItems.remove(bodyItems.get(position));
            adapter.notifyItemChanged(position);
            saveSearches();
        });
        fillLoadList(Constants.BodyTypes);
    }

    @Override
    public void setData(List<ChoiceItem> bodyItems) {
        this.bodyItems = bodyItems;
        for (int i = 0; i < bodyItems.size(); i++) {
            if (!checkBodyStyle(bodyItems.get(i))) {
                bodyItems.remove(i);
                i--;
            }
        }
        for (ChoiceItem bodyItem : this.bodyItems) {
            if (searchData.getBodyStyles() != null)
                for (int i = 0; i < searchData.getBodyStyles().size(); i++) {
                    if (bodyItem.getName().equalsIgnoreCase(searchData.getBodyStyles().get(i).getName())) {
                        if (bodyItem.getValue() == 0) {
                            searchData.getBodyStyles().remove(i);
                            i--;
                            continue;
                        }
                        selectBodyItems.add(bodyItem);
                        bodyItem.setSelected(true);
                    }
                }
        }
        adapter.addAll(bodyItems);
    }

    @Override
    public void fillLoadList(String type) {
        presenterImp.fillLoadList(type);
    }

    private boolean checkBodyStyle(ChoiceItem item) {
        switch (item.getName().toLowerCase()) {
            case SALOON:
                return true;
            case HATCHBACK:
                return true;
            case PEOPLE_CARRIER:
                return true;
            case CONVERTIBLE:
                return true;
            case COUPE:
                return true;
            case ESTATE:
                return true;
            case PICK_UP:
                return true;
            case FOUR_X_FOUR:
                return true;
        }
        return false;
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        searchData.clearMakeModels();
        selectBodyItems.clear();
        for (ChoiceItem make : bodyItems) {
            make.setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectBodyItems.size(); i++) {
            if (selectBodyItems.get(i).isSelected()) choiceItems.add(selectBodyItems.get(i));
        }
        searchData.setBodyStyle(choiceItems);
    }
}