package com.motors.phone.app.main.presenter;

import com.motors.phone.app.main.model.UsedCars;
import com.motors.phone.app.main.view.UsedCarsFragment;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Achilov Bakhrom on 9/13/17.
 */

public class UsedCarsPresenterImpl implements UsedCarsPresenter {
    private UsedCarsFragment fragment;
    private ApiManager apiManager;

    @Inject
    public UsedCarsPresenterImpl(UsedCarsFragment fragment, ApiManager apiManager) {
        this.fragment = fragment;
        this.apiManager = apiManager;
    }

    @Override
    public void fillUsedCarsList() {
        apiManager.getUsedCars()
                .subscribe(new Observer<List<UsedCars>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(List<UsedCars> usedCars) {
                        fragment.getAdapter().addAll(usedCars);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
