package com.motors.phone.app.buy.presenter.adapters;

import android.view.View;
import android.view.ViewGroup;

import com.example.nasimxon.components.custom_ui.circleProgressView.CircleProgressLayout;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;

/**
 * Created by Nasimxon on 9/27/2017.
 */

public class SpecTechHolder extends BaseViewHolder {
    private CircleProgressLayout cplSpecTech;

    public SpecTechHolder(View itemView) {
        super(itemView);
    }

    public SpecTechHolder(ViewGroup parent, int res) {
        super(parent, res);
        cplSpecTech = (CircleProgressLayout) $(R.id.cplSpecTech);
    }

    @Override
    public void setData(Object data) {
        super.setData(data);
        cplSpecTech.setProgress(190, 210);
        cplSpecTech.setDescription("MPC " + getAdapterPosition());
    }
}