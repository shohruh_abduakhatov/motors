package com.motors.phone.app.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.motors.phone.R;
import com.motors.phone.app.intro.IntroPageFragment;
import com.motors.phone.util.FragmentManagerUtils;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/**
 * Created by Nasimxon on 9/4/2017.
 */

public class LoginActivity extends AppCompatActivity {
    @Inject
    SharedPreferences sharedPreferences;
    private LoginFragment loginFragment;

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FirebaseApp.initializeApp(this);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            TODO Implement the layout according to the orientation
        }
        if (!sharedPreferences.getBoolean("introShowed", false)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.flContainer, new IntroPageFragment())
                    .commit();
        } else finishIntro();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("email", loginFragment.etEmail.getText().toString());
        outState.putString("password", loginFragment.etPassword.getText().toString());
        outState.putBoolean("isError", loginFragment.hasError);
        outState.putInt("isRegisterVisible", loginFragment.llRegister.getVisibility());
        outState.putString("loginButton", loginFragment.btnLogin.getText().toString());
        outState.putInt("tvSkipVisibility", loginFragment.tvSkip.getVisibility());
        if (loginFragment.btnLoginSection.isSelected()) {
            outState.putBoolean("loginSelected", true);
        } else {
            outState.putBoolean("registerSelected", true);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        loginFragment.tvSkip.setVisibility(savedInstanceState.getInt("tvSkipVisibility"));
        loginFragment.btnLogin.setText(savedInstanceState.getString("loginButton"));
        loginFragment.hasError = savedInstanceState.getBoolean("isError");
        loginFragment.etEmail.setText(savedInstanceState.getString("email"));
        loginFragment.etPassword.setText(savedInstanceState.getString("password"));
        loginFragment.llRegister.setVisibility(savedInstanceState.getInt("isRegisterVisible"));
        loginFragment.btnLoginSection.setSelected(savedInstanceState.getBoolean("loginSelected"));
        loginFragment.btnRegister.setSelected(savedInstanceState.getBoolean("registerSelected"));

    }

    public void finishIntro() {
        sharedPreferences.edit().putBoolean("introShowed", true).apply();
        FragmentManagerUtils.popBackstack(this);
        if (loginFragment == null) loginFragment = new LoginFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.flContainer, loginFragment)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (loginFragment != null) loginFragment.onActivityResult(requestCode, resultCode, data);
    }
}