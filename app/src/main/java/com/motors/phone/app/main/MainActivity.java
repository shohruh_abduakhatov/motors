package com.motors.phone.app.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nasimxon.components.custom_ui.bottom_bar_view.BottomBarBaseAdapter;
import com.example.nasimxon.components.custom_ui.bottom_bar_view.BottomBarView;
import com.example.nasimxon.components.custom_ui.bottom_bar_view.bottom_bar_item_view.MessageCountView;
import com.motors.phone.R;
import com.motors.phone.app.buy.BuyFragment;
import com.motors.phone.app.main.view.WishListFragment;
import com.motors.phone.app.message.MessageFragment;
import com.motors.phone.app.notification.NotificationsFragment;
import com.motors.phone.app.ownership.OwnershipFragment;
import com.motors.phone.app.search.SearchActivity;
import com.motors.phone.app.sell.SellFragment;
import com.motors.phone.app.settings.SettingsFragment;
import com.motors.phone.app.value.ValueFragment;
import com.motors.phone.common.net_objects.FullDetail;
import com.motors.phone.model.BottomBarItem;
import com.motors.phone.util.FragmentManagerUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nasimxon on 9/4/2017.
 */

public class MainActivity extends AppCompatActivity implements BottomBarView.BottomBarClickListener {

    public static final int BUY = 0, SELL = 1, VALUE = 2, OWNERSHIP = 3, MESSAGE = 4;
    public static final int CANCEL = 0, RESET = 1, DONE = 2, SAVE_SEARCH = 3;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.bbvBottomBar)
    BottomBarView bottomBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FullDetail fullDetail;
    private int selectToolbarItemId = -1;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        ButterKnife.bind(this);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            TODO Implement the layout according to the orientation
        }
        initToolbar();
        FragmentManagerUtils.openMainFragment(this, R.id.flMain);
        bottomBar.setListener(this);
        bottomBar.setAdapter(buildAdapterForNormalModel());
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        setTitle(null);
    }

    private BottomBarAdapter buildAdapterForNormalModel() {
        List<BottomBarItem> items = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            BottomBarItem item = new BottomBarItem();
            switch (i) {
                case 0:
                    item.setText("Buy");
                    item.setIconId(R.drawable.vector_buy_icon);
                    break;
                case 1:
                    item.setText("Sell");
                    item.setIconId(R.drawable.vector_sell_icon);
                    break;
                case 2:
                    item.setText("Value");
                    item.setIconId(R.drawable.vector_buy_icon);
                    break;
                case 3:
                    item.setText("Ownership");
                    item.setIconId(R.drawable.vector_ownership_icon);
                    break;
                case 4:
                    item.setText("Message");
                    item.setIconId(R.drawable.vector_message_icon);
                    break;
            }
            item.setMessageSupport(i == 4);
            items.add(item);
        }
        return new BottomBarAdapter(items, this, true);
    }

    @OnClick(R.id.rlToolbarHomeButton)
    public void homeButtonClick() {
        bottomBar.clearSelection();
        FragmentManagerUtils.openMainFragment(MainActivity.this, R.id.flMain);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d("sss", "" + (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof NotificationsFragment));
        for (int i = 0; i < menu.size(); i++)
            menu.getItem(i).getIcon().mutate().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        if (selectToolbarItemId == R.id.toolbar_heart) {
            menu.getItem(1).getIcon().mutate().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        } else if (selectToolbarItemId == R.id.toolbar_notifications) {
            menu.getItem(2).getIcon().mutate().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        } else if (selectToolbarItemId == R.id.toolbar_settings) {
            menu.getItem(3).getIcon().mutate().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.toolbar_search)
            SearchActivity.start(this);
        if (selectToolbarItemId == item.getItemId()) return false;
        selectToolbarItemId = item.getItemId();
        if (!FragmentManagerUtils.isMainPage(this, R.id.flMain))
            FragmentManagerUtils.popBackstack(this);
        switch (item.getItemId()) {
            case R.id.toolbar_heart:
                FragmentManagerUtils.addFragment(this, R.id.flMain, new WishListFragment());
                break;
            case R.id.toolbar_notifications:
                FragmentManagerUtils.addFragment(this, R.id.flMain, new NotificationsFragment());
                break;
            case R.id.toolbar_settings:
                FragmentManagerUtils.addFragment(this, R.id.flMain, new SettingsFragment());
                break;
        }
        bottomBar.clearSelection();
        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void selection(int pos) {
        Fragment fragment = null;
        switch (pos) {
            case BUY:
                if (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof BuyFragment)
                    return;
                fragment = new BuyFragment();
                break;
            case SELL:
                if (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof SellFragment)
                    return;
                fragment = new SellFragment();
                break;
            case VALUE:
                if (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof ValueFragment)
                    return;
                fragment = new ValueFragment();
                break;
            case OWNERSHIP:
                if (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof OwnershipFragment)
                    return;
                fragment = new OwnershipFragment();
                break;
            case MESSAGE:
                if (getSupportFragmentManager().findFragmentById(R.id.flMain) instanceof MessageFragment)
                    return;
                fragment = new MessageFragment();
                break;
        }
        if (fragment != null) {
            if (!FragmentManagerUtils.isMainPage(this, R.id.flMain)) {
                FragmentManagerUtils.popBackstack(this);
            }
            FragmentManagerUtils.addFragment(this, R.id.flMain, fragment);
        }
    }

    @Override
    public void onBackPressed() {
        if (FragmentManagerUtils.isMainPage(this, R.id.flMain))
            finish();
        else {
            super.onBackPressed();
            invalidateOptionsMenu();
        }
    }

    //BottomBar adapter
    public static class BottomBarAdapter extends BottomBarBaseAdapter<BottomBarItem> {
        private boolean type;
        private Context context;

        public BottomBarAdapter(List<BottomBarItem> items, Context context, boolean type) {
            super(items);
            this.type = type;
            this.context = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        protected void onBind(ViewGroup child, int position) {
            int layout = -1;
            if (type) {
                layout = R.layout.bottom_bar_item_view;
            } else {
                if (position != 3) {
                    layout = R.layout.bottom_bar_item_view_for_search_big;
                } else {
                    layout = R.layout.bottom_bar_item_view_for_search_normal;
                }
            }
            LayoutInflater.from(context).inflate(layout, child, true);
            ((ImageView) child.findViewById(R.id.ivBottomBarItem)).setImageResource(items.get(position).getIconId());
            if (type) {
                ((TextView) child.findViewById(R.id.tvBottomBarView)).setText(items.get(position).getText());
                child.findViewById(R.id.mcvMessageCount)
                        .setVisibility(items.get(position).isMessageSupport() ? View.VISIBLE : View.GONE);
                ((MessageCountView) child.findViewById(R.id.mcvMessageCount)).setMessageCount(3);
            }
        }

        @Override
        protected BottomBarItem getItem(int position) {
            return items.get(position);
        }
    }
}