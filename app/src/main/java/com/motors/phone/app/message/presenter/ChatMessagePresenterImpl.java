package com.motors.phone.app.message.presenter;

import com.motors.phone.app.message.ChatActivity;
import com.motors.phone.app.message.model.ChatMessagesModel;
import com.motors.phone.util.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

//import com.motors.phone.app.message.ChatMessagesFragment;

/**
 * Created by Nasimxon on 9/6/17.
 */

public class ChatMessagePresenterImpl implements MessagePresenter {

    private ChatActivity chatActivity;
    private ApiManager apiManager;

    @Inject
    public ChatMessagePresenterImpl(ChatActivity chatActivity, ApiManager apiManager) {
        this.apiManager = apiManager;
        this.chatActivity = chatActivity;
    }

    @Override
    public void fillMessages() {
        apiManager.getChatMessages()
                .subscribe(new Observer<List<ChatMessagesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onNext(List<ChatMessagesModel> recentSearchModels) {
                        chatActivity.getAdapterMessages().addAll(recentSearchModels);
                        chatActivity.getAdapterMessages().addAll(recentSearchModels);
                    }
                });
    }
}
