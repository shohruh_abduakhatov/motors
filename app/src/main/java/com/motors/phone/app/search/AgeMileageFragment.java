package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.common.AgeType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.common.search.TotalMileageType;
import com.motors.phone.core.BaseSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by Achilov Bakhrom on 9/18/17.
 */

public class AgeMileageFragment extends BaseSearchFragment {
    @Inject
    SearchData searchData;
    @BindView(R.id.spAge)
    AppCompatSpinner spAge;
    @BindView(R.id.spMileage)
    AppCompatSpinner spMileage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.age_mileage_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        spAge.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getAgeTypeList(AgeType.values())));
        spMileage.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getAgeTypeList(TotalMileageType.values())));
        if (searchData.getMinAge() != null) spAge.setSelection(AgeType.getAgePosByKey(AgeType.getAgePosByKey(searchData.getMinAge())));
        if (searchData.getMinMileage() != null) spMileage.setSelection(TotalMileageType.getMilagePosByKey(searchData.getMinMileage()));
        spAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                searchData.setMinAge(AgeType.getKeyByValue(spAge.getSelectedItem().toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        spMileage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                searchData.setMinMileage(TotalMileageType.getMileageByValue(spMileage.getSelectedItem().toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    public List<String> getAgeTypeList(iSearchType[] st) {
        List<String> res = new ArrayList<>();
        for (iSearchType i : st)
            res.add(i.getStrValue());
        return res;
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        spMileage.setSelection(0);
        spAge.setSelection(0);
        searchData.setMinMileage(null);
        searchData.setMinAge(null);
    }

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.AGE_MILEAGE));
    }

    @Override
    public void saveSearches() {}
}