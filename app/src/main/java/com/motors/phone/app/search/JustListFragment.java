package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.RxBus;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.search.presenter.JustListPresenter;
import com.motors.phone.app.search.presenter.JustListPresenterImp;
import com.motors.phone.app.search.presenter.adapters.ChoiceHolder;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;
import com.motors.phone.util.ChoicesComparator;
import com.example.nasimxon.components.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 10/25/2017.
 */

public class JustListFragment extends BaseSearchFragment implements JustListPresenter {
    @Inject
    SearchData searchData;
    @Inject
    JustListPresenterImp<JustListFragment> presenterImp;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ervList)
    EasyRecyclerView ervList;
    @Getter
    RecyclerArrayAdapter adapter;
    private List<ChoiceItem> numbers;
    private List<ChoiceItem> selectedNumbers;
    private String type;

    public static Fragment getInstance(String type) {
        JustListFragment justListFragment = new JustListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        justListFragment.setArguments(bundle);
        return justListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
        if (type == null) throw new NullPointerException();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.just_list_fragment, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    private void init() {
        numbers = new ArrayList<>();
        selectedNumbers = new ArrayList<>();
        switch (type) {
            case Constants.Doors: {
                tvTitle.setText("How many door wood you like?");
                break;
            }
            case Constants.Seats: {
                tvTitle.setText("How many seats do you need?");
                break;
            }
            case Constants.SafetyRatings: {
                tvTitle.setText("You can also filter your search by Euro NCAP Safety Rating");
                break;
            }
            case Constants.FuelType: {
                tvTitle.setVisibility(View.GONE);
                break;
            }
            case Constants.Transmissions: {
                tvTitle.setText("What sort of Transmissions would you prefer?");
                break;
            }
        }
        ervList.setLayoutManager(new LinearLayoutManager(getContext()));
        ervList.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ChoiceHolder(parent, R.layout.item_makes, false);
            }
        });
        adapter.setOnItemClickListener(position -> {
            if (numbers.get(position).getValue() == 0) return;
            numbers.get(position).setWithAnim(true);
            numbers.get(position).setSelected(!numbers.get(position).isSelected());
            if (numbers.get(position).isSelected())
                selectedNumbers.add(numbers.get(position));
            else selectedNumbers.remove(numbers.get(position));
            adapter.notifyItemChanged(position);
            saveSearches();
        });
        fillLoadList(type);
    }

    @Override
    public void clickReset() {
        Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
        switch (type) {
            case Constants.Seats: {
                searchData.setSeats(new ArrayList<>());
                break;
            }
            case Constants.Doors: {
                searchData.setDoors(new ArrayList<>());
                break;
            }
            case Constants.SafetyRatings: {
                searchData.setSafetyRatings(new ArrayList<>());
                break;
            }
            case Constants.FuelType: {
                searchData.setFuelTypes(new ArrayList<>());
                break;
            }
            case Constants.Transmissions: {
                searchData.setTransmission(new ArrayList<>());
                break;
            }
        }
        selectedNumbers.clear();
        for (ChoiceItem number : selectedNumbers) {
            number.setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveSearches() {
        List<ChoiceItem> choiceItems = new ArrayList<>();
        for (int i = 0; i < selectedNumbers.size(); i++) {
            if (selectedNumbers.get(i).isSelected()) choiceItems.add(selectedNumbers.get(i));
        }
        switch (type) {
            case Constants.Seats: {
                searchData.setSeats(choiceItems);
                break;
            }
            case Constants.Doors: {
                searchData.setDoors(choiceItems);
                break;
            }
            case Constants.SafetyRatings: {
                searchData.setSafetyRatings(choiceItems);
                break;
            }
            case Constants.FuelType: {
                searchData.setFuelTypes(choiceItems);
                break;
            }
            case Constants.Transmissions: {
                searchData.setTransmission(choiceItems);
                break;
            }
        }
    }

    @Override
    public void setData(List<ChoiceItem> list) {
        this.numbers = list;
        List<ChoiceItem> items = null;
        switch (type) {
            case Constants.Doors: {
                items = searchData.getDoors();
                break;
            }
            case Constants.Seats: {
                items = searchData.getSeats();
                break;
            }
            case Constants.SafetyRatings: {
                items = searchData.getSafetyRatings();
                break;
            }
            case Constants.FuelType: {
                items = searchData.getFuelTypes();
                break;
            }
            case Constants.Transmissions: {
                items = searchData.getTransmission();
                break;
            }
        }
        Collections.sort(list, new ChoicesComparator(list.size()));
        for (ChoiceItem make : numbers) {
            if (items != null)
                for (ChoiceItem choiceItem : items) {
                    if (make.getName().equalsIgnoreCase(choiceItem.getName())) {
                        selectedNumbers.add(make);
                        make.setSelected(true);
                    }
                }
        }
        adapter.addAll(numbers);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (type.equalsIgnoreCase(Constants.Transmissions))
            RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.TRANSMISSION));
        else if (type.equalsIgnoreCase(Constants.FuelType))
            RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.FUEL_TYPE));
        else RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.VEHICLE_SPEC));
    }

    @Override
    public void fillLoadList(String type) {
        presenterImp.fillLoadList(type);
    }
}
