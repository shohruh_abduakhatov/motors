package com.motors.phone.app.search.presenter;

import android.widget.Toast;

import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BasePresenterImp;
import com.motors.phone.core.BaseSearchFragment;
import com.example.nasimxon.components.utils.Constants;
import com.motors.phone.util.JsonParseUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

/**
 * Created by Nasimxon on 10/11/2017.
 */

public class JustListPresenterImp<T extends BaseSearchFragment> extends BasePresenterImp<T> implements JustListPresenter {
    @Inject SearchData searchData;

    @Inject
    public JustListPresenterImp(T fragment) {
        super(fragment);
    }

    @Override
    public void fillLoadList(String type) {
        if (apiManager.hasConnection()) {
            loadingDialog.show();
            apiManager.getFacets()
                    .subscribe(new Observer<ResponseBody>() {
                        @Override
                        public void onSubscribe(Disposable d) {}
                        @Override
                        public void onNext(ResponseBody responseBody) {
                            loadingDialog.hide();
                            try {
                                JSONObject jsonObject = new JSONObject(responseBody.string());
                                if (jsonObject.get(Constants.Data) != null) {
                                    fragment.setData(JsonParseUtil.getFacetChoicesByTag(jsonObject, type));
                                } else if (!jsonObject.getBoolean(Constants.IS_VALID)) {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingDialog.hide();
                            Toast.makeText(context, Constants.INFO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
                        }
                        @Override
                        public void onComplete() {
                        }
                    });
        } else
            Toast.makeText(context, Constants.INFO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
    }
}
