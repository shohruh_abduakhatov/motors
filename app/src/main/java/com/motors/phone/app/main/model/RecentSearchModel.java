package com.motors.phone.app.main.model;

import android.support.annotation.IdRes;

import lombok.Data;

/**
 * Created by Nasimxon on 9/6/2017.
 */
@Data
public class RecentSearchModel {
    private @IdRes
    int photo;
    private String txt1;
    private String txt2;
    private String txt3;
}
