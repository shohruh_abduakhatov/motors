package com.motors.phone.app.main.model;

import android.support.annotation.IdRes;

import lombok.Data;

/**
 * Created by LOC on 9/6/2017.
 */
@Data
public class SinceAwayModel {
    private @IdRes
    int photo;
    private String txt1;
    private String txt2;
    private String txt3;
    private String txt4;
    private String txt5;
}
