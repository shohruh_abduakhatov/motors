package com.motors.phone.app.main.view;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.main.presenter.WishDetailListPresenter;
import com.motors.phone.app.main.presenter.WishDetailListPresenterImpl;
import com.motors.phone.app.main.presenter.adapters.WishListHolder;
import com.motors.phone.core.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * A simple {@link Fragment} subclass.
 */

public class WishListDetailFragment extends BaseFragment implements WishDetailListPresenter {
    @BindView(R.id.list)
    EasyRecyclerView easyRecyclerView;
    @Inject
    WishDetailListPresenterImpl wishListPresenter;
    @Getter
    RecyclerArrayAdapter adapter;
    private Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_wishlist, container, false);
        ButterKnife.bind(this, v);
        initUI();
        return v;
    }

    private void initUI() {
        easyRecyclerView.getRecyclerView().setNestedScrollingEnabled(false);
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wishlist_dialog);
        dialog.findViewById(R.id.btnExit).setOnClickListener((view) -> dialog.dismiss());
        easyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        easyRecyclerView.setAdapter(adapter = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new WishListHolder(parent, R.layout.item_select);
            }
        });
        adapter.addHeader(new WishDetailHeader());
        fillWishList();
    }

    @Override
    public void fillWishList() {
        wishListPresenter.fillWishList();
    }

    class WishDetailHeader implements RecyclerArrayAdapter.ItemView {
        @Override
        public View onCreateView(ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.wish_list_header2, parent, false);
            return view;
        }

        @Override
        public void onBindView(View headerView) {
        }
    }
}