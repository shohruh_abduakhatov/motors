package com.motors.phone.app.ownership;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.motors.phone.R;
import com.motors.phone.util.FragmentManagerUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */

public class OwnershipDetailFragment extends Fragment {
    @BindView(R.id.spMotDateDay)
    AppCompatSpinner spMotDateDay;
    @BindView(R.id.spMotDateMonth)
    AppCompatSpinner spMotDateMonth;
    @BindView(R.id.spMotDateYear)
    AppCompatSpinner spMotDateYear;

    @BindView(R.id.spTaxDateDay)
    AppCompatSpinner spTaxDateDay;
    @BindView(R.id.spTaxDateMonth)
    AppCompatSpinner spTaxDateMonth;
    @BindView(R.id.spTaxDateYear)
    AppCompatSpinner spTaxDateYear;

    @BindView(R.id.spServiceDateDay)
    AppCompatSpinner spServiceDateDay;
    @BindView(R.id.spServiceDateMonth)
    AppCompatSpinner spServiceDateMonth;
    @BindView(R.id.spServiceDateYear)
    AppCompatSpinner spServiceDateYear;

    @BindView(R.id.spBreakDownDateDay)
    AppCompatSpinner spBreakDownDateDay;
    @BindView(R.id.spBreakDownDateMonth)
    AppCompatSpinner spBreakDownDateMonth;
    @BindView(R.id.spBreakDownDateYear)
    AppCompatSpinner spBreakDownDateYear;

    @BindView(R.id.spInsuranceDateDay)
    AppCompatSpinner spInsuranceDateDay;
    @BindView(R.id.spInsuranceDateMonth)
    AppCompatSpinner spInsuranceDateMonth;
    @BindView(R.id.spInsuranceDateYear)
    AppCompatSpinner spInsuranceDateYear;

    @BindView(R.id.spWarrantyDateDay)
    AppCompatSpinner spWarrantyDateDay;
    @BindView(R.id.spWarrantyDateMonth)
    AppCompatSpinner spWarrantyDateMonth;
    @BindView(R.id.spWarrantyDateYear)
    AppCompatSpinner spWarrantyDateYear;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ownership_detail, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        ArrayAdapter<String> adapterDay = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, new String[]{"01", "02", "03"});
        spMotDateDay.setAdapter(adapterDay);
        spTaxDateDay.setAdapter(adapterDay);
        spServiceDateDay.setAdapter(adapterDay);
        spInsuranceDateDay.setAdapter(adapterDay);
        spBreakDownDateDay.setAdapter(adapterDay);
        spWarrantyDateDay.setAdapter(adapterDay);
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, new String[]{"01", "02", "03"});
        spMotDateMonth.setAdapter(adapterMonth);
        spTaxDateMonth.setAdapter(adapterMonth);
        spServiceDateMonth.setAdapter(adapterMonth);
        spInsuranceDateMonth.setAdapter(adapterMonth);
        spBreakDownDateMonth.setAdapter(adapterMonth);
        spWarrantyDateMonth.setAdapter(adapterMonth);

        ArrayAdapter<String> adapterYear = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, new String[]{"2016", "2015", "2014"});
        spMotDateYear.setAdapter(adapterYear);
        spTaxDateYear.setAdapter(adapterYear);
        spServiceDateYear.setAdapter(adapterYear);
        spInsuranceDateYear.setAdapter(adapterYear);
        spBreakDownDateYear.setAdapter(adapterYear);
        spWarrantyDateYear.setAdapter(adapterYear);
    }

    @OnClick(R.id.btSave)
    public void go() {
        FragmentManagerUtils.replaceFragment((AppCompatActivity) getActivity(), R.id.flMain, new OwnershipFragment());
    }

}