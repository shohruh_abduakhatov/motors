package com.motors.phone.app.sell;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.motors.phone.R;
import com.motors.phone.core.BaseFragment;
import com.motors.phone.util.FragmentManagerUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Achilov Bakhrom on 9/11/17.
 */

public class SellDetailsFragment extends BaseFragment {
    @BindView(R.id.sp1)
    AppCompatSpinner sp1;
    @BindView(R.id.sp2)
    AppCompatSpinner sp2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selling_add_fragment, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, new String[]{"Alfa Romeo", "Alfa Romeo 2 ", "Alfa Romeo 3 "});
        sp1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, new String[]{"Giulia", "Giulia 2 ", "Giulia 3 "});
        sp2.setAdapter(adapter2);
    }

    @OnClick(R.id.btnAdvertiseCarOnMotors)
    public void goToAddingDescription() {
        FragmentManagerUtils.addFragment((AppCompatActivity) getActivity(), R.id.flMain, new SellAddDescriptionFragment());
    }

}
