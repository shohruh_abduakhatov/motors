package com.motors.phone.app.main.model;

import android.support.annotation.IdRes;

import lombok.Data;

/**
 * Created by Nasimxon on 9/6/2017.
 */
@Data
public class NotificationModel {
    private @IdRes
    int avatar;
    private String message;
    private String comment;
    private @IdRes
    int photo;
}
