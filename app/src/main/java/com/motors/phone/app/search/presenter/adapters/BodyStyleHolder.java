package com.motors.phone.app.search.presenter.adapters;

import android.view.ViewGroup;

import com.example.nasimxon.components.custom_ui.BodyStyleItemView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.R;
import com.motors.phone.common.net_objects.ChoiceItem;

/**
 * Created by KarimovSardor on 30/09/2017.
 */

public class BodyStyleHolder extends BaseViewHolder<ChoiceItem> {
    private BodyStyleItemView bodyStyleItemView;

    public BodyStyleHolder(ViewGroup parent, int res) {
        super(parent, res);
        bodyStyleItemView = $(R.id.bodyStyleItem);
    }

    @Override
    public void setData(ChoiceItem data) {
        super.setData(data);
        bodyStyleItemView.setBodyStyleItem(data.getName(), data.getValue());
        bodyStyleItemView.setSelected(data.isSelected());
    }
}