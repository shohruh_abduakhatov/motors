package com.motors.phone.app.buy;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.nasimxon.components.custom_ui.MileageTextView;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.motors.phone.R;
import com.motors.phone.app.buy.presenter.BuyDetailPresenterImpl;
import com.motors.phone.app.buy.presenter.BuyPresenter;
import com.motors.phone.app.buy.presenter.adapters.BuyCarsHolder;
import com.motors.phone.app.buy.presenter.adapters.SpecTechHolder;
import com.motors.phone.core.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import lombok.Getter;

/**
 * Created by Nasimxon on 9/16/2017.
 */

public class BuyDetailFragment extends BaseFragment implements BuyPresenter, ScrollView.OnTouchListener {
    @Inject
    BuyDetailPresenterImpl buyPresenter;
    @Getter
    @BindView(R.id.ervBuySimilarCars)
    EasyRecyclerView easyRecyclerView;
    @Getter
    RecyclerArrayAdapter adapterCars;
    Dialog dialog = null;
    @BindView(R.id.btnContact)
    LinearLayout btnContact;
    @BindView(R.id.scroll)
    ScrollView scrollview;
    @BindView(R.id.llExpandable)
    ExpandableLinearLayout llExpandable;
    @BindView(R.id.exTechSpec)
    ExpandableLinearLayout exTechSpec;
    @BindView(R.id.ervTechSpec)
    EasyRecyclerView ervTechSpec;
    @Getter
    RecyclerArrayAdapter adapterTechSpec;
    @BindView(R.id.ivTechSpecDropDown)
    ImageView ivTechSpecDropDown;
    @BindView(R.id.tvVehicleDescription)
    TextView tvVehicleDescription;
    @BindView(R.id.tvMileage)
    MileageTextView tvMileage;

    private float touch_up = 0;
    private float pos = 0.0f;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.buy_car_detail_fragment, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUI() {
        easyRecyclerView.getRecyclerView().setNestedScrollingEnabled(false);
        ervTechSpec.getRecyclerView().setNestedScrollingEnabled(false);
        pos = btnContact.getTranslationY();
        scrollview.setOnTouchListener(this);

        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wishlist_dialog);
        dialog.findViewById(R.id.btnExit).setOnClickListener((view) -> dialog.dismiss());

        easyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        easyRecyclerView.setAdapter(adapterCars = new RecyclerArrayAdapter(getContext()) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new BuyCarsHolder(parent, R.layout.item_used_cars2);
            }
        });
        ervTechSpec.setLayoutManager(new GridLayoutManager(getContext(), 3));
        ervTechSpec.setAdapter(adapterTechSpec = new RecyclerArrayAdapter(getContext(),
                new Object[]{"1", "1", "1", "1", "1", "1", "1", "1", "1"}) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new SpecTechHolder(parent, R.layout.item_spec_tech);
            }
        });
        tvMileage.setText(8213215);
        fillBuyCars();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE || motionEvent.getAction() == MotionEvent.ACTION_SCROLL) {
            if (touch_up > motionEvent.getY()) {
                ObjectAnimator move = ObjectAnimator.ofFloat(btnContact, View.TRANSLATION_Y, size.y);
                move.setDuration(500);
                move.start();
            } else {
                ObjectAnimator move = ObjectAnimator.ofFloat(btnContact, View.TRANSLATION_Y, pos * -1);
                move.setDuration(500);
                move.start();
            }
            touch_up = motionEvent.getY();
        }
        return false;
    }

    @OnClick(R.id.btnContactDealer)
    public void openDialog() {
        dialog.show();
    }

    @Override
    public void fillBuyCars() {
        buyPresenter.fillBuyCars();
    }

    @OnClick(R.id.sp)
    public void clickExpandable() {
        if (llExpandable.isExpanded()) {
            llExpandable.collapse();
        } else {
            llExpandable.expand();
        }
    }

    @OnClick(R.id.llTechSpec)
    public void clickTechSpec() {
        if (exTechSpec.isExpanded()) {
            exTechSpec.collapse();
            ivTechSpecDropDown.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rotate_to_up));
        } else {
            adapterTechSpec.notifyDataSetChanged();
            exTechSpec.expand();
        }
    }
}