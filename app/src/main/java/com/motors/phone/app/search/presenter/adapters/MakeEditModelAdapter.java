package com.motors.phone.app.search.presenter.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.motors.phone.R;

import java.util.ArrayList;

/**
 * Created by LavenderMoon on 10/11/17.
 */

public class MakeEditModelAdapter extends RecyclerView.Adapter<MakeEditModelAdapter.ViewHolder> {

    private ArrayList<String> arrayList = null;


    @Override
    public MakeEditModelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_make_edit, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MakeEditModelAdapter.ViewHolder holder, int position) {
        arrayList = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            arrayList.add("car" + String.valueOf(i));
        }
        holder.spinnerOne.setAdapter(new ArrayAdapter<String>(holder.spinnerOne.getContext(), android.R.layout.simple_list_item_1, arrayList));
        holder.spinnerTwo.setAdapter(new ArrayAdapter<String>(holder.spinnerTwo.getContext(), android.R.layout.simple_list_item_1, arrayList));
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private Spinner spinnerOne = null;
        private Spinner spinnerTwo = null;

        ViewHolder(View itemView) {
            super(itemView);

            this.spinnerOne = itemView.findViewById(R.id.spOne);
            this.spinnerTwo = itemView.findViewById(R.id.spTwo);
        }
    }
}