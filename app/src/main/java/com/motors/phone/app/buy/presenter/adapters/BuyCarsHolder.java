package com.motors.phone.app.buy.presenter.adapters;

import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.motors.phone.app.buy.model.BuyCarsModel;

/**
 * Created by Nasimxon on 9/6/2017.
 */

public class BuyCarsHolder extends BaseViewHolder<BuyCarsModel> {

    public BuyCarsHolder(ViewGroup parent, int res) {
        super(parent, res);
        itemView.setAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in));
    }

    @Override
    public void setData(BuyCarsModel data) {
        super.setData(data);
    }

}