package com.motors.phone.app.login.oauth;

/**
 * Created by Nasimxon on 10/5/2017.
 */

public interface AccountConstants {
    /**
     * Account type
     */
    public static final String ACCOUNT_TYPE = "com.motors.android.mobile";
    /**
     * Account name
     */
    public static final String WINGS_ACCOUNT_NAME = "Motors";
    /**
     * Provider id
     */
    public static final String WINGS_PROVIDER_AUTHORITY = "com.motors.tablet.sync";
    /**
     * Login type
     */
    public static final String LOGIN_TYPE_KEY = "loginType";

    public static final String ARG_IS_ADDING_NEW_ACCOUNT = "isAddingNewAccount";

}
