package com.motors.phone.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.nasimxon.components.utils.ChangeSearchDataEvent;
import com.example.nasimxon.components.utils.Constants;
import com.example.nasimxon.components.utils.RxBus;
import com.motors.phone.R;
import com.motors.phone.common.Co2EmissionsType;
import com.motors.phone.common.CostAnnualTaxType;
import com.motors.phone.common.FuelEfficiencyType;
import com.motors.phone.common.InsuranceType;
import com.motors.phone.common.TopSpeedType;
import com.motors.phone.common.iSearchType;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.core.BaseSearchFragment;

import java.io.ObjectInputValidation;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by abduakhatov on 10/25/17 at 5:05 PM.
 */

public class RunningCostsFragment extends BaseSearchFragment implements AdapterView.OnItemSelectedListener {
    @Inject
    SearchData searchData;
    @BindView(R.id.spAnnualTax)
    AppCompatSpinner spAnnualTax;
    @BindView(R.id.spInsuranceGroup)
    AppCompatSpinner spInsuranceGroup;
    @BindView(R.id.spFuelEfficiency)
    AppCompatSpinner spFuelEfficiency;
    @BindView(R.id.spEmissions)
    AppCompatSpinner spEmissions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.running_costs_fragment, null, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        ArrayAdapter<String> adapterAnnualTax = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getRunningCostTypesList(CostAnnualTaxType.values()));
        spAnnualTax.setAdapter(adapterAnnualTax);
        spAnnualTax.setOnItemSelectedListener(this);
        if (searchData.getSelectedCostAnnualTax() != null) spAnnualTax.setSelection(CostAnnualTaxType.getPosByvalue(searchData.getSelectedCostAnnualTax()));
        ArrayAdapter<String> adapterInsuranceGroup = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getRunningCostTypesList(InsuranceType.values()));
        spInsuranceGroup.setAdapter(adapterInsuranceGroup);
        spInsuranceGroup.setOnItemSelectedListener(this);
        if (searchData.getSelectedInsuranceGroup() != null) spInsuranceGroup.setSelection(InsuranceType.getPosByvalue(searchData.getSelectedInsuranceGroup()));
        ArrayAdapter<String> adapterFuelEfficiency = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getRunningCostTypesList(FuelEfficiencyType.values()));
        spFuelEfficiency.setAdapter(adapterFuelEfficiency);
        spFuelEfficiency.setOnItemSelectedListener(this);
        if (searchData.getSelectedFuelEfficiency() != null) spFuelEfficiency.setSelection(FuelEfficiencyType.getPosByvalue(searchData.getSelectedFuelEfficiency()));
        ArrayAdapter<String> adapterEmissions = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getRunningCostTypesList(Co2EmissionsType.values()));
        spEmissions.setAdapter(adapterEmissions);
        spEmissions.setOnItemSelectedListener(this);
        if (searchData.getSelectedCO2Emission() != null) spEmissions.setSelection(FuelEfficiencyType.getPosByvalue(searchData.getSelectedCO2Emission()));
    }

    @Override
    public void clickReset() {
        searchData.setSelectedCostAnnualTax("*");
        searchData.setSelectedInsuranceGroup("*");
        searchData.setSelectedFuelEfficiency("*");
        searchData.setSelectedCO2Emission("*");
        spAnnualTax.setSelection(0);
        spInsuranceGroup.setSelection(0);
        spFuelEfficiency.setSelection(0);
        spEmissions.setSelection(0);
    }

    @Override
    public void saveSearches() {
        searchData.setSelectedCostAnnualTax(spAnnualTax.getSelectedItem().toString());
        searchData.setSelectedInsuranceGroup(spInsuranceGroup.getSelectedItem().toString());
        searchData.setSelectedFuelEfficiency(spFuelEfficiency.getSelectedItem().toString());
        searchData.setSelectedCO2Emission(spEmissions.getSelectedItem().toString());
    }

    public List<String> getRunningCostTypesList(iSearchType[] list){
        List<String> res = new ArrayList<>();
        for (iSearchType i : list)
            res.add(i.getStrKey());
        return res;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spAnnualTax: {
                searchData.setSelectedCostAnnualTax(CostAnnualTaxType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            }
            case R.id.spInsuranceGroup: {
                searchData.setSelectedInsuranceGroup(InsuranceType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            }
            case R.id.spFuelEfficiency: {
                searchData.setSelectedFuelEfficiency(FuelEfficiencyType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            }
            case R.id.spEmissions: {
                searchData.setSelectedCO2Emission(Co2EmissionsType.getValueByKey(adapterView.getSelectedItem().toString()));
                break;
            }
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onStop() {
        super.onStop();
        RxBus.instanceOf().publish(new ChangeSearchDataEvent(Constants.RUNNING_COST));
    }
}
