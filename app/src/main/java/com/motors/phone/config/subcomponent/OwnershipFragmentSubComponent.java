package com.motors.phone.config.subcomponent;

import com.motors.phone.app.ownership.OwnershipFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface OwnershipFragmentSubComponent extends AndroidInjector<OwnershipFragment> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<OwnershipFragment> {
    }
}