package com.motors.phone.config.subcomponent;

import com.motors.phone.app.buy.BuyDetailFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface BuyDetailFragmentSubComponent extends AndroidInjector<BuyDetailFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<BuyDetailFragment> {
    }
}