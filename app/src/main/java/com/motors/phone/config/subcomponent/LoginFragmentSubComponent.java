package com.motors.phone.config.subcomponent;

import com.motors.phone.app.login.LoginFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface LoginFragmentSubComponent extends AndroidInjector<LoginFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<LoginFragment> {
    }
}