package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.JustListFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

//import com.motors.phone.app.message.ChatMessagesFragment;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface JustListFragmentSubComponent extends AndroidInjector<JustListFragment> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<JustListFragment> {
    }
}