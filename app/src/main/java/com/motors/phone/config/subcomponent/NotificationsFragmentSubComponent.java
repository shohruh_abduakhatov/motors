package com.motors.phone.config.subcomponent;

import com.motors.phone.app.notification.NotificationsFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface NotificationsFragmentSubComponent extends AndroidInjector<NotificationsFragment> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<NotificationsFragment> {
    }
}