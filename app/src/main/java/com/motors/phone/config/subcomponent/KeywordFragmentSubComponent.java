package com.motors.phone.config.subcomponent;

import com.motors.phone.app.login.RegisterFragment;
import com.motors.phone.app.search.KeywordFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface KeywordFragmentSubComponent extends AndroidInjector<KeywordFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<KeywordFragment> {
    }
}