package com.motors.phone.config.module;


import com.motors.phone.app.buy.BuyDetailFragment;
import com.motors.phone.app.buy.BuyFragment;
import com.motors.phone.app.login.LoginActivity;
import com.motors.phone.app.login.LoginFragment;
import com.motors.phone.app.login.RegisterFragment;
import com.motors.phone.app.main.view.MainFragment;
import com.motors.phone.app.main.view.UsedCarsDetailFragment;
import com.motors.phone.app.main.view.UsedCarsFragment;
import com.motors.phone.app.main.view.WishListDetailFragment;
import com.motors.phone.app.main.view.WishListFragment;
import com.motors.phone.app.message.ChatActivity;
import com.motors.phone.app.message.MessageFragment;
import com.motors.phone.app.notification.NotificationsFragment;
import com.motors.phone.app.ownership.OwnershipFragment;
import com.motors.phone.app.search.AgeMileageFragment;
import com.motors.phone.app.search.BodyStyleFragment;
import com.motors.phone.app.search.BudgetFragment;
import com.motors.phone.app.search.ColoursFragment;
import com.motors.phone.app.search.EngineFragment;
import com.motors.phone.app.search.KeywordFragment;
import com.motors.phone.app.search.MakeFragment;
import com.motors.phone.app.search.ModelFragment;
import com.motors.phone.app.search.JustListFragment;
import com.motors.phone.app.search.MoreOptionsFragment;
import com.motors.phone.app.search.PerformanceFragment;
import com.motors.phone.app.search.RunningCostsFragment;
import com.motors.phone.app.search.SearchActivity;
import com.motors.phone.app.search.SearchFragment;
import com.motors.phone.app.search.TowingWeightFragment;
import com.motors.phone.config.subcomponent.AgeMileageFragmentSubComponent;
import com.motors.phone.config.subcomponent.BodyStyleFragmentSubComponent;
import com.motors.phone.config.subcomponent.BudgetFragmentSubComponent;
import com.motors.phone.config.subcomponent.BuyFragmentSubComponent;
import com.motors.phone.config.subcomponent.ChatMessagesActivitySubComponent;
import com.motors.phone.config.subcomponent.ColourFragmentSubComponent;
import com.motors.phone.config.subcomponent.EngineSizeFragmentSubComponent;
import com.motors.phone.config.subcomponent.KeywordFragmentSubComponent;
import com.motors.phone.config.subcomponent.LoginActivitySubComponent;
import com.motors.phone.config.subcomponent.LoginFragmentSubComponent;
import com.motors.phone.config.subcomponent.MainFragmentSubComponent;
import com.motors.phone.config.subcomponent.MakeFragmentSubComponent;
import com.motors.phone.config.subcomponent.MessagesFragmentSubComponent;
import com.motors.phone.config.subcomponent.ModelFragmentSubComponent;
import com.motors.phone.config.subcomponent.MoreOptionsSubComponent;
import com.motors.phone.config.subcomponent.NotificationsFragmentSubComponent;
import com.motors.phone.config.subcomponent.JustListFragmentSubComponent;
import com.motors.phone.config.subcomponent.OwnershipFragmentSubComponent;
import com.motors.phone.config.subcomponent.PerformanceFragmentSubComponent;
import com.motors.phone.config.subcomponent.RegisterFragmentSubComponent;
import com.motors.phone.config.subcomponent.RunningCostFragmetSubComponent;
import com.motors.phone.config.subcomponent.SearchActivitySubComponent;
import com.motors.phone.config.subcomponent.SearchFragmentSubComponent;
import com.motors.phone.config.subcomponent.TowingWeingWeightFragmentSubComponent;
import com.motors.phone.config.subcomponent.UsedCarsDetailFragmentSubComponent;
import com.motors.phone.config.subcomponent.UsedCarsFragmentSubComponent;
import com.motors.phone.config.subcomponent.WishCarsFragmentSubComponent;
import com.motors.phone.config.subcomponent.WishDetailCarsFragmentSubComponent;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

//import com.motors.phone.app.message.ChatMessagesFragment;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */

@Module(subcomponents = {
        LoginActivitySubComponent.class,
        LoginFragmentSubComponent.class,
        MainFragmentSubComponent.class,
        BuyFragmentSubComponent.class,
        MainFragmentSubComponent.class,
        UsedCarsFragmentSubComponent.class,
        NotificationsFragmentSubComponent.class,
        MessagesFragmentSubComponent.class,
        OwnershipFragmentSubComponent.class,
        WishDetailCarsFragmentSubComponent.class,
        WishCarsFragmentSubComponent.class,
        BodyStyleFragmentSubComponent.class,
        MakeFragmentSubComponent.class,
        RegisterFragmentSubComponent.class,
        ChatMessagesActivitySubComponent.class,
        UsedCarsDetailFragmentSubComponent.class,
        ModelFragmentSubComponent.class,
        SearchFragmentSubComponent.class,
        EngineSizeFragmentSubComponent.class,
        JustListFragmentSubComponent.class,
        ColourFragmentSubComponent.class,
        KeywordFragmentSubComponent.class,
        BudgetFragmentSubComponent.class,
        AgeMileageFragmentSubComponent.class,
        MoreOptionsSubComponent.class,
        TowingWeingWeightFragmentSubComponent.class,
        PerformanceFragmentSubComponent.class,
        SearchActivitySubComponent.class,
        RunningCostFragmetSubComponent.class
})
public abstract class MainModule {

    @ContributesAndroidInjector
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();

    @ContributesAndroidInjector
    abstract BuyFragment contributeBuyFragment();

    @ContributesAndroidInjector
    abstract BuyDetailFragment contributeBuyDetailFragment();

    @ContributesAndroidInjector
    abstract UsedCarsFragment contributeUsedCarsFragment();

    @ContributesAndroidInjector
    abstract UsedCarsDetailFragment contributeUsedCarsDetailFragment();

    @ContributesAndroidInjector
    abstract WishListFragment contributeWishListFragment();

    @ContributesAndroidInjector
    abstract WishListDetailFragment contributeWishDetailListFragment();

    @ContributesAndroidInjector
    abstract MessageFragment contributeMessagesFragment();

    @ContributesAndroidInjector
    abstract NotificationsFragment contributeNotificationsFragment();

    @ContributesAndroidInjector
    abstract OwnershipFragment contributeOwnershipFragment();

    @ContributesAndroidInjector
    abstract ChatActivity contributeChatMessageActivity();

    @ContributesAndroidInjector
    abstract MakeFragment contributeMakeFragment();

    @ContributesAndroidInjector
    abstract BodyStyleFragment contributeBodyStyleFragment();

    @ContributesAndroidInjector
    abstract RegisterFragment contributeRegisterFragment();

    @ContributesAndroidInjector
    abstract LoginFragment contributeLoginFragment();

    @ContributesAndroidInjector
    abstract ModelFragment contributeModelFragment();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragment();

    @ContributesAndroidInjector
    abstract ColoursFragment contributeColourFragment();

    @ContributesAndroidInjector
    abstract EngineFragment contributeEngineFragment();

    @ContributesAndroidInjector
    abstract JustListFragment contributeJustListFragment();

    @ContributesAndroidInjector
    abstract BudgetFragment contributeBudgetFragment();

    @ContributesAndroidInjector
    abstract KeywordFragment contributeKeywordFragment();

    @ContributesAndroidInjector
    abstract AgeMileageFragment contributeAgeMileageFragment();

    @ContributesAndroidInjector
    abstract RunningCostsFragment contributeRunningCostsFragment();

    @ContributesAndroidInjector
    abstract PerformanceFragment contributePerformanceFragment();

    @ContributesAndroidInjector
    abstract MoreOptionsFragment contributeMoreOptionsFragment();

    @ContributesAndroidInjector
    abstract SearchActivity contributeSearchActivity();

    @ContributesAndroidInjector
    abstract TowingWeightFragment contributeTowingWeightFragment();

}
