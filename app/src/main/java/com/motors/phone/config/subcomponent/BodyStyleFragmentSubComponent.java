package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.BodyStyleFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface BodyStyleFragmentSubComponent extends AndroidInjector<BodyStyleFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<BodyStyleFragment> {
    }
}