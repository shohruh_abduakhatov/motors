package com.motors.phone.config.subcomponent;

import com.motors.phone.app.login.LoginActivity;
import com.motors.phone.app.message.MessageFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface LoginActivitySubComponent extends AndroidInjector<LoginActivity> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<LoginActivity> {
    }
}