package com.motors.phone.config.component;

import com.motors.phone.MotorsApplication;
import com.motors.phone.config.module.AppModule;
import com.motors.phone.config.module.MainModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;


/**
 * Created by Achilov Bakhrom on 9/4/17.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, MainModule.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(MotorsApplication application);

        AppComponent build();
    }

    void inject(MotorsApplication motorsApplication);
}
