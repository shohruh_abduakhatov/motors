package com.motors.phone.config.subcomponent;

import com.motors.phone.app.message.ChatActivity;
import com.motors.phone.app.search.MoreOptionsFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;


/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface MoreOptionsSubComponent extends AndroidInjector<MoreOptionsFragment> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<MoreOptionsFragment> {
    }
}