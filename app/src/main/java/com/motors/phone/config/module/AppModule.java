package com.motors.phone.config.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.motors.phone.MotorsApplication;
import com.motors.phone.common.net_objects.FullDetail;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.util.ApiManager;
import com.example.nasimxon.components.utils.Constants;
import com.motors.phone.util.ManagerUtil;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Achilov Bakhrom on 9/4/17.
 */

@Module
public class AppModule {
    @Provides
    public Context provideContext(MotorsApplication application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    public ManagerUtil provideManager(SharedPreferences sharedPreferences) {
        return new ManagerUtil(sharedPreferences);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Request.Builder builder = request.newBuilder()
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .method(request.method(), request.body());
                    return chain.proceed(builder.build());
                })
                .connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public ApiManager provideApiManager(Context context, Retrofit retrofit, OkHttpClient okHttpClient, SearchData searchData, Gson gson) {
        return new ApiManager(context, retrofit, okHttpClient, searchData, gson);
    }

    @Provides
    @Singleton
    public SearchData provideSearchData(Context context) {
        return new SearchData();
    }

    @Provides
    @Singleton
    public FullDetail provideFullDetail() {
        return new FullDetail();
    }
}
