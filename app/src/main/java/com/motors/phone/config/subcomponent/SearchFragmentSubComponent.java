package com.motors.phone.config.subcomponent;

import com.motors.phone.app.main.view.UsedCarsFragment;
import com.motors.phone.app.search.SearchFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface SearchFragmentSubComponent extends AndroidInjector<SearchFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<SearchFragment> {
    }
}