package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.AgeMileageFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by abduakhatov on 10/26/17 at 3:22 PM.
 */
@Subcomponent
public interface AgeMileageFragmentSubComponent extends AndroidInjector<AgeMileageFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<AgeMileageFragment>{

    }

}
