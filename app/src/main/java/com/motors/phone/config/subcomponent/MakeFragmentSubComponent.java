package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.MakeFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface MakeFragmentSubComponent extends AndroidInjector<MakeFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<MakeFragment> {
    }
}