package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.AgeMileageFragment;
import com.motors.phone.app.search.RunningCostsFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by abduakhatov on 10/26/17 at 3:55 PM.
 */
@Subcomponent
public interface RunningCostFragmetSubComponent extends AndroidInjector<RunningCostsFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<RunningCostsFragment>{

    }

}
