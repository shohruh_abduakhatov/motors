package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.ModelFragment;
import com.motors.phone.app.search.ModelTrimPagerFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface ModelFragmentSubComponent extends AndroidInjector<ModelFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<ModelFragment> {
    }
}