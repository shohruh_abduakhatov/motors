package com.motors.phone.config.subcomponent;

import com.motors.phone.app.main.view.UsedCarsDetailFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface UsedCarsDetailFragmentSubComponent extends AndroidInjector<UsedCarsDetailFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<UsedCarsDetailFragment> {
    }
}