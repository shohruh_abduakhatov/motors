package com.motors.phone.config.subcomponent;

import com.motors.phone.app.login.RegisterFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface RegisterFragmentSubComponent extends AndroidInjector<RegisterFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<RegisterFragment> {
    }
}