package com.motors.phone.config.subcomponent;

import com.motors.phone.app.notification.NotificationsFragment;
import com.motors.phone.app.search.EngineFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface EngineSizeFragmentSubComponent extends AndroidInjector<EngineFragment> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<EngineFragment> {
    }
}