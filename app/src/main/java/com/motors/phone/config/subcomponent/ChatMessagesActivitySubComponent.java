package com.motors.phone.config.subcomponent;

import com.motors.phone.app.message.ChatActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

//import com.motors.phone.app.message.ChatMessagesFragment;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface ChatMessagesActivitySubComponent extends AndroidInjector<ChatActivity> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<ChatActivity> {
    }
}