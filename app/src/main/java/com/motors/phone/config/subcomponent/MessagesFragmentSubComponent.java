package com.motors.phone.config.subcomponent;

import com.motors.phone.app.message.MessageFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface MessagesFragmentSubComponent extends AndroidInjector<MessageFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<MessageFragment> {
    }
}