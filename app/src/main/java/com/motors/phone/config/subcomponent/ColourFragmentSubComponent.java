package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.ColoursFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface ColourFragmentSubComponent extends AndroidInjector<ColoursFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ColoursFragment> {
    }
}