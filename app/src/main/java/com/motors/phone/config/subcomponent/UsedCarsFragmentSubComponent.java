package com.motors.phone.config.subcomponent;

import com.motors.phone.app.main.view.UsedCarsFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Achilov Bakhrom on 9/7/17.
 */
@Subcomponent
public interface UsedCarsFragmentSubComponent extends AndroidInjector<UsedCarsFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<UsedCarsFragment> {
    }
}