package com.motors.phone.config.subcomponent;

import com.motors.phone.app.buy.BuyFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Nasimxon on 9/7/17.
 */
@Subcomponent
public interface BuyFragmentSubComponent extends AndroidInjector<BuyFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<BuyFragment> {
    }
}