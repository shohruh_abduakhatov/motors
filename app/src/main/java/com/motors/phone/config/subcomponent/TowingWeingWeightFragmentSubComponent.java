package com.motors.phone.config.subcomponent;

import com.motors.phone.app.search.TowingWeightFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by abduakhatov on 10/25/17 at 3:46 PM.
 */
@Subcomponent
public interface TowingWeingWeightFragmentSubComponent extends AndroidInjector<TowingWeightFragment> {

    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<TowingWeightFragment>{

    }

}
