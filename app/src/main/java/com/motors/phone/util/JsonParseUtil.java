package com.motors.phone.util;

import com.example.nasimxon.components.utils.Constants;
import com.motors.phone.common.net_objects.ChoiceItem;
import com.motors.phone.common.net_objects.MotorsAddress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Nasimxon on 9/29/2017.
 */

public class JsonParseUtil {
    public static List<ChoiceItem> getFacetChoicesByTag(JSONObject jsonObject, String tag) throws JSONException {
        List<ChoiceItem> items = new ArrayList<>();
        JSONObject transmissions = null;
        if ((transmissions = jsonObject.getJSONObject(Constants.Data).getJSONObject("Facets").getJSONObject(tag)) != null) {
            for (Iterator<String> it = transmissions.keys(); it.hasNext(); ) {
                String key = it.next();
                ChoiceItem choiceItem = new ChoiceItem(key);
                choiceItem.setValue(transmissions.getInt(key));
                items.add(choiceItem);
            }
        }
        return items;
    }

    public static MotorsAddress parseGoogleLocation(JSONObject data) throws Exception {
        MotorsAddress address = null;
        boolean isValid = false;
        boolean isGotStreet = false;
        String county = "", town = "", street = "";
        if (data != null && data.optString("status") != null && data.optString("status").equals("OK")) {
            JSONArray jsonResults = data.optJSONArray("results");
            if (jsonResults != null && jsonResults.length() > 0) {
                JSONObject jsonAddress = (JSONObject) jsonResults.get(0);
                JSONArray jsonAddresses = jsonAddress.optJSONArray("address_components");
                String formattedAddress = jsonAddress.optString("formatted_address");
                if (jsonAddresses != null && jsonAddresses.length() > 0) {
                    JSONObject addressObject;
                    for (int counter = 0; counter < jsonAddresses.length(); counter++) {
                        addressObject = (JSONObject) jsonAddresses.get(counter);
                        JSONArray addressTypes = addressObject.getJSONArray("types");
                        if (addressTypes != null && addressTypes.length() > 0) {
                            String addressType = addressTypes.getString(0);
                            String longValue = addressObject.getString("long_name");
                            String shortValue = addressObject.getString("short_name");
                            if (addressType.equals("administrative_area_level_2")) {
                                county = longValue;
                            } else if (addressType.equals("postal_town")) {
                                town = longValue;
                            } else if (addressType.equals("route")) {
                                street = longValue;
                                isGotStreet = true;
                            } else if (addressType.equals("country")) {
                                if (shortValue.equals("GB")) {
                                    isValid = true;
                                }
                            }
                        }
                    }
                }
                if (isValid) {
                    address = new MotorsAddress();
                    address.setCountry(county);
                    address.setFormattedAddress(formattedAddress);
                    address.setTown(town);
                    address.setLine1(street);
                    address.setLine2(town);
                    address.setLine3(county);
                }
            }
        }
        return address;
    }

    public static String toJsonString(String value) {
        return value == null ? "" : value;
    }
}