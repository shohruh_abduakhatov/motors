package com.motors.phone.util;

import com.motors.phone.common.net_objects.ChoiceItem;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class ChoicesComparator implements Comparator<ChoiceItem> {

    private int maxLen;
    private static final String REGEX = "[0-9]+";

    public ChoicesComparator(int maxLen) {
        this.maxLen = maxLen;
    }

    @Override
    public int compare(ChoiceItem lhs, ChoiceItem rhs) {
        String o1 = lhs.getName();
        String o2 = rhs.getName();
        // both numbers
        if (o1.matches("[1-9]+") && o2.matches("[1-9]+")) {
            Integer integer1 = Integer.valueOf(o1);
            Integer integer2 = Integer.valueOf(o2);
            return integer1.compareTo(integer2);
        }
        // both string
        if (o1.matches("[a-zA-Z]+") && o2.matches("[a-zA-Z]+")) {
            return o1.compareTo(o2);
        }
        Pattern p = Pattern.compile(REGEX);
        Matcher m1 = p.matcher(o1);
        Matcher m2 = p.matcher(o2);
        List<String> list = new ArrayList<String>();
        while (m1.find()) {
            list.add(m1.group());
        }
        for (String string : list) {
            o1.replaceFirst(string, leftPad(string, "0", maxLen));
        }
        list.clear();
        while (m2.find()) {
            list.add(m2.group());
        }
        for (String string : list) {
            o2.replaceFirst(string, leftPad(string, "0", maxLen));
        }
        return o1.compareTo(o2);
    }

    public static String leftPad(String stringToPad, String padder, Integer size) {
        final StringBuilder strb = new StringBuilder(size);
        final StringCharacterIterator sci = new StringCharacterIterator(padder);
        while (strb.length() < (size - stringToPad.length())) {
            for (char ch = sci.first(); ch != CharacterIterator.DONE; ch = sci.next()) {
                if (strb.length() < (size - stringToPad.length())) {
                    strb.insert(strb.length(), String.valueOf(ch));
                }
            }
        }
        return strb.append(stringToPad).toString();
    }
}