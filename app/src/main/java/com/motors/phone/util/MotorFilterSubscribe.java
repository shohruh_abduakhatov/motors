package com.motors.phone.util;

import com.motors.phone.common.net_objects.ErrorMessage;
import com.motors.phone.common.net_objects.MotorsResponse;

import java.net.ConnectException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

/**
 * Created by Nasimxon on 10/9/2017.
 */

public abstract class MotorFilterSubscribe<T extends MotorsResponse> implements Observer<T> {
    protected abstract void onError(Throwable t, String message);
    protected abstract void onResult(T result);
    @Override
    public void onSubscribe(Disposable d) {}
    @Override
    public void onNext(T result) {
        if (result.isValid()) {
            onResult(result);
        } else {
            onError(null, ((ErrorMessage)result.getMessages().get(0)).getFriendMessage());
        }
    }
    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            HttpException ex = (HttpException) e;
            if (ex.code() == 401) {
//                onError(e, "");
            }
        } else if (e instanceof ConnectException) {
            onError(e, "Server connection failed");
        } else onError(e, null);
    }
    @Override
    public void onComplete() {}
}