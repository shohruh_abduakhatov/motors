package com.motors.phone.util;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.motors.phone.R;
import com.motors.phone.app.main.MainActivity;
import com.motors.phone.app.main.view.MainFragment;
import com.motors.phone.app.search.MakeFragment;
import com.motors.phone.app.search.ModelTrimPagerFragment;
import com.motors.phone.app.search.SearchActivity;
import com.motors.phone.app.search.SearchFragment;
import com.motors.phone.core.BaseActivity;

/**
 * Created by Achilov Bakhrom on 9/5/17.
 */

public class FragmentManagerUtils {
    public static final int WITHOUT_FRAME_ID = -1;

    //Common functions
    public static void replaceFragment(AppCompatActivity activity, @IdRes int frameId, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(frameId, fragment)
                .commit();
    }

    public static void addFragment(AppCompatActivity activity, @IdRes int frameId, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .add(frameId, fragment)
                .addToBackStack(null)
                .commit();
    }

    public static void clearAllFragments(AppCompatActivity activity) {
        while (activity.getFragmentManager().getBackStackEntryCount() != 0)
            activity.getSupportFragmentManager().popBackStack();
    }

    public static void popBackstack(AppCompatActivity activity) {
        activity.getSupportFragmentManager().popBackStack();
    }

    // MainActivity custom methods
    public static void openMainFragment(AppCompatActivity activity, @IdRes int frameId) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager.findFragmentById(frameId) instanceof MainFragment) return;
        int count = fragmentManager.getBackStackEntryCount();
        while (count < 0) popBackstack(activity);
        addFragment(activity, frameId, new MainFragment());
    }

    public static void backPressed(AppCompatActivity activity, @IdRes int frameId) {
        if (frameId == WITHOUT_FRAME_ID) {
            activity.finish();
            return;
        }
        if (activity instanceof MainActivity) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager.findFragmentById(frameId) instanceof MainFragment) {
                activity.finish();
                return;
            } else {
                popBackstack(activity);
            }
        }
    }

    public static boolean isMainPage(AppCompatActivity appCompatActivity, @IdRes int frameId) {
        return appCompatActivity
                .getSupportFragmentManager()
                .findFragmentById(frameId) instanceof MainFragment;
    }
}