package com.motors.phone.util;

import com.motors.phone.app.login.response.LoginResponse;
import com.motors.phone.common.net_objects.MotorsResponse;

import org.json.JSONObject;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Nasimxon on 9/28/2017.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("MobileAccount.ashx")
    Observable<MotorsResponse<Object>> register(@Field("token1") String token1,
                                @Field("token2") String token2,
                                @Field("apptype") String apptype,
                                @Field("clientversion") Double clientversion,
                                @Field("res") String res,
                                @Field("device") String device,
                                @FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("MobileAccount.ashx")
    Observable<MotorsResponse<LoginResponse>> signIn(@Field("token1") String token1,
                                                     @Field("token2") String token2,
                                                     @Field("apptype") String apptype,
                                                     @Field("clientversion") Double clientversion,
                                                     @Field("res") String res,
                                                     @Field("device") String device,
                                                     @FieldMap Map<String, String> data);

    @GET
    Observable<ResponseBody> getAddressByPostcode(@Url String url);

    @FormUrlEncoded
    @POST("MobileSearch.ashx")
    Observable<ResponseBody> getSearchParseByHandObjects(@Field("token1") String token1,
                                                                         @Field("token2") String token2,
                                                                         @Field("apptype") String apptype,
                                                                         @Field("clientversion") Double clientversion,
                                                                         @Field("res") String res,
                                                                         @Field("device") String device,
                                                                         @FieldMap Map<String, String> data);

}
