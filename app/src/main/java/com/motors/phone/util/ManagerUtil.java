package com.motors.phone.util;

import android.content.SharedPreferences;

import com.example.nasimxon.components.utils.Constants;
import com.motors.phone.common.SearchPanelType;

import javax.inject.Inject;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class ManagerUtil {
    private SharedPreferences preferences;

    @Inject
    public ManagerUtil(SharedPreferences sharedPreferences) {
        this.preferences = sharedPreferences;
    }

    public void saveUID(String uid) {
        preferences.edit().putString(Constants.UID_KEY, uid).apply();
    }

    public String getUID() {
        return preferences.getString(Constants.UID_KEY, null);
    }

    public void saveSearchPanelType(SearchPanelType type) {
        preferences.edit().putInt("search_panel_type", type.getValue()).apply();
    }

}
