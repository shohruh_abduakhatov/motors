package com.motors.phone.util;

import com.example.nasimxon.components.utils.Constants;

/**
 * User: Shahobiddin
 * Date: 11.04.13 16:55
 */
public class GUID {

    public static String createToken() {
        return createToken(false);
    }

    public static String createToken(boolean isAccount) {
        String token1 = UUID.uuid();

        return "token1=" + token1.toUpperCase() + "&token2=" + GUID.getSecondToken(token1) +
                "&apptype=" + Constants.APP_TYPE + "&device=" + Constants.DEVICE_NAME + "&clientversion=" + Constants.APP_VERSION;
    }

    public static String getSecondToken(String token) {
        System.out.println(token);
        int[] res = getMidleResults(token);
        int[] rel = new int[8];

        rel[0] = res[0];
        rel[1] = res[1];
        rel[2] = res[2];
        rel[3] = res[3];
        rel[4] = res[4] / 2 + res[5] / 2;
        rel[5] = res[5] / 2 + res[6] / 2;
        rel[6] = res[6] / 2 + res[7] / 2;
        rel[7] = res[7] / 2 + res[7] / 2;
        String retS = ifStringValid(Integer.toHexString(rel[0]))
                + ifStringValid(Integer.toHexString(rel[1]))
                + ifStringValid(Integer.toHexString(rel[2]))
                + ifStringValid(Integer.toHexString(rel[3]))
                + ifStringValid(Integer.toHexString(rel[4]))
                + ifStringValid(Integer.toHexString(rel[5]))
                + ifStringValid(Integer.toHexString(rel[6]))
                + ifStringValid(Integer.toHexString(rel[7]));

        System.out.println(getThrueString(toString(retS)));
        return getThrueString(toString(retS));
    }

    private static String ifStringValid(String s) {
        if (s.length() != 4) {
            if (s.length() == 3)
                s = "0" + s;
            else if (s.length() == 2)
                s = "00" + s;
            else if (s.length() == 1)
                s = "000" + s;
        }
        return s;
    }

    private static int[] getMidleResults(String s) {
        int[] outdata = new int[8];
        String ResultString = s.replace("-", "");
        outdata[0] = getThrueValue(ResultString.substring(0, 4));
        outdata[1] = getThrueValue(ResultString.substring(4, 8));
        outdata[2] = getThrueValue(ResultString.substring(8, 12));
        outdata[3] = getThrueValue(ResultString.substring(12, 16));
        outdata[4] = getThrueValue(ResultString.substring(16, 20));
        outdata[5] = getThrueValue(ResultString.substring(20, 24));
        outdata[6] = getThrueValue(ResultString.substring(24, 28));
        outdata[7] = getThrueValue(ResultString.substring(28, 32));
        return outdata;
    }

    private static int getThrueValue(String s) {
        int catchData = Integer.parseInt(s, 16);
        return ((catchData & 0x0000ff00) >> 8)
                | ((catchData & 0x000000ff) << 8);
    }

    private static String getThrueString(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s.substring(2, 4));
        sb.append(s.substring(0, 2));
        sb.append(s.substring(6, 8));
        sb.append(s.substring(4, 6));

        sb.append(s.substring(8, 9));

        sb.append(s.substring(11, 13));
        sb.append(s.substring(9, 11));

        sb.append(s.substring(13, 14));

        sb.append(s.substring(16, 18));
        sb.append(s.substring(14, 16));

        sb.append(s.substring(18, 19));

        sb.append(s.substring(21, 23));
        sb.append(s.substring(19, 21));

        sb.append(s.substring(23, 24));

        sb.append(s.substring(26, 28));
        sb.append(s.substring(24, 26));
        sb.append(s.substring(30, 32));
        sb.append(s.substring(28, 30));
        sb.append(s.substring(34, 36));
        sb.append(s.substring(32, 34));

        return sb.toString();
    }

    private static String toString(String s) {
        String raw = s.toUpperCase();
        StringBuilder sb = new StringBuilder();
        sb.append(raw.substring(0, 8));
        sb.append("-");
        sb.append(raw.substring(8, 12));
        sb.append("-");
        sb.append(raw.substring(12, 16));
        sb.append("-");
        sb.append(raw.substring(16, 20));
        sb.append("-");
        sb.append(raw.substring(20));

        return sb.toString();
    }
}
