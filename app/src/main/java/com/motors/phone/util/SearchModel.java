package com.motors.phone.util;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nasimxon on 9/28/2017.
 */

public class SearchModel implements Serializable {
    @SerializedName("token1")
    private String token1;
    @SerializedName("token2")
    private String token2;
    @SerializedName("apptype")
    private String apptype;
    @SerializedName("clientversion")
    private Double clientVersion;
    @SerializedName("res")
    private String res;
    @SerializedName("device")
    private String device;
    @SerializedName("Data")
    private Data data;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getToken1() {
        return token1;
    }

    public void setToken1(String token1) {
        this.token1 = token1;
    }

    public String getToken2() {
        return token2;
    }

    public void setToken2(String token2) {
        this.token2 = token2;
    }

    public String getApptype() {
        return apptype;
    }

    public void setApptype(String apptype) {
        this.apptype = apptype;
    }

    public Double getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(Double clientVersion) {
        this.clientVersion = clientVersion;
    }

    public static class Data implements Serializable {
        @SerializedName("Type")
        private Integer type;

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "{Type:" + type + "}";
        }
    }
}
