package com.motors.phone.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.nasimxon.components.utils.Constants;
import com.google.gson.Gson;
import com.motors.phone.R;
import com.motors.phone.app.buy.model.BuyCarsModel;
import com.motors.phone.app.login.model.LoginModel;
import com.motors.phone.app.login.model.RegisterModel;
import com.motors.phone.app.login.response.LoginResponse;
import com.motors.phone.app.main.model.NearestDealersModel;
import com.motors.phone.app.main.model.NotificationModel;
import com.motors.phone.app.main.model.RecentSearchModel;
import com.motors.phone.app.main.model.SinceAwayModel;
import com.motors.phone.app.main.model.UsedCars;
import com.motors.phone.app.main.model.WishListModel;
import com.motors.phone.app.message.model.ChatMessagesModel;
import com.motors.phone.app.message.model.MessagesModel;
import com.motors.phone.common.net_objects.MotorsResponse;
import com.motors.phone.common.net_objects.SearchData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

/**
 * Created by Nasimxon on 9/4/17.s
 */

public class ApiManager {
    private String GoogleGetAddressURL = "http://maps.googleapis.com/maps/api/geocode/json?";
    private ApiService apiService;
    private OkHttpClient okHttpClient;
    private Context context;
    private SearchData searchData;
    private Gson gson;

    public ApiManager(Context context, Retrofit retrofit, OkHttpClient okHttpClient, SearchData searchData, Gson gson) {
        this.context = context;
        this.okHttpClient = okHttpClient;
        apiService = retrofit.create(ApiService.class);
        this.searchData = searchData;
        this.gson = gson;
    }

    public Observable<List<NotificationModel>> getNotification() {
        return Observable.create((ObservableOnSubscribe<List<NotificationModel>>) e -> {
            List<NotificationModel> list = new ArrayList<>();
            NotificationModel first = new NotificationModel();
            first.setAvatar(R.drawable.banner);
            first.setComment("Subject: BMW M4 Test Drive");
            first.setMessage("Replied to a message");
            first.setPhoto(R.drawable.banner);
            NotificationModel second = new NotificationModel();
            second.setAvatar(R.drawable.banner);
            second.setComment("Subject: BMW M4 Test Drive");
            second.setMessage("Replied to a message");
            second.setPhoto(R.drawable.banner);
            NotificationModel third = new NotificationModel();
            third.setAvatar(R.drawable.banner);
            third.setComment("Subject: BMW M4 Test Drive");
            third.setMessage("Replied to a message");
            third.setPhoto(R.drawable.banner);
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SinceAwayModel>> getSinceAway() {
        return Observable.create((ObservableOnSubscribe<List<SinceAwayModel>>) e -> {
            List<SinceAwayModel> list = new ArrayList<>();
            SinceAwayModel first = new SinceAwayModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("17.900 Mileage");
            first.setTxt4("Petrol");
            first.setTxt5("Oct 2013(62)");
            SinceAwayModel second = new SinceAwayModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            second.setTxt3("17.900 Mileage");
            second.setTxt4("Petrol");
            second.setTxt5("Oct 2013(62)");
            SinceAwayModel third = new SinceAwayModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            third.setTxt3("17.900 Mileage");
            third.setTxt4("Petrol");
            third.setTxt5("Oct 2013(62)");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<RecentSearchModel>> getRecentSearch() {
        return Observable.create((ObservableOnSubscribe<List<RecentSearchModel>>) e -> {
            List<RecentSearchModel> list = new ArrayList<>();
            RecentSearchModel first = new RecentSearchModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("17.900 Mileage");
            RecentSearchModel second = new RecentSearchModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            second.setTxt3("17.900 Mileage");
            RecentSearchModel third = new RecentSearchModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            third.setTxt3("17.900 Mileage");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<WishListModel>> getSavedSearch() {
        return Observable.create((ObservableOnSubscribe<List<WishListModel>>) e -> {
            List<WishListModel> list = new ArrayList<>();
            WishListModel first = new WishListModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("17.900 Mileage");
            WishListModel second = new WishListModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            second.setTxt3("17.900 Mileage");
            WishListModel third = new WishListModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            third.setTxt3("17.900 Mileage");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<NearestDealersModel>> getNearestDealer() {
        return Observable.create((ObservableOnSubscribe<List<NearestDealersModel>>) e -> {
            List<NearestDealersModel> list = new ArrayList<>();
            NearestDealersModel first = new NearestDealersModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            NearestDealersModel second = new NearestDealersModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            NearestDealersModel third = new NearestDealersModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<BuyCarsModel>> getBuyCars() {
        return Observable.create((ObservableOnSubscribe<List<BuyCarsModel>>) e -> {
            List<BuyCarsModel> list = new ArrayList<>();
            BuyCarsModel first = new BuyCarsModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            first.setTxt4("Now $12.956");
            first.setTxt5("Now $12.956");
            first.setTxt6("Now $12.956");
            BuyCarsModel second = new BuyCarsModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            first.setTxt4("Now $12.956");
            first.setTxt5("Now $12.956");
            first.setTxt6("Now $12.956");
            BuyCarsModel third = new BuyCarsModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            first.setTxt4("Now $12.956");
            first.setTxt5("Now $12.956");
            first.setTxt6("Now $12.956");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<WishListModel>> getWishCars() {
        return Observable.create((ObservableOnSubscribe<List<WishListModel>>) e -> {
            List<WishListModel> list = new ArrayList<>();
            WishListModel first = new WishListModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            WishListModel second = new WishListModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            WishListModel third = new WishListModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<MessagesModel>> getMessages() {
        return Observable.create((ObservableOnSubscribe<List<MessagesModel>>) e -> {
            List<MessagesModel> list = new ArrayList<>();
            MessagesModel first = new MessagesModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            MessagesModel second = new MessagesModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            MessagesModel third = new MessagesModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            first.setTxt3("Now $12.956");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ChatMessagesModel>> getChatMessages() {
        return Observable.create((ObservableOnSubscribe<List<ChatMessagesModel>>) e -> {
            List<ChatMessagesModel> list = new ArrayList<>();
            ChatMessagesModel first = new ChatMessagesModel();
            first.setPhoto(R.drawable.banner);
            first.setTxt1("Reduced by $2380");
            first.setTxt2("Now $12.956");
            ChatMessagesModel second = new ChatMessagesModel();
            second.setPhoto(R.drawable.banner);
            second.setTxt1("Reduced by $2380");
            second.setTxt2("Now $12.956");
            ChatMessagesModel third = new ChatMessagesModel();
            third.setPhoto(R.drawable.banner);
            third.setTxt1("Reduced by $2380");
            third.setTxt2("Now $12.956");
            list.add(first);
            list.add(second);
            list.add(third);
            e.onNext(list);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<UsedCars>> getUsedCars() {
        return Observable.create((ObservableOnSubscribe<List<UsedCars>>) e -> {
            List<UsedCars> usedCars = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                usedCars.add(new UsedCars());
            }
            e.onNext(usedCars);
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MotorsResponse<Object>> register(RegisterModel user) {
        String token1 = UUID.uuid();
        String token2 = GUID.getSecondToken(token1);
        String appType = Constants.APP_TYPE;
        String device = Constants.DEVICE_NAME;
        String request = new Gson().toJson(user);
        Map<String, String> params = new HashMap<>();
        params.put(Constants.Data, request);
        return apiService.register(token1, token2, appType, 3.7,
                Constants.P_MOTCRAC, device, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getFacets() {
        String token1 = UUID.uuid();
        String token2 = GUID.getSecondToken(token1);
        String appType = Constants.APP_TYPE;
        String device = Constants.DEVICE_NAME;
        Map<String, String> params = new HashMap<>();
        params.put(Constants.Data, gson.toJson(searchData));
        return apiService.getSearchParseByHandObjects(token1, token2, appType, 3.7,
                Constants.P_MOTFACET, device, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MotorsResponse<LoginResponse>> login(LoginModel user) {
        String token1 = UUID.uuid();
        String token2 = GUID.getSecondToken(token1);
        String appType = Constants.APP_TYPE;
        String device = Constants.DEVICE_NAME;
        String request = new Gson().toJson(user);
        Map<String, String> params = new HashMap<>();
        params.put(Constants.Data, request);
        return apiService.signIn(token1, token2, appType, 3.7,
                Constants.P_MOTLOIN, device, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getAddressByPostcode(String postCode) {
        return apiService.getAddressByPostcode(GoogleGetAddressURL + "address=" + postCode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void cancelAllRequest() {
        okHttpClient.dispatcher().cancelAll();
    }

    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return activeNetwork.isConnected();
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return activeNetwork.isConnected();
        }
        return false;
    }
}