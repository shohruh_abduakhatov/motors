package com.motors.phone.core;

import com.motors.phone.common.net_objects.ChoiceItem;

import java.util.List;

/**
 * Created by Achilov Bakhrom on 9/4/17.
 */

public abstract class BaseSearchFragment extends BaseFragment {
    public abstract void clickReset();
    public abstract void saveSearches();
    public void setData(List<ChoiceItem> list){};
}