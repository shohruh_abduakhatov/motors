package com.motors.phone.core;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.motors.phone.util.FragmentManagerUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Achilov Bakhrom on 9/4/17.
 */

public abstract class BaseFragment extends Fragment {
    @Getter
    @Setter
    private boolean isRequestSend;

    public void popBackStack() {
        FragmentManagerUtils.popBackstack((AppCompatActivity) getContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().invalidateOptionsMenu();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStop() {
        super.onStop();
        View view = getView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void makeAlertText(final EditText editText, int text_id) {
        editText.requestFocus();
        editText.setError(getText(text_id));
    }

    protected void makeAlertText(final EditText editText, String text) {
        editText.requestFocus();
        editText.setError(text);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}