package com.motors.phone.core;

import android.content.Context;
import android.content.DialogInterface;

import com.example.nasimxon.components.custom_ui.ProgressLoadingDialog;
import com.motors.phone.common.net_objects.FullDetail;
import com.motors.phone.common.net_objects.SearchData;
import com.motors.phone.util.ApiManager;
import com.motors.phone.util.ManagerUtil;

import javax.inject.Inject;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class BasePresenterImp<T extends BaseFragment> implements DialogInterface.OnCancelListener {
    @Inject protected ManagerUtil managerUtil;
    @Inject protected ApiManager apiManager;
    @Inject protected FullDetail fullDetail;
    @Inject protected SearchData searchData;
    protected ProgressLoadingDialog loadingDialog;
    protected T fragment;
    protected Context context;

    public BasePresenterImp(T fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        loadingDialog = new ProgressLoadingDialog(fragment.getContext());
        loadingDialog.setOnCancelListener(this);
    }
    @Override
    public void onCancel(DialogInterface dialogInterface) {
        fragment.setRequestSend(false);
        apiManager.cancelAllRequest();
    }

}
