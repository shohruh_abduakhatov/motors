package com.motors.phone.common;

/**
 * Created by LavenderMoon on 10/25/17.
 */

public enum TankType implements iSearchType {
    Any("Tank Range (Any)", "1"),
    TANK_TYPE_200("Up to 4 secs 0-62 mph", "0 - 4"),
    TANK_TYPE_400("Up to 5 secs 0-62 mph", "0 - 5"),
    TANK_TYPE_600("Up to 6 secs 0-62 mph", "0 - 6"),
    TANK_TYPE_800("Up to 7 secs 0-62 mph", "0 - 7"),
    TANK_TYPE_1000("Up to 8 secs 0-62 mph","0 - 8"),;

    private String key;
    private String value;

    TankType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String arg) {
        for (TankType a : TankType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < TankType.values().length; i++) {
            if (TankType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}
