package com.motors.phone.common.search;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum SortByType {
    Default(-1, "Default"),
    Distance(0, "Sort by Distance"),
    LowestPrice(1, "Lowest price"),
    HighestPrice(2, "Highest price"),
    Newest(3, "Most Recently Added"),
    Random(4, "Random"),
    NameAsc(5, "A-Z"),
    NameDesc(6, "Z-A"),
    GreatestPriceReduction(7, "Greatest Price Reduction"),
    NewCarNameAsc(8, "A"),
    NewCarLowestPrice(9, "B"),
    NewCarHighestPrice(10, "C");

    @Getter
    @Setter
    private int key;
    @Getter
    @Setter
    private String title;

    private SortByType() {
    }

    private SortByType(int key, String title) {
        this.key = key;
        this.title = title;
    }

    public static SortByType getType(int key) {
        SortByType result = Distance;
        if (key != result.key) {
            for (SortByType item : SortByType.values()) {
                if (key == item.key) {
                    return item;
                }
            }
        }
        return result;
    }

    public static SortByType getTypeByTitle(String title) {
        SortByType result = Distance;
        if (!title.equals(result.title)) {
            for (SortByType item : SortByType.values()) {
                if (title.equals(item.title)) {
                    return item;
                }
            }
        }
        return result;
    }
}

