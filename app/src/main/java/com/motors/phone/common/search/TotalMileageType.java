package com.motors.phone.common.search;

import com.motors.phone.common.iSearchType;

/**
 * Created by abduakhatov on 10/26/17 at 3:45 PM.
 */

public enum TotalMileageType implements iSearchType {
    Any( -1, "Mileage (Any)"),
    Total_100( 100, "up to 100 miles"),
    Total_1000( 1000, "up to 1,000 miles"),
    Total_5000( 5000, "up to 5,000 miles"),
    Total_10000( 10000, "up to 10,000 miles"),
    Total_20000( 20000, "up to 20,000 miles"),
    Total_30000( 30000, "up to 30,000 miles"),
    Total_40000( 40000, "up to 40,000 miles"),
    Total_50000( 50000, "up to 50,000 miles"),
    Total_60000( 60000, "up to 60,000 miles"),
    Total_70000( 70000, "up to 70,000 miles"),
    Total_80000( 80000, "up to 80,000 miles"),
    Total_90000( 90000, "up to 90,000 miles"),
    Total_100000( 100000, "up to 100,000 miles"),
    Total_110000( 110000, "over 100,000 miles");

    private int key;
    private String value;

    TotalMileageType(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public static int getMilagePosByKey(int key) {
        for (int i = 0; i < TotalMileageType.values().length; i++) {
            if (TotalMileageType.values()[i].getIntKey() == key)
                return i;
        }
        return 0;
    }

    public static int getMileageByValue(String value) {
        for (TotalMileageType totalMileageType : TotalMileageType.values()) {
            if (totalMileageType.getStrValue().equalsIgnoreCase(value)) return totalMileageType.getIntKey();
        }
        return TotalMileageType.Any.getIntKey();
    }

    @Override
    public String getStrKey() {
        return null;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return key;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}
