package com.motors.phone.common.net_objects;

import com.example.nasimxon.components.utils.Constants;
import com.google.gson.annotations.SerializedName;
import com.motors.phone.common.AccelerationType;
import com.motors.phone.common.CarType;
import com.motors.phone.common.Co2EmissionsType;
import com.motors.phone.common.CostAnnualTaxType;
import com.motors.phone.common.EngineSizeType;
import com.motors.phone.common.FuelEfficiencyType;
import com.motors.phone.common.InsuranceType;
import com.motors.phone.common.PowerType;
import com.motors.phone.common.SearchPanelType;
import com.motors.phone.common.TankType;
import com.motors.phone.common.TopSpeedType;
import com.motors.phone.common.search.SearchType;
import com.motors.phone.common.search.SellerType;
import com.motors.phone.common.search.SortByType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class SearchData implements Serializable {
    //basic search fields
    transient private SortByType sortByType;
    transient private SellerType sellerType;
    transient private CarType carType;
    transient private SearchPanelType searchPanelType = SearchPanelType.UsedCars;
    transient private SearchType searchType = SearchType.Normal;

    //advanced search fields
    @SerializedName("VehicleId")
    private Integer vehicleId;
    @SerializedName("MinAge")
    private Integer minAge;
    @SerializedName("MaxAge")
    private Integer maxAge;
    @SerializedName("MinMileage")
    private Integer minMileage;
    @SerializedName("MaxMileage")
    private Integer maxMileage;
    @SerializedName("minCost")
    private Integer minCost;
    @SerializedName("maxCost")
    private Integer maxCost;
    @SerializedName("MinPrice")
    private Integer minPrice;
    @SerializedName("MaxPrice")
    private Integer maxPrice;
    @SerializedName("Distance")
    private Integer distance = 1000;
    @SerializedName("PageSize")
    private Integer pageSize;
    @SerializedName("FirstResult")
    private Integer firstResult = 1;
    @SerializedName("CurrentPage")
    private Integer currentPage = 1;
    @SerializedName("TotalCount")
    private Integer totalCount;
    @SerializedName("DealerId")
    private Integer dealerId;
    @SerializedName(Constants.Type)
    private Integer type = SearchPanelType.UsedCars.getValue();

    @SerializedName("deposit")
    private Integer deposit;
    @SerializedName("term")
    private Integer term;
    @SerializedName("mileage")
    private Integer mileage;

    @SerializedName("SelectedTopSpeed")
    private String selectedTopSpeed;
    @SerializedName("SelectedPower")
    private String selectedPower;
    @SerializedName("SelectedAcceleration")
    private String selectedAcceleration;
    @SerializedName("SelectedInsuranceGroup")
    private String selectedInsuranceGroup;
    @SerializedName("SelectedFuelEfficiency")
    private String selectedFuelEfficiency;
    @SerializedName("SelectedCostAnnualTax")
    private String selectedCostAnnualTax;
    @SerializedName("SelectedTankRange")
    private String selectedTankRange;
    @SerializedName("SelectedCO2Emission")
    private String selectedCO2Emission;
    @SerializedName("SelectedEngineSize")
    private String selectedEngineSize;
    @SerializedName("SelectedSoldStatus")
    private String selectedSoldStatus;
    @SerializedName("SelectedAdvertType")
    private String selectedAdvertType;
    @SerializedName("PostCode")
    private String postCode;
    @SerializedName("DealerName")
    private String dealerName;
    transient private String lat;
    transient private String lng;
    transient private String locationId;
    transient private String jsonString;
    transient private boolean isMonthly;

    @SerializedName("IsAdvanced")
    private boolean isAdvanced;
    private boolean hasPostcodeError;
    @SerializedName("IsServerError")
    private boolean isServerError;
    @SerializedName("IsRecentlyAdded")
    private boolean isRecentlyAdded;
    @SerializedName("IsReduced")
    private boolean isReduced;
    @SerializedName("IsNewSearch")
    private boolean isNewSearch = true;
    @SerializedName("IsSmartSearch")
    private boolean isSmartSearch = false;
    @SerializedName("IsLogging")
    private boolean isLogging = true;

    transient private boolean excluded;
    transient private boolean fromSmartSearch;
    transient private List<ChoiceItem> transmission = new ArrayList<>();
    transient private List<ChoiceItem> fuelTypes = new ArrayList<>();
    transient private List<ChoiceItem> bodyStyles = new ArrayList<>();
    transient private List<ChoiceItem> doors = new ArrayList<>();
    transient private List<ChoiceItem> seats = new ArrayList<>();
    transient private List<ChoiceItem> safetyRatings = new ArrayList<>();
    transient private List<ChoiceItem> otherOptions = new ArrayList<>();
    transient private List<ChoiceItem> colours = new ArrayList<>();

    transient private Map<String, List<String>> makeModels = new HashMap<>();
    @SerializedName("MakeModels")
    private List<ChoiceItem> makes = new ArrayList<>();
    @SerializedName("Models")
    private List<ChoiceItem> models = new ArrayList<>();
    @SerializedName("BodyStyles")
    private List<String> bdStyles = new ArrayList<String>();
    @SerializedName("Colours")
    private List<String> clours = new ArrayList<String>();
    @SerializedName("Keywords")
    private Set<String> keywords = new HashSet<>();
    @SerializedName("Transmissions")
    private List<String> trans = new ArrayList<String>();
    @SerializedName("FuelTypes")
    private List<String> flTypes = new ArrayList<String>();
    @SerializedName("Doors")
    private List<String> drs = new ArrayList<String>();
    @SerializedName("Seats")
    private List<String> sts = new ArrayList<String>();
    @SerializedName("SafetyRatings")
    private List<String> sftyRatings = new ArrayList<String>();
    @SerializedName("OtherOptions")
    private List<String> otherOpts = new ArrayList<String>();

    public void clearMakeModels() {
        if (makes != null)
            makes.clear();
        if (models != null)
            models.clear();
    }

    public void reset() {
        totalCount = 0;
        pageSize = Constants.PER_PAGE;
        firstResult = 1;
        hasPostcodeError = false;
        sortByType = SortByType.Distance;
        isLogging = false;
        excluded = false;
        //Set default values
        this.setSellerType(SellerType.AllSellers);
        this.setCarType(CarType.AllCars);
        setBodyStyle(null);
        setTransmission(null);
        setFuelTypes(null);
        setDoors(null);
        setColours(null);
        setSeats(null);
        setSafetyRatings(null);
        setOtherOptions(null);
        setMakes(null);
        setModels(null);
        this.setDistance(Integer.valueOf(Constants.NATIONAL_DISTANCE));
        this.keywords.clear();
        minPrice = null;
        maxPrice = null;
        minCost = null;
        maxCost = null;
        minAge = null;
        minMileage = null;
        selectedCostAnnualTax = null;
        selectedInsuranceGroup = null;
        selectedFuelEfficiency = null;
        selectedCO2Emission = null;
        selectedEngineSize = null;
        term = null;
        setMonthly(false);
    }

    public void setBodyStyle(List<ChoiceItem> bodyStyle) {
        this.bodyStyles = (bodyStyle == null) ? new ArrayList<>() : new ArrayList<>(bodyStyle);
        if (bdStyles == null) bdStyles = new ArrayList<>();
        bdStyles.clear();
        for (ChoiceItem choiceItem : this.bodyStyles) {
            bdStyles.add(choiceItem.getName());
        }
    }

    public void setColours(List<ChoiceItem> colours) {
        this.colours = (colours == null) ? new ArrayList<>() : new ArrayList<>(colours);
        if (clours == null) clours = new ArrayList<>();
        clours.clear();
        for (ChoiceItem choiceItem : this.colours) {
            clours.add(choiceItem.getName());
        }
    }

    public void setDoors(List<ChoiceItem> doors) {
        this.doors = (doors == null) ? new ArrayList<>() : new ArrayList<>(doors);
        if (drs == null) drs = new ArrayList<>();
        drs.clear();
        for (ChoiceItem choiceItem : this.doors) {
            drs.add(choiceItem.getName());
        }
    }

    public void setSeats(List<ChoiceItem> seats) {
        this.seats = (seats == null) ? new ArrayList<>() : new ArrayList<>(seats);
        if (sts == null) sts = new ArrayList<>();
        sts.clear();
        for (ChoiceItem choiceItem : this.seats) {
            sts.add(choiceItem.getName());
        }
    }

    public void setOtherOptions(List<ChoiceItem> options) {
        this.otherOptions = (options == null) ? new ArrayList<>() : new ArrayList<>(options);
        if (otherOpts == null) otherOpts = new ArrayList<>();
        otherOpts.clear();
        for (ChoiceItem choiceItem : this.otherOptions) {
            otherOpts.add(choiceItem.getName());
        }
    }

    public void safetyRatings(List<ChoiceItem> safety) {
        this.safetyRatings = (safety == null) ? new ArrayList<>() : new ArrayList<>(safety);
        if (sftyRatings == null) sftyRatings = new ArrayList<>();
        sftyRatings.clear();
        for (ChoiceItem choiceItem : this.safetyRatings) {
            sftyRatings.add(choiceItem.getName());
        }
    }

    public void setFuelTypes(List<ChoiceItem> fuelTypes) {
        this.fuelTypes = (fuelTypes == null) ? new ArrayList<>() : new ArrayList<>(fuelTypes);
        if (flTypes == null) flTypes = new ArrayList<>();
        flTypes.clear();
        for (ChoiceItem choiceItem : this.fuelTypes) {
            flTypes.add(choiceItem.getName());
        }
    }

    public void setTransmission(List<ChoiceItem> transmission) {
        this.transmission = (transmission == null) ? new ArrayList<>() : new ArrayList<>(transmission);
        if (trans == null) trans = new ArrayList<>();
        for (ChoiceItem choiceItem : this.transmission) {
            trans.add(choiceItem.getName());
        }
    }

    public void removeModelsByParent(ChoiceItem item) {
        if (models != null)
            for (int i = 0; i < models.size(); i++) {
                if (models.get(i).getParentName().equalsIgnoreCase(item.getName())) {
                    models.remove(i);
                    i--;
                }
            }
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMakes(List<ChoiceItem> makes) {
        this.makes = makes;
    }

    public boolean isChangeMake() {
        return makes != null && !makes.isEmpty();
    }

    public boolean isChangeBudget() {
        return maxPrice != null && maxPrice != -1 || minPrice != null && minPrice != -1;
    }

    public boolean isChangeAgeMileage() {
        return minAge != null && minAge != -1 || minMileage != null && minMileage != -1;
    }

    public boolean isChangeTransmissions() {
        return fuelTypes != null && !fuelTypes.isEmpty();
    }

    public boolean isChangeFuelType() {
        return bodyStyles != null && !bodyStyles.isEmpty();
    }

    public boolean isChangeBodyStyle() {
        return bdStyles != null && !bdStyles.isEmpty();
    }

    public boolean isChageColour() {
        return clours != null && !clours.isEmpty();
    }

    public boolean isChangeVehicle() {
        return selectedEngineSize != null && !selectedEngineSize.isEmpty() && !selectedEngineSize.equalsIgnoreCase(EngineSizeType.Any.getKey())
                || drs != null && !drs.isEmpty()
                || sts != null && !sts.isEmpty()
                || sftyRatings != null && !sftyRatings.isEmpty();
    }

    public boolean isChangePerformance() {
        return selectedTopSpeed != null && !selectedTopSpeed.isEmpty() && !selectedTopSpeed.equalsIgnoreCase(TopSpeedType.Any.getStrValue())
                || selectedPower != null && !selectedPower.isEmpty() && !selectedPower.equalsIgnoreCase(PowerType.Any.getStrValue())
                || selectedTankRange != null && !selectedTankRange.isEmpty() && !selectedTankRange.equalsIgnoreCase(TankType.Any.getStrValue())
                || selectedAcceleration != null && !selectedAcceleration.isEmpty() && !selectedAcceleration.equalsIgnoreCase(AccelerationType.Any.getStrValue());
    }

    public boolean isChangeRunningCost() {
        return selectedCostAnnualTax != null && !selectedCostAnnualTax.isEmpty() && !selectedCostAnnualTax.equalsIgnoreCase(CostAnnualTaxType.Any.getStrValue())
                || selectedInsuranceGroup != null && !selectedInsuranceGroup.isEmpty() && !selectedInsuranceGroup.equalsIgnoreCase(InsuranceType.Any.getStrValue())
                || selectedFuelEfficiency != null && !selectedFuelEfficiency.isEmpty() && !selectedFuelEfficiency.equalsIgnoreCase(FuelEfficiencyType.Any.getStrValue())
                || selectedCO2Emission != null && !selectedCO2Emission.isEmpty() && !selectedCO2Emission.equalsIgnoreCase(Co2EmissionsType.Any.getStrValue());
    }

    public boolean isChangeKeyword() {
        return keywords != null && !keywords.isEmpty();
    }

    public boolean isChangeMoreOptions() {
        return otherOptions != null && !otherOptions.isEmpty();
    }
}