package com.motors.phone.common;

/**
 * Created by abduakhatov on 10/23/17 at 12:47 PM.
 */

public enum DepositType implements iSearchType{
    Deposit_0(" Deposit (\u00a3 0)", 1),
    Deposit_500(" Deposit (\u00a3 500)", 2),
    Deposit_1000(" Deposit (\u00a3 1,000)", 3),
    Deposit_2000(" Deposit (\u00a3 2,000)", 4),
    Deposit_3000(" Deposit (\u00a3 3,000)", 5),
    Deposit_4000(" Deposit (\u00a3 4,000)", 6),
    Deposit_5000(" Deposit (\u00a3 5,000)", 7),
    Deposit_6000(" Deposit (\u00a3 6,000)", 8),
    Deposit_7000(" Deposit (\u00a3 7,000)", 9),
    Deposit_8000(" Deposit (\u00a3 8,000)", 10),
    Deposit_9000(" Deposit (\u00a3 9,000)", 11),
    Deposit_10000(" Deposit (\u00a3 10,000)", 12),
    Deposit_12500(" Deposit (\u00a3 12,500)", 13),
    Deposit_15000(" Deposit (\u00a3 15,000)", 14);

    private String key;
    private int value;

    private DepositType(String key, int value) {
        this.key = key;
        this.value = value;
    }



    public void setKey(String key) {
        this.key = key;
    }

    public static int getValueByKey(String key) {
        for (DepositType item : DepositType.values())
            if (key == item.key)
                return item.value;

        return Deposit_0.value;
    }

    public static int getPosByvalue(int value) {
        for (int i = 0; i < DepositType.values().length; i++) {
            if (DepositType.values()[i].getIntValue() == value) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}
