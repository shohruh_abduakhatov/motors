package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/13/2017.
 */

@Data
public class Dealer implements Serializable {
    @SerializedName("Name")
    private String name;
    @SerializedName("Id")
    private Integer id;
    @SerializedName("LogoImageUrl")
    private String logoImageUrl;
    @SerializedName("WebsiteLink")
    private String webLink;
    @SerializedName("OriginalWebsiteLink")
    private String originalWebLink;
    @SerializedName("Email")
    private String email;
    @SerializedName("Profile")
    private String profile;
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("ServicePhoneNumber")
    private String servicePhoneNumber;
    @SerializedName("PartsPhoneNumber")
    private String partsPhoneNumber;
    @SerializedName("Address")
    private MotorsAddress address;
    @SerializedName("AddressEnabled")
    private boolean addressEnabled;
    @SerializedName("MapEnabled")
    private boolean mapEnabled;
    @SerializedName("WebsiteEnabled")
    private boolean webSiteEnabled;
    @SerializedName("NumberOfCars")
    private int numberCars;
    @SerializedName("NumberOfVans")
    private int numberVans;
    @SerializedName("SecondaryLogoImageUrl")
    private String secondLogoUrl;
    @SerializedName("TypeImageUrl")
    private String typeImageUrl;
    @SerializedName("IsPremium")
    private boolean premium;
    @SerializedName("StockLink")
    private String stockLink;
    //todo Franchises
    @SerializedName("Distance")
    private int distance;
    @SerializedName("ShowNightOwl")
    private boolean showNightOwl;
    @SerializedName("NightOwlEmail")
    private String nightOwlEmail;
    @SerializedName("LatLngOverride")
    private String latlngOverride;
    @SerializedName("ShowDeepLink")
    private boolean showDeepLink;
    @SerializedName("DeepLinkLockediFrame")
    private boolean deepLinkLockedFrame;
    @SerializedName("HideCapData")
    private boolean hideCapData;
    @SerializedName("Location")
    private String location;
    @SerializedName("Type")
    private int type;
    @SerializedName("NameSEOEncoded")
    private String nameSeoEncoded;
    @SerializedName("TownSEOEncoded")
    private String townSeoEncoded;
    @SerializedName("NameURLEncoded")
    private String nameUrlEncoded;
    @SerializedName("DistanceText")
    private String distanceText;
}
