package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum SearchPanelType {
    Unknown("Unknown", 0, "unknown", ""),
    UsedCars("UsedCars", 1, "used cars", "Cars"),
    UsedVans("UsedVans", 2, "used vans", "Vans"),
    NewCars("NewCars", 3, "new cars", "New Cars"),
    NewVans("NewVans", 4, "new vans", "New Vans"),
    CarDealer("CarDealer", 5, "car dealer", "Car Dealer"),
    VanDealer("VanDealer", 6, "van dealer", "Van Dealer"),
    CarStock("CarStock", 7, "car stock", "Car Stock"),
    VanStock("VanStock", 8, "van stock", "Van Stock"),
    WhatsMineWorthCar("WhatsMineWorthCar", 9, "whats mine worth car", "whats mine worth car");

    private int value;
    private String name;
    private String desc;
    private String title;

    SearchPanelType(String name, int value, String desc, String title) {
        this.name = name;
        this.value = value;
        this.desc = desc;
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public static SearchPanelType getTypeByValue(int value) {
        for (SearchPanelType item : SearchPanelType.values()) {
            if (value == item.value) {
                return item;
            }
        }
        return UsedCars;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
