package com.motors.phone.common;

import com.example.nasimxon.components.utils.StringUtils;

/**
 * Created by Nasimxon on 10/10/2017.
 */

public enum EmailType {
    EmailDealer("EmailDealer"),
    RequestSellerDetails("RequestPrivateSellerDetails"),
    SendToFriend("SendToFriend"),
    DealerQuestion("DealerQuestion"),
    NightOwlDealer("NightOwlDealer");

    private String name;

    EmailType(String name) {
        this.name = name;
    }

    public static EmailType getType(String name) {
        if (!StringUtils.isEmpty(name)) {
            for (EmailType emailType : EmailType.values()) {
                if (name.equals(emailType.toString())) {
                    return emailType;
                }
            }
        }
        return EmailDealer;
    }

    public String toString() {
        return this.name;
    }
}