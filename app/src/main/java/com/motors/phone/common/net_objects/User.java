package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;
import com.motors.phone.common.SocialProviderType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class User implements Serializable {
    private static String ticket;
    private static String uid;
    private static String providerName;

    @SerializedName("ProviderName")
    private String prName;
    @SerializedName("Salutation")
    private String salutation;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("EmailAddress")
    private String email;
//    @SerializedName("MobileNumber")
    private String phoneNumber;
//    @SerializedName("ProviderName")
    private String houseNumberOrName;

    @SerializedName("Address")
    private MotorsAddress address;
//    @SerializedName("ProviderName")
    private String streetName = "";
//    @SerializedName("ProviderName")
    private String townCity = "";
//    @SerializedName("ProviderName")
    private String county = "";
//    @SerializedName("ProviderName")
    private String country = "";

    private String lat;
    private String lng;

    @SerializedName("Subscriptions")
    private transient List<String> subscriptionList = new ArrayList<>();

    private String userName;
    @SerializedName("Password")
    private String password;

    @SerializedName("Year")
    private String year;
    @SerializedName("MembershipUserKey")
    private String membershipUserKey;
    @SerializedName("Month")
    private String month;
    @SerializedName("Day")
    private String day;
    @SerializedName("MobileNumber")
    private String mobileNumber;

    private SocialProviderType socialProviderType = SocialProviderType.Default;

    private boolean keepMeSignedIn = false;
    private boolean responseIsValid = false;

    @SerializedName("EmailActivityEnabled")
    private boolean isEmailActivityEnabled = false;
    @SerializedName("HasAdvert")
    private boolean hasAdvert = false;
    @SerializedName("CarAlerts")
    private boolean hasCarAlerts = false;

    @SerializedName("Competitions")
    private boolean competitions = true;
    @SerializedName("MotorsUpdates")
    private boolean motorsUpdates = true;
    @SerializedName("LatestNews")
    private boolean latestNews = true;
    @SerializedName("ThirdParties")
    private boolean selectThirdParty = true;

    private static boolean isSignIn = false;
    private boolean hasDetails = false;
    private boolean isUpdatedAllData = false;

    private boolean poolSynced = false;
    private boolean listPoolSynced = false;
    private boolean signIn;

    private long lastReceivedNotificationDate = 0;

    private int countSavedSearches = 0;
    private int countShortlist = 0;
    private int countEmails = 0;
    private int countNotifications = 0;
    private int countRemovedVehicles;
    private int countNotificationCars = 0;
    private String searchGuid;

    public boolean isSignIn() {
        return signIn;
    }

    public String getUid() {
        return uid;
    }

    public void setTicket(String ticket) {
        User.ticket = ticket;
    }

    public String getTicket() {
        return ticket;
    }
}
