package com.motors.phone.common;

/**
 * Created by LavenderMoon on 10/26/17.
 */

public enum MilesType implements iSearchType {

    Any("National", 10000),
    MILES_TYPE_1("< 1 mile", 1),
    MILES_TYPE_10("< 10 miles", 10),
    MILES_TYPE_20("< 20 miles", 20),
    MILES_TYPE_30("< 30 miles", 30),
    MILES_TYPE_40("< 40 miles", 40),
    MILES_TYPE_50("< 50 miles", 50),
    MILES_TYPE_60("< 60 miles", 60),
    MILES_TYPE_100("< 100 miles", 100),
    MILES_TYPE_200("< 200 miles", 200);

    private String key;
    private int value;

    MilesType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public static int getValueByKey(String key) {
        for (MilesType item : MilesType.values())
            if (key.equalsIgnoreCase(item.getStrKey()))
                return item.getIntValue();
        return Any.value;
    }

    public static int getPosByvalue(int value) {
        for (int i = 0; i < MileageType.values().length; i++) {
            if (MileageType.values()[i].getIntValue() == value) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}