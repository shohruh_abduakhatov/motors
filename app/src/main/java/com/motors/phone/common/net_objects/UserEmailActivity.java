package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;
import com.motors.phone.common.EmailType;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by Nasimxon on 10/10/2017.
 */
@Data
public class UserEmailActivity implements Serializable{
    @SerializedName("ActivityId")
    private long activityId;
    @SerializedName("Version")
    private String version;

    @SerializedName("EmailName")
    private String emailName;
    @SerializedName("EmailDescription")
    private String emailDescription;
    @SerializedName("CreatedDate")
    private String createDate;
    @SerializedName("Vehicle")
    private String vehicle;

    @SerializedName("VehicleId")
    private String vehicleID;
    @SerializedName("VehicleImageUrl")
    private String vehicleImageUrl;

    @SerializedName("VehicleUrl")
    private String vehicleUrl;
    @SerializedName("DealerDetails")
    private List<Dealer> dealer;

    @SerializedName("VehicleDetails")
    private MotorsVehicle car = new MotorsVehicle();

    @SerializedName("EmailWMWData")
    private String emailWMWData;

}
