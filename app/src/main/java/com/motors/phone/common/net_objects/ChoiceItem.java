package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;
import com.example.nasimxon.components.utils.Constants;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public class ChoiceItem implements Serializable { // MakeModels
    @SerializedName("Value")
    private String name;
    transient private Boolean isParent;
    transient private boolean isSelected;
    transient private String parentName;
    transient private int value;
    transient private boolean isWithAnim;

    public ChoiceItem(String name) {
        this.name = name;
    }

    public ChoiceItem() {
    }

    public boolean isWithAnim() {
        return isWithAnim;
    }

    public void setWithAnim(boolean withAnim) {
        isWithAnim = withAnim;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public Boolean isParent() {
        return isParent == null ? false : isParent;
    }

    public void setIsParent(Boolean header) {
        this.isParent = header;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String header_name) {
        this.parentName = header_name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChoiceItem)
            if (((ChoiceItem) obj).getName() != null)
            return name.equalsIgnoreCase(((ChoiceItem) obj).getName());
        return false;
    }
}