package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum PowerType implements iSearchType {
    Any("Top Power (Any)", "1"),
    POWER_0_70("0-70", "0 - 70"),
    POWER_71_100("71-100", "71 - 100"),
    POWER_101_150("101-150", "101 - 150"),
    POWER_151_200("151-200", "151 - 200"),
    POWER_201_250("201-250", "201 - 250"),
    POWER_251_300("251-300", "251 - 300"),
    POWER_301_plus("301+", "301");

    private String key;
    private String value;

    PowerType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String arg) {
        for (PowerType a : PowerType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < PowerType.values().length; i++) {
            if (PowerType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}