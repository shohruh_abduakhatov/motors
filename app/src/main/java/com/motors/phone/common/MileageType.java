package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum MileageType implements iSearchType {
    UpTo1("Mileage (5,000 pa)", 1),
    UpTo2("Mileage (10,000 pa)", 2),
    UpTo3("Mileage (15,000 pa)", 3),
    UpTo4("Mileage (20,000 pa)", 4),
    UpTo5("Mileage (25,000 pa)", 5),
    UpTo6("Mileage (30,000 pa)", 6);

    private String key;
    private int value;

    MileageType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static int getValueByKey(String key) {
        for (MileageType item : MileageType.values())
            if (key.equalsIgnoreCase(item.key))
                return item.value;
        return UpTo1.value;
    }

    public static int getPosByvalue(int value) {
        for (int i = 0; i < MileageType.values().length; i++)
            if (MileageType.values()[i].getIntValue() == value)
                return i == 0 ? -1 : i;
        return -1;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}