package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum Co2EmissionsType implements iSearchType{
    Any("CO2 Emissions (Any)", "*"),
    CO2_0_75("Up to 75 g/km CO2", "0-75"),
    CO2_0_100("Up to 100 g/km CO2", "0-100"),
    CO2_0_110("Up to 110 g/km CO2", "0-110"),
    CO2_0_120("Up to 120 g/km CO2", "0-120"),
    CO2_0_130("Up to 130 g/km CO2", "0-130"),
    CO2_0_140("Up to 140 g/km CO2", "0-140"),
    CO2_0_150("Up to 150 g/km CO2", "0-150"),
    CO2_0_165("Up to 165 g/km CO2", "0-165"),
    CO2_0_175("Up to 175 g/km CO2", "0-175"),
    CO2_0_185("Up to 185 g/km CO2", "0-185"),
    CO2_0_200("Up to 200 g/km CO2", "0-200"),
    CO2_0_225("Up to 225 g/km CO2", "0-225"),
    CO2_0_255("Up to 255 g/km CO2", "0-255"),
    CO2_255_plus("255 g/km CO2 and above", "255+");

    private String key;
    private String value;

    Co2EmissionsType(String key, String value) {
         this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String key) {
        for (Co2EmissionsType item : Co2EmissionsType.values())
            if (key.equalsIgnoreCase(item.getStrKey()))
                return item.getStrValue();
        return Any.value;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}