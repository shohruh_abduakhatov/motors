package com.motors.phone.common;

import android.text.TextUtils;

/**
 * Created by Nasimxon on 10/3/2017.
 */

public enum SalutationType {
    Mr("Mr", "Mr", 1),
    Mrs("Mrs", "Mrs", 2),
    Miss("Miss", "Miss", 3);

    private String name;
    private String title;
    private int index;

    SalutationType(String name, String title, int index) {
        this.name = name;
        this.title = title;
        this.index = index;
    }

    public static int getIndexByName(String name) {
        int result = 0;
        if (!TextUtils.isEmpty(name)) {
            for (SalutationType type : SalutationType.values()) {
                if (type.name.equals(name)) {
                    result = type.index;
                }
            }
        }
        return result;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }
}