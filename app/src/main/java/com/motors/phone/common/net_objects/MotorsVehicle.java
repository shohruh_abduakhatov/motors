package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class MotorsVehicle implements Serializable {
    @SerializedName("Price")
    private int price;
    @SerializedName("OriginalPrice")
    private int originalPrice;
//    @SerializedName("Price")
    private int oldPrice;
    @SerializedName("Domain")
    private int domain;
    @SerializedName("VehicleId")
    private int vehicleId = -1;
    @SerializedName("Distance")
    private int distance;
    @SerializedName("HistoryCheck")
    private int histosyCheck = -1;
    @SerializedName("PriceDiscount")
    private int priceDiscount;
    @SerializedName("ImageCount")
    private int imageCount;
    @SerializedName("VideoCount")
    private int videoCount;
    @SerializedName("Doors")
    private int doors;
    @SerializedName("Seats")
    private int seats;
    @SerializedName("VehicleResultType")
    private int vehicleResultType;
    @SerializedName("Position")
    private int position;
    //todo IconData
    @SerializedName("TankSizeValue")
    private int tankSizeValue;
    //not results value
    @SerializedName("TotalCount")
    private int totalCount;
//    @SerializedName("Price")
    private int currentImageIndex = 0;
    @SerializedName("FuelPriceValue")
    private double fuelPriceValue;

    @SerializedName("IsPromotion")
    private boolean isPromotion;
    @SerializedName("IsVehicleStandOut")
    private boolean vehicleStandOut;
    @SerializedName("IsFeaturedAdvert")
    private boolean featureAdvert;
    @SerializedName("IsHeroSpotAdvert")
    private boolean heroSpotAdvert;
    @SerializedName("Status")
    private boolean status = true;//vehicle isAvailable or removed
    @SerializedName("IsDisplayNightOwl")
    private boolean displayNightOwl;
    @SerializedName("IsShortListed")
    private boolean shortListed;
    @SerializedName("IsPriceExcludingVAT")
    private boolean priceExcludingVAT;
    @SerializedName("ChatEnabled")
    private boolean chatEnabled;
    @SerializedName("JudgeServiceEnabled")
    private boolean judgeServiceEnabled;
    @SerializedName("HideAge")
    private boolean hideAge;
    @SerializedName("Car2CarClickEnabled")
    private boolean carToCarClickEnabled;
    @SerializedName("FranchiseApproved")
    private boolean franchiseApproved;
    @SerializedName("Sold")
    private boolean sold;
    @SerializedName("WhatsMineWorthStatus")
    private boolean whatsMineWorthStatus;
    @SerializedName("DefaultDealerImage")
    private boolean defaultDealerImage;
    @SerializedName("IsAssociatedVehiclesRequired")
    private boolean isAssociatedVehiclesRequired;

    @SerializedName("IsReduced")
    private Boolean reduced;

    @SerializedName("GBPPrice")
    private String gbpPrice;
    @SerializedName("GBPOriginalPrice")
    private String gbpOriginalPrice;
    @SerializedName("DiscountText")
    private String discountText;
    @SerializedName("ShortDescription")
    private String shortDesc;
    @SerializedName("ShortFeaturesSummary")
    private String shortFeaturesSummary;
    @SerializedName("Description")
    private String description;
    @SerializedName("EngineSizeLitres")
    private String engineSizeLiters;
    @SerializedName("MotorsMobilePhoneNumber")
    private String motorsMobilePhoneNumber;
    @SerializedName("DistanceText")
    private String distanceText;
    @SerializedName("HistoryCheckDescription")
    private String historyCheckDesc;
    @SerializedName("Title")
    private String title;
    @SerializedName("Manufacturer")
    private String manufacturer;
    @SerializedName("Model")
    private String model;
    @SerializedName("Location")
    private String location;
    @SerializedName("Mileage")
    private String mileage;
    @SerializedName("Transmission")
    private String transmission;
    @SerializedName("Colour")
    private String colour;
    @SerializedName("BodyStyle")
    private String bodyStyle;
    @SerializedName("FuelType")
    private String fuelType;
    @SerializedName("RegistrationYear")
    private String registerYear;
    @SerializedName("PromotionText")
    private String promotionText;
    @SerializedName("PostCode")
    private String postCode;
    @SerializedName("CapTrim")
    private String capTrim;
    @SerializedName("RegistrationNumberYear")
    private String registerNumberYear;
    @SerializedName("RelatedSearchLink")
    private String relatedSearchLink;
    @SerializedName("Car2CarClick")
    private String carToCarClick;
    @SerializedName("redirectUrl")
    private String redirectUrl;
    @SerializedName("FeaturesSummary")
    private String featuresSummary;
    @SerializedName("DefaultFinanceData")
    private String defaultFinanceData;
    @SerializedName("InteriorDescription")
    private String interiorDesc;
    @SerializedName("DefaultMonthlyCost")
    private String defaultMonthlyCost;
    @SerializedName("DefaultCFCUrl")
    private String defaultCFUrl;
    @SerializedName("MPG")
    private String mpg;
    @SerializedName("FuelTankCapacity")
    private String fuelTankCapacity;
    @SerializedName("TankRange")
    private String tankRange;
    @SerializedName("CostToFill")
    private String costToFill;
    @SerializedName("RoadTax")
    private String roadTax;
    @SerializedName("CO2")
    private String co2;
    @SerializedName("Email")
    private String email;
    //from detail
    @SerializedName("InsuranceGroup")
    private String insurancegroup;
    @SerializedName("BHP")
    private String bhp;
    @SerializedName("CO2Emissions")
    private String co2Emissions;
    @SerializedName("SafetyRating")
    private String safetyRating;
    @SerializedName("RegistrationMonth")
    private String registrationMonth;
    @SerializedName("VehicleRedirectUrl")
    private String vehicleRedirectUrl;
    @SerializedName("youTubeLink")
    private String youTubeLink;
    @SerializedName("GBPPriceText")
    private String gbpPriceText;

//    private CapData capData;
//    private MainImage mainImage;
//    private Dealer dealer;
//    private SearchPanelType searchPanelType;
//    private VendorType vendorType;
//    private VehicleType vehicleType;
}
