package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum AccelerationType implements iSearchType {
    Any("Acceleration (Any)", "1"),
    ACCELERATION_0_4("Up to 4 secs 0-62 mph", "0 - 4"),
    ACCELERATION_0_5("Up to 5 secs 0-62 mph", "0 - 5"),
    ACCELERATION_0_6("Up to 6 secs 0-62 mph", "0 - 6"),
    ACCELERATION_0_7("Up to 7 secs 0-62 mph", "0 - 7"),
    ACCELERATION_0_8("Up to 8 secs 0-62 mph", "0 - 8"),
    ACCELERATION_0_9("Up to 9 secs 0-62 mph", "0 - 9"),
    ACCELERATION_10_plus("10 secs and above 0-62 mph", "10");

    private String key;
    private String value;

    AccelerationType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String arg) {
        for (AccelerationType a : AccelerationType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < TankType.values().length; i++) {
            if (AccelerationType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}