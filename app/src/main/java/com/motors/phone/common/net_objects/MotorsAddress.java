package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/7/2017.
 */
@Data
public class MotorsAddress implements Serializable {
    @SerializedName("Line1")
    private String line1;
    @SerializedName("Line2")
    private String line2;
    @SerializedName("Line3")
    private String line3;
    @SerializedName("County")
    private String country;
    @SerializedName("Town")
    private String town;
    @SerializedName("Postcode")
    private String postcode;
    @SerializedName("FormattedAddress")
    private String formattedAddress;
    public MotorsAddress(JSONObject object) {
        line1 = object.optString("Line1");
        line2 = object.optString("Line2");
        line3 = object.optString("Line3");
        country = object.optString("County");
        town = object.optString("Town");
        postcode = object.optString("Postcode");
        formattedAddress = object.optString("FormattedAddress");
    }
    public MotorsAddress() {}
}
