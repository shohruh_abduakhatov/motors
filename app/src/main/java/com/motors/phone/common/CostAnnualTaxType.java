package com.motors.phone.common;

/**
 * Created by abduakhatov on 10/25/17 at 4:37 PM.
 */

public enum CostAnnualTaxType implements iSearchType {
    Any("Cost of Annual Tax (Any)", "*"),
    TAX_TYPE_A("Tax £0 pa", "A"),
    TAX_TYPE_B("Tax £20 pa or less", "B"),
    TAX_TYPE_C("Tax £30 pa or less", "C"),
    TAX_TYPE_D("Tax £110 pa or less", "D"),
    TAX_TYPE_E("Tax £130 pa or less", "E"),
    TAX_TYPE_F("Tax £145 pa or less", "F"),
    TAX_TYPE_G("Tax £178 pa or less", "G"),
    TAX_TYPE_H("Tax £205 pa or less", "H"),
    TAX_TYPE_I("Tax £225 pa or less", "I"),
    TAX_TYPE_J("Tax £265 pa or less", "J"),
    TAX_TYPE_K("Tax £285 pa or less", "K"),
    TAX_TYPE_L("Tax £485 pa or less", "L"),
    TAX_TYPE_M("Tax £500 pa or less", "M");

    private String key;
    private String value;

    CostAnnualTaxType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String key) {
        for (CostAnnualTaxType item : CostAnnualTaxType.values())
            if (key.equalsIgnoreCase(item.getStrKey()))
                return item.getStrValue();
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < CostAnnualTaxType.values().length; i++) {
            if (CostAnnualTaxType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}
