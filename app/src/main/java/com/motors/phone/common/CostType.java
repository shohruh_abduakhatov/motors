package com.motors.phone.common;

/**
 * Created by abduakhatov on 10/23/17 at 8:12 AM.
 */

public enum CostType implements iSearchType {
    Any("Cost (Any)", 1),
    Cost_50("\u00a3 50", 2),
    Cost_100("\u00a3 100", 3),
    Cost_150("\u00a3 150", 4),
    Cost_200("\u00a3 200", 5),
    Cost_250("\u00a3 250", 6),
    Cost_300("\u00a3 300", 7),
    Cost_350("\u00a3 350", 8),
    Cost_400("\u00a3 400", 9),
    Cost_450("\u00a3 450", 10),
    Cost_500("\u00a3 500", 11),
    Cost_600("\u00a3 600", 12),
    Cost_700("\u00a3 700", 13),
    Cost_800("\u00a3 800", 14),
    Cost_900("\u00a3 900", 15),
    Cost_1000("\u00a3 1,000", 16);

    private String key;
    private int value;


    CostType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public static int getValueByKey(String key) {
        for (CostType item : CostType.values())
            if (key == item.key)
                return item.value;

        return Any.value;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}
