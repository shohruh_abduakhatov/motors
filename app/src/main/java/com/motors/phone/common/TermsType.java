package com.motors.phone.common;

/**
 * Created by abduakhatov on 10/23/17 at 12:52 PM.
 */

public enum TermsType implements iSearchType{
    Term_12("Term (12 Months pa)", 0),
    Term_18("Term (18 Months pa)", 1),
    Term_24("Term (24 Months pa)", 2),
    Term_30("Term (30 Months pa)", 3),
    Term_36("Term (36 Months pa)", 4),
    Term_42("Term (42 Months pa)", 5),
    Term_48("Term (48 Months pa)", 6),
    Term_54("Term (54 Months pa)", 7),
    Term_60("Term (60 Months pa)", 8);

    private String key;
    private int value;

    TermsType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static int getValueByKey(String key) {
        for (TermsType item : TermsType.values())
            if (key == item.key)
                return item.value;

        return Term_12.value;
    }

    public static int getPosByvalue(int value) {
        for (int i = 0; i < TermsType.values().length; i++) {
            if (TermsType.values()[i].getIntValue() == value) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}
