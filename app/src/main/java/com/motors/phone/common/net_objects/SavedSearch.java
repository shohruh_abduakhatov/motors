package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by LOC on 10/10/2017.
 */
@Data
public class SavedSearch implements Serializable {
    @SerializedName("Domain")
    private Integer domain;
    @SerializedName("Id")
    private Long id;
    @SerializedName("Site")
    private String site;
    @SerializedName("AccountId")
    private Long accountId;
    @SerializedName("Name")
    private String name;
    @SerializedName("AlertEnabled")
    private Boolean alertEnabled;
    @SerializedName("CreatedDate")
    private String createdDate;
    @SerializedName("LastProcessedDate")
    private String lastProcessedDate;
    @SerializedName("Type")
    private String type;

    @SerializedName("SearchParameters")
    private SearchData searchData;
    @SerializedName("Schedule")
    private List<Integer> schedule;
}
