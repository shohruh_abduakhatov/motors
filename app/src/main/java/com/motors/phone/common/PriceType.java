package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum PriceType implements iSearchType {
    Any("Price (Any)", 1),
    Price_500("\u00a3 500", 2),
    Price_1000("\u00a3 1,000", 3),
    Price_2000("\u00a3 2,000", 4),
    Price_3000("\u00a3 3,000", 5),
    Price_4000("\u00a3 4,000", 6),
    Price_5000("\u00a3 5,000", 7),
    Price_6000("\u00a3 6,000", 8),
    Price_7000("\u00a3 7,000", 9),
    Price_8000("\u00a3 8,000", 10),
    Price_9000("\u00a3 9,000", 11),
    Price_10000("\u00a3 10,000", 12),
    Price_12500("\u00a3 12,500", 13),
    Price_15000("\u00a3 15,000", 14),
    Price_17500("\u00a3 17,500", 15),
    Price_20000("\u00a3 20,000", 16),
    Price_25000("\u00a3 25,000", 17),
    Price_30000("\u00a3 30,000", 18),
    Price_35000("\u00a3 35,000", 19),
    Price_40000("\u00a3 40,000", 20),
    Price_50000("\u00a3 50,000", 21),
    Price_75000("\u00a3 75,000", 22),
    Price_100000("\u00a3 100,000", 23),
    Price_250000("\u00a3 250,000", 24),
    Price_500000("\u00a3 500,000", 25),
    Price_1000000("\u00a3 1,000,000", 26);

    private String key;
    private int value;

    PriceType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public static int getValueByKey(String key) {
        for (PriceType item : PriceType.values())
            if (key.equals(item.key))
                return item.value;

        return Any.value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return value;
    }
}