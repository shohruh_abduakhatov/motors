package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum CarType {
    SaleCars("Cars For Sale", false, "notsold"),
    SoldCars("Sold Cars", true, "sold"),
    AllCars("All Cars", null, "both");

    private String name;
    private Boolean value;
    private String label;

    private CarType() {
    }

    CarType(String name, Boolean value, String label) {
        this.name = name;
        this.value = value;
        this.label = label;
    }

    public String toString() {
        return this.name;
    }

    public Boolean boolValue() {
        return this.value;
    }

    public String getLabel() {
        return this.label;
    }

    public static CarType getByName(String name) {
        if (SoldCars.name.equals(name)) {
            return SoldCars;
        } else if (SaleCars.name.equals(name)) {
            return SaleCars;
        } else {
            return AllCars;
        }
    }

    public static CarType getByLabel(String label) {
        if (SoldCars.label.equals(label)) {
            return SoldCars;
        } else if (SaleCars.label.equals(label)) {
            return SaleCars;
        } else {
            return AllCars;
        }
    }
}