package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum TopSpeedType implements iSearchType {
    Any("Top Speed (Any)", "1"),
    SPEED_0_100("0-100 mph", "0 - 100"),
    SPEED_101_130("101-130 mph", "101 - 130"),
    SPEED_131_160("131-160 mph", "131 - 160"),
    SPEED_161_plus("161 + mph", "161");

    private String key;
    private String value;

    TopSpeedType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String arg) {
        for (TopSpeedType a : TopSpeedType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < TopSpeedType.values().length; i++) {
            if (TopSpeedType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}