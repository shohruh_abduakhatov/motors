package com.motors.phone.common;

import com.motors.phone.common.search.TotalMileageType;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum AgeType implements iSearchType{
    Any(-1, "Age (Any)"),
    UpTo1(2, "Up to 1 year old"),
    UpTo2(3, "Up to 2 year old"),
    UpTo3(4, "Up to 3 year old"),
    UpTo4(5, "Up to 4 year old"),
    UpTo5(6, "Up to 5 year old"),
    UpTo6(7, "Up to 6 year old"),
    UpTo7(8, "Up to 7 year old"),
    UpTo8(9, "Up to 8 year old"),
    UpTo9(10, "Up to 9 year old"),
    UpTo10(11, "Up to 10 year old");
//    UpTo11(-1, 12);

    private int key;
    private String value;

    AgeType(int key, String value) {
        this.key = key;
        this.value = value;
    }


    public void setKey(int key) {
        this.key = key;
    }

    @Override
    public String getStrKey() {
        return null;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return key;
    }

    @Override
    public int getIntValue() {
        return 0;
    }

    public static int getAgePosByKey(int key) {
        for (int i = 0; i < AgeType.values().length; i++) {
            if (AgeType.values()[i].getIntKey() == key)
                return i;
        }
        return 0;
    }

    public static int getKeyByValue (String value) {
        for (AgeType ageType : AgeType.values()) {
            if (ageType.getStrValue().equalsIgnoreCase(value)) return ageType.getIntKey();
        }
        return AgeType.Any.getIntKey();
    }
}