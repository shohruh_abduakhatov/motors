package com.motors.phone.common.search;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum SellerType {
    AllSellers("All Sellers", "*"),
    Dealer("Dealer", "trade"),
    Private("Private", "private");

    private String name;
    private String value;

    private SellerType(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String toString() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public static SellerType getByName(String name) {
        if (Dealer.name.equals(name)) {
            return Dealer;
        } else if (Private.name.equals(name)) {
            return Private;
        } else {
            return AllSellers;
        }
    }

    public static SellerType getByValue(String value) {
        if (Dealer.value.equals(value)) {
            return Dealer;
        } else if (Private.value.equals(value)) {
            return Private;
        } else {
            return AllSellers;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}