package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum FuelEfficiencyType implements iSearchType{
    Any("Fuel Efficiency (Any)", "*"),
    FUEL_0_40("Up to 40 mpg", "0-40"),
    FUEL_40_plus("Over 40 mpg", "40+"),
    FUEL_50_plus("Over 50 mpg", "50+"),
    FUEL_60_plus("Over 60 mpg", "60+"),
    FUEL_70_plus("70 mpg and above", "70+");

    private String key;
    private String value;

    private FuelEfficiencyType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String key) {
        for (FuelEfficiencyType item : FuelEfficiencyType.values())
            if (key.equalsIgnoreCase(item.getStrKey()))
                return item.getStrValue();
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < FuelEfficiencyType.values().length; i++) {
            if (FuelEfficiencyType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}