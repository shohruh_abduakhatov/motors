package com.motors.phone.common;

/**
 * Created by LavenderMoon on 10/26/17.
 */

public enum MaxTowingUnbrakedType implements iSearchType {


    Any("Max Towing UnBraked (Any)", 1),
    MAX_TOWING_BRAKED_TYPE_250("At least 250 kg", 250),
    MAX_TOWING_BRAKED_TYPE_500("At least 500 kg", 500),
    MAX_TOWING_BRAKED_TYPE_750("At least 750 kg", 750);


    private String key;
    private int value;
    private String keyWithPlus;

    MaxTowingUnbrakedType(String key, int value) {
        this(key, value, key);
    }

    MaxTowingUnbrakedType(String key, int value, String keyWithPlus) {
        this.key = key;
        this.value = value;
        this.keyWithPlus = keyWithPlus;
    }

    public static int getValueByKey(String arg, boolean isKey) {
        for (MaxTowingUnbrakedType a : MaxTowingUnbrakedType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public int getIntValue() {
        return value;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }
}