package com.motors.phone.common;

import com.example.nasimxon.components.utils.Constants;

/**
 * Created by Nasimxon on 10/25/2017.
 */

public enum EngineSizeType {
    Any("*", Constants.ANY, 1),
    EngineSize_0_1("0-1", "Less than 1L", 2),
    EngineSize_1_13("1-1.3", "1L - 1.3L", 3),
    EngineSize_14_16("1.4-1.6", "1.4L - 1.6L", 4),
    EngineSize_17_19("1.7-1.9", "1.7L - 1.9L", 5),
    EngineSize_2_25("2-2.5", "2L - 2.5L", 6),
    EngineSize_26_29("2.6-2.9", "2.6L - 2.9L", 7),
    EngineSize_3_39("3-3.9", "3L - 3.9L", 8),
    EngineSize_4_49("4-4.9", "4L - 4.9L", 9),
    EngineSize_5_plus("5+", "5L +", 10, "5+");

    private String key;
    private String value;
    private int id;
    private String keyWithPlus;

    private EngineSizeType(String key, String value, int id) {
        this(key, value, id, key);
    }
    private EngineSizeType(String key, String value, int id, String keyWithPlus) {
        this.key = key;
        this.value = value;
        this.keyWithPlus = keyWithPlus;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return value;
    }

    public static String getValueByKey(String arg, boolean isKey) {
        String result = isKey ? Any.toString() : Any.getKey();
        if (!arg.equals(result)) {
            for (EngineSizeType item : EngineSizeType.values()) {
                if (arg.equals(isKey ? item.key : item.value)) {
                    return isKey ? item.value : item.key;
                }
            }
        }
        return result;
    }

    public static EngineSizeType getEngineById(int id) {
        for (EngineSizeType engineSizeType : EngineSizeType.values()) {
            if (engineSizeType.getId() == id) return engineSizeType;
        }
        return EngineSizeType.Any;
    }

    public static EngineSizeType getEngineByKey(String key) {
        for (EngineSizeType engineSizeType : EngineSizeType.values()) {
            if (engineSizeType.getKey().equalsIgnoreCase(key)) return engineSizeType;
        }
        return EngineSizeType.Any;
    }
}
