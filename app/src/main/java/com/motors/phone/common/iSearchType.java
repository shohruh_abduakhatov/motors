package com.motors.phone.common;

/**
 * Created by abduakhatov on 10/24/17 at 4:19 PM.
 */

public interface iSearchType {
    String getStrKey();

    String getStrValue();

    int getIntKey();

    int getIntValue();
}
