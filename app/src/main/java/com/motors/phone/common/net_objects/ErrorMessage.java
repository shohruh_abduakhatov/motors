package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */

@Data
public class ErrorMessage implements Serializable {
    @SerializedName("FriendlyMessage")
    private String friendMessage;
    @SerializedName("Message")
    private String message;
    @SerializedName("Code")
    private Integer code;
}
