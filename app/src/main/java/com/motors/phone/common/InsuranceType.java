package com.motors.phone.common;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum InsuranceType implements iSearchType{
    Any("Insurance Group (Any)", "*"),
    INSURANCE_0_1("Insurance group 1", "0-1"),
    INSURANCE_0_5("Insurance group 5 and under", "0-5"),
    INSURANCE_0_10("Insurance group 10 and under", "0-10"),
    INSURANCE_0_15("Insurance group 15 and under", "0-15"),
    INSURANCE_0_20("Insurance group 20 and under", "0-20"),
    INSURANCE_0_25("Insurance group 25 and under", "0-25"),
    INSURANCE_0_30("Insurance group 30 and under", "0-30"),
    INSURANCE_0_35("Insurance group 35 and under", "0-35"),
    INSURANCE_0_40("Insurance group 40 and under", "0-40"),
    INSURANCE_0_45("Insurance group 45 and under", "0-45");

    private String key;
    private String value;

    InsuranceType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(String key) {
        for (InsuranceType item : InsuranceType.values())
            if (key.equalsIgnoreCase(item.getStrKey()))
                return item.getStrValue();
        return Any.value;
    }

    public static int getPosByvalue(String value) {
        for (int i = 0; i < InsuranceType.values().length; i++) {
            if (InsuranceType.values()[i].getStrValue().equalsIgnoreCase(value)) return i;
        }
        return 0;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public String getStrValue() {
        return value;
    }

    @Override
    public int getIntKey() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return 0;
    }
}