package com.motors.phone.common;

import com.example.nasimxon.components.utils.Constants;

/**
 * Created by Nasimxon on 10/16/2017.
 */

public enum PriceTypeSmart {
    Any("-1", Constants.ANY),
    Price_1("1", "£1"),
    Price_500("500", "£500"),
    Price_1000("1000", "£1,000"),
    Price_2000("2000", "£2,000"),
    Price_3000("3000", "£3,000"),
    Price_4000("4000", "£4,000"),
    Price_5000("5000", "£5,000"),
    Price_6000("6000", "£6,000"),
    Price_7000("7000", "£7,000"),
    Price_8000("8000", "£8,000"),
    Price_9000("9000", "£9,000"),
    Price_10000("10000", "£10,000"),
    Price_12500("12500", "£12,500"),
    Price_15000("15000", "£15,000"),
    Price_17500("17500", "£17,500"),
    Price_20000("20000", "£20,000"),
    Price_25000("25000", "£25,000"),
    Price_30000("30000", "£30,000"),
    Price_35000("35000", "£35,000"),
    Price_40000("40000", "£40,000"),
    Price_50000("50000", "£50,000"),
    Price_75000("75000", "£75,000"),
    Price_100000("100000", "£100,000"),
    Price_250000("250000", "£250,000"),
    Price_500000("500000", "£500,000"),
    Price_1000000("1000000", "£1,000,000");

    private String key;
    private String value;


    private PriceTypeSmart(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return value;
    }

    public static String getValueByKey(String arg, boolean isKey) {
        String result = isKey ? Any.toString() : Any.getKey();
        if (!arg.equals(result)) {
            for (PriceTypeSmart item : PriceTypeSmart.values()) {
                if (arg.equals(isKey ? item.key : item.value)) {
                    return isKey ? item.value : item.key;
                }
            }
        }
        return result;
    }
}