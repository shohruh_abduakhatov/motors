package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by Nasimxon on 10/10/2017.
 */

@Data
public class FullDetail implements Serializable{
    @SerializedName("UserDetails")
    private User user;
    @SerializedName("SavedSearches")
    private List<SavedSearch> savedSearches;
    @SerializedName("SavedVehicles")
    private List<MotorsVehicle> savedVehicles;
    @SerializedName("EmailActivity")
    private SubEmail emailActivity;
    @SerializedName("RemovedVehiclesCount")
    private Integer removedVehiclesCount;
    @SerializedName("Advert")
    private Boolean advert;
    @Data
    private class SubEmail implements Serializable{
        @SerializedName("EmailActivity")
        private List<UserEmailActivity> emailActivity;
    }
}
