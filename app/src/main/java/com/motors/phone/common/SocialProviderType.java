package com.motors.phone.common;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum SocialProviderType implements Serializable{
    Default("default"),
    Facebook("facebook"),
    Twitter("twitter"),
    GooglePlus("google");

    @SerializedName("ProviderName")
    private String name;

    private SocialProviderType(String name) {
        this.name = name;
    }

    public static SocialProviderType getType(String typeName) {
        SocialProviderType type = Default;
        if (typeName != null && !typeName.isEmpty()) {
            if (typeName.equals("facebook")) {
                type = Facebook;
            } else if (typeName.equals("twitter")) {
                type = Twitter;
            } else if (typeName.contains("google")) {
                type = GooglePlus;
            }
        }
        return type;
    }

    public String getName() {
        return name;
    }
}
