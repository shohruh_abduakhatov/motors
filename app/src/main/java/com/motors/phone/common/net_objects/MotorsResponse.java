package com.motors.phone.common.net_objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by Nasimxon on 10/2/2017.
 */
@Data
public class MotorsResponse<T> implements Serializable {
    @SerializedName("Data")
    private T data;
    @SerializedName("ErrorMessages")
    private List<ErrorMessage> messages;
    @SerializedName("IsValid")
    private boolean valid;
    @SerializedName("AppAlert")
    private Object appAlert;
}
