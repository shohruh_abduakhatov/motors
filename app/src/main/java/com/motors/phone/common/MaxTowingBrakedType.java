package com.motors.phone.common;

/**
 * Created by LavenderMoon on 10/26/17.
 */

public enum MaxTowingBrakedType implements iSearchType {
    Any("Max Towing Braked (Any)", 1),
    MAX_TOWING_BRAKED_TYPE_250("At least 250 kg", 250),
    MAX_TOWING_BRAKED_TYPE_500("At least 500 kg", 500),
    MAX_TOWING_BRAKED_TYPE_750("At least 750 kg", 750),
    MAX_TOWING_BRAKED_TYPE_1000("At least 1000 kg", 1000),
    MAX_TOWING_BRAKED_TYPE_1250("At least 1250 kg", 1250),
    MAX_TOWING_BRAKED_TYPE_1500("At least 1500 kg", 1500),
    MAX_TOWING_BRAKED_TYPE_1750("At least 1750 kg", 1750),
    MAX_TOWING_BRAKED_TYPE_2000("At least 2000 kg", 2000),
    MAX_TOWING_BRAKED_TYPE_2250("At least 2250 kg", 2250),
    MAX_TOWING_BRAKED_TYPE_2500("At least 2500 kg", 2500),
    MAX_TOWING_BRAKED_TYPE_2750("At least 2750 kg", 2750),
    MAX_TOWING_BRAKED_TYPE_3000("At least 3000 kg", 3000),
    MAX_TOWING_BRAKED_TYPE_3250("At least 3250 kg", 3250),
    MAX_TOWING_BRAKED_TYPE_3500("At least 3500 kg", 3500);

    private String key;
    private int value;

    MaxTowingBrakedType(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public static int getValueByKey(String arg, boolean isKey) {
        for (MaxTowingBrakedType a : MaxTowingBrakedType.values()) {
            if (arg.equals(a.key)) {
                return a.value;
            }
        }
        return Any.value;
    }

    @Override
    public String getStrKey() {
        return key;
    }

    @Override
    public int getIntValue() {
        return value;
    }

    @Override
    public String getStrValue() {
        return null;
    }

    @Override
    public int getIntKey() {
        return 0;
    }
}
