package com.motors.phone.common.search;

/**
 * Created by Nasimxon on 10/2/2017.
 */

public enum SearchType {
    Default, /*dummy*/
    Normal,
    DealerStock,
    Notification
}